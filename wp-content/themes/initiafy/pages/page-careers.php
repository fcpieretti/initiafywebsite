<?php /* Template Name: Page Careers */
//get_header();
get_template_part( 'parts/header-type');
$page_id = get_the_ID(); ?>

<?php $intro_text = get_field('intro_text'); 
if(!empty($intro_text)){ ?> 
	<section class="red_circle_background gray_background">
		<div class="red_circle"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-12 text_center">
					<?php echo $intro_text; ?>
				</div>
			</div> 
		</div>
	</section>
<?php } ?>
 
<section class="gray_background jobs_section">
	<div class="container container-small-4">
		<div class="row">
			<div class="col-md-12 general_content">
				<?php 	$jobs_intro = get_field('jobs_intro');
						if(!empty($jobs_intro)){ ?>
							<article class="general_content full-article">
								<?php echo $jobs_intro; ?>
							</article>
				<?php } ?> 
			</div>
		<?php 
			$args =  array( 
                'ignore_sticky_posts' 	=> true, 
                'post_type'           	=> 'job',
                'order'              	=> 'DESC',
                'posts_per_page'		=> 10,
                'meta_query'			=>  array(
						array(
							'key'     	=> 'active_job',
							'compare' 	=> '==',
							'value' 	=> '1'		
						)
					)
			);   

        	$args['paged'] = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
		 	$loop = new WP_Query( $args ); 
 			$count = 1 ;
 			if ($loop->have_posts()) {  ?>
 				<div class="jobs-list">
 				<?php  while ($loop->have_posts())	{  
 						$loop->the_post();
						$job_id = get_the_ID();  ?>

						<div class="jobs-list-item">
							<div class="jobs-meta-block">
								<div class="jobs-meta">
									<h2><?php echo get_the_title($job_id); ?></h2>	

									<div class="jobs-meta-wrapp">
										<?php $location = get_field('location', $job_id); 
										if(!empty($location)){ ?>
											<div class="jobs-meta-single">
												<p><i class="fa fa-map-marker" aria-hidden="true"></i><?php echo $location; ?></p>
											</div>
										<?php } ?>

										<?php 	$salary = get_field('salary', $job_id);
												if(!empty( $salary)){  ?>
													<div class="jobs-meta-single">
														<p><?php echo $salary; ?></p>
													</div>
										<?php 	} ?>
									</div>
								</div>

								<?php 	$external_link = get_field('external_link', $job_id); 
										if(!empty($external_link)){ ?>
										<div class="jobs-meta-appy">
											<a  href="<?php echo $external_link; ?>" target="_blank"  title="" class="btn rounded_btn red_btn applyform"><?php _e('APPLY NOW', THEME_TEXT_DOMAIN); ?></a>
										</div>
									<?php } ?>
							</div>
							
							<?php 	$description = get_field('description', $job_id); 
									if(!empty($description)){ ?>
										<div class="jobs-description">
											<?php echo $description; ?>	
										</div>
							<?php 	} ?>
						</div>
 				<?php }	?>
 			</div>
 			
			<div class="col-md-12">
				<div class="text-center">
				 	<?php wp_pagenavi(array( 'query' => $loop )); ?>
				</div>
			</div>

		<?php }	?>
		<?php wp_reset_query(); ?>		
		</div>
	</div>	
</section>

<?php get_footer();
