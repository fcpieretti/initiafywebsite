<?php defined( 'ABSPATH' ) or die( 'Direct access is forbidden!' );

define('THEME_TEXT_DOMAIN', 'initiafy');

/**
 * This is required for $_SESSION variables to work
 */
session_start();

function add_media_upload() {
    if ( is_admin() ) {
         return;
       }
    wp_enqueue_media();
}
add_action('wp_enqueue_scripts', 'add_media_upload');

# General (theme setup, enqueue css/js, hide admin bar)
require_once TEMPLATEPATH . '/inc/general.php';

# Settings page
require_once TEMPLATEPATH . '/inc/settings.php';

# Register navigation menus
require_once TEMPLATEPATH . '/inc/navigation.php';


function cc_mime_types( $mimes ){
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}
add_filter( 'upload_mimes', 'cc_mime_types' );


function slugify($text){
    // replace non letter or digits by -
    $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
    // trim
    $text = trim($text, '-');
    // transliterate
    if (function_exists('iconv')){
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
    }
    // lowercase
    $text = strtolower($text);
    // remove unwanted characters
    $text = preg_replace('~[^-\w]+~', '', $text);
    if (empty($text)) {
        return 'n-a';
    }
    return $text;
}


function phonenr($args){
    $str = $args; 
    $str = preg_replace('/[^+0-9a-zA-Z]/', '', $str); 
    return $str; 
}

// ajax load content slider - gallery page
function add_localize_script()  {
?>
<script type="text/javascript">
    var jsHomeUrl = '<?php echo home_url(); ?>';
    var ajaxUrl = "<?php echo admin_url( 'admin-ajax.php' ) ?>";
</script>
<?php
}
add_action('wp_head', 'add_localize_script', 999);


// function generate login
function generate_login_form($classes, $color){

    $add_login_button_header    = get_field('add_login_button_header', 'options');
    $login_button_header        = get_field('login_button_header', 'options');
    $login_button_link_header   = get_field('login_button_link_header', 'options');
    $external                   = get_field('login_button_external_link_header', 'options');

    if(!empty($add_login_button_header)) {
        if(!empty($login_button_header) && !empty($login_button_link_header)) {

            switch ( $color) {
                case 'white': ?>
                    
                    <a href="<?php echo $login_button_link_header; ?>" <?php if(!empty($external)){ echo 'target="_blank"'; } ?> title="" <?php if(!empty($classes)) { echo 'class="btn rounded_btn login_btn"'; } ?>>
                        <img src="<?php echo get_template_directory_uri();?>/images/lock-white-icon.png"><?php echo $login_button_header; ?>
                    </a>

        <?php       break;
                case 'pink': ?>

                    <a href="<?php echo $login_button_link_header; ?>" <?php if(!empty($external)){ echo 'target="_blank"'; } ?> title="" <?php if(!empty($classes)) { echo 'class="btn rounded_btn login_btn"'; } ?>>
                        <img src="<?php echo get_template_directory_uri();?>/images/lock-pink-icon.png"><?php echo $login_button_header; ?>
                    </a>
                    
         <?php      break;
                default: break;
            }

        }
    }
}

// excerpt
function custom_excerpt_length( $length ) {
    return 26;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

// width
add_filter( 'post_thumbnail_html', 'remove_width_attribute', 10 );
add_filter( 'image_send_to_editor', 'remove_width_attribute', 10 );

function remove_width_attribute( $html ) {
   $html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
   return $html;
}
 


// Hide editor for specific page templates. 

add_action( 'admin_init', 'hide_editor' );
function hide_editor() {
    // Get the Post ID.
    $post_id = 0;

    if(!empty($_GET['post']) || !empty($_POST['post_ID'])){
        $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
        if( !isset( $post_id ) ) return;

        // Get the name of the Page Template file.
        $template_file = get_post_meta($post_id, '_wp_page_template', true);
        
        $array_remove_editor = array(
            'pages/page-pricing.php',
            'pages/page-product.php'
        );

        if( in_array ($template_file, $array_remove_editor ) ){ // edit the template name
            remove_post_type_support('page', 'editor');
        }

        $array_remove_thumbnail = array(
            'pages/page-pricing.php',
            'pages/page-product.php'
        );

        if( in_array ($template_file, $array_remove_thumbnail) ){
            remove_post_type_support('page', 'thumbnail'); 
        }
        //remove_post_type_support( 'post', 'thumbnail' );

    }
}


