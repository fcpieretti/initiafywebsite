<?php /* Template Name: Page About */

// $type_of_header = get_field('type_of_header');
// switch ($type_of_header) {
// 	case 'white':
// 		get_header();
// 		break;
// 	case 'transparent':
// 		get_header('single'); 
// 		break; 
// 	default: get_header(); break;
// } 

get_template_part( 'parts/header-type');  ?>

<?php /*	$intro_title = get_field('intro_title');
		$intro_image = get_field('intro_image'); ?>
<section class="hero blog_hero about_hero hero_header">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="img_background" style="background-image: url('<?php  if(!empty($intro_image)){ echo $intro_image['sizes']['hero-single']; } ?>');" alt="<?php echo get_the_title(); ?>">
					<div class="dark_blue_overlay"></div>
					<div class="hero_content">
						<div class="container container-small">
							<div class="col-md-12 text_center">
								<div class="blog-post-title-center">
									<?php if(!empty($intro_title)){ ?>
										<h1><?php echo $intro_title; ?></h1>
									<?php } ?>
								</div>								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section> 
<?php */ ?>

<?php $simple_content = get_field('simple_content');
if(!empty($simple_content)){ ?>
<section class="gray_bg general-section simple-page-wrapper">
	<div class="container container-small-2"> 
		<div class="row">
			<div class="col-md-12 standard-page">
				<article class="main-article general_content"> <?php // main-article-full ?>
					<?php echo $simple_content; ?>
				</article>
			</div>
		</div>
	</div>
</section>
<?php } ?>

<?php 
$title_section_hierarchy = get_field('title_section_hierarchy');
if( have_rows('hierarchy_flexible') ):
	$count_all = 0;
	$count_sections = 0;  ?>
	<div class="wrapp-icons-list ">
	    <?php while ( have_rows('hierarchy_flexible') ) : the_row(); $count_sections++;

	        if( get_row_layout() == 'white_section' ):
	        	$icons = get_sub_field('icons_white_section');
	        	if(!empty($icons)){	 ?>
		        	<div class="wrapp-icons-list-single white_section_about cancelz">
		        		<?php if($count_sections != 1){ ?>
			        		<div class="position_relative product_triangle_2_parent_parent smalz">
								<div class="product_triangle_2_parent product_triangle_2_parent_transparent" >
									<div class="product_triangle_2"></div>
								</div>
							</div>	
						<?php } ?>
		        		<div class="container largerz container-small-2"> <?php // largerz ?>
							<div class="row">

								<?php if($count_sections == 1){ ?>
									<?php if(!empty($title_section_hierarchy)){ ?>
										<div class="col-md-12 title-section-hierarchy">
											<h2 class="style_h2 centred"><?php echo $title_section_hierarchy; ?></h2>
										</div>
									<?php } ?>
								<?php } ?>
								<div class="col-md-12">

									<?php foreach ($icons as $icon) {
											$icon_type 	= $icon['icon_type'];
											$icon_color = $icon['icon_color'];
											$image_icon = $icon['image_icon'];
											$text_icon  = $icon['text_icon'];
											$text 		= $icon['text'];
											$count_all++;

										?>
										<div class="single-icon">
											<div class="icon-middle">
												<?php switch ($icon_type) {
													case 'image': ?>
														<div class="icon-middle-img" style="background-image: url(<?php echo $image_icon['sizes']['icon_newsletter']; ?>);"></div>	
												<?php break;
													case 'text': ?>
														<div class="icon-middle-text <?php echo $icon_color; ?>"><?php echo $text_icon; ?></div>
												<?php break;
													
												} ?>
											</div>


											<div class="icon-text position-<?php if($count_all % 2){ echo 'right'; } else { echo 'left'; } ?> <?php echo $icon_color; ?>" >
												<div class="wrapp-text"><?php echo $text; ?></div>
											</div>
										</div>
									<?php } ?>

								</div>
							</div>
						</div>	

		        	</div>	
	        	<?php } ?>

	        <?php elseif( get_row_layout() == 'blue_section' ): 

	        	$icons = get_sub_field('icons_blue_section');
	        	if(!empty($icons)){	 ?>
		        	<div class="wrapp-icons-list-single black_bg_dekstop blue_section_about cancelz">
		        		<?php if($count_sections != 1){ ?>
			        		<div class="position_relative product_triangle_3_parent_parent smalz">
								<div class="product_triangle_3_parent" >
									<div class="product_triangle_3"></div>
								</div>
							</div>
						<?php } ?>
		        		<div class="container largerz container-small-2">
							<div class="row">
								<?php if($count_sections == 1){ ?>
									<?php if(!empty($title_section_hierarchy)){ ?>
										<div class="col-md-12 title-section-hierarchy">
											<h2 class="style_h2 centred"><?php echo $title_section_hierarchy; ?></h2>
										</div>
									<?php } ?>
								<?php } ?>
								<div class="col-md-12">

									<?php foreach ($icons as $icon) {
											$icon_type 	= $icon['icon_type'];
											$icon_color = $icon['icon_color'];
											$image_icon = $icon['image_icon'];
											$text_icon  = $icon['text_icon'];
											$text 		= $icon['text'];
											$count_all++;

										?>
										<div class="single-icon">
											<div class="icon-middle">
												<?php switch ($icon_type) {
													case 'image': ?>
														<div class="icon-middle-img" style="background-image: url(<?php echo $image_icon['sizes']['icon_newsletter']; ?>);"></div>	
												<?php break;
													case 'text': ?>
														<div class="icon-middle-text <?php echo $icon_color; ?>"><?php echo $text_icon; ?></div>
												<?php break;
													
												} ?>
											</div>


											<div class="icon-text position-<?php if($count_all % 2){ echo 'right'; } else { echo 'left'; } ?> <?php echo $icon_color; ?>" >
												<div class="wrapp-text"><?php echo $text; ?></div>
											</div>
										</div>
									<?php } ?>

								</div>
							</div>
						</div>

		        	</div>	
	        	<?php } ?>

	        <?php endif;

	    endwhile; ?>
	</div>
<?php endif; ?>


<?php 	$title_section_team = get_field('title_section_team');
		$team = get_field('team'); ?>

<section class="hero blog_hero team_hero">
	<div class="container fix-position-plus">
		<div class="row">
			<?php if(!empty($title_section_team)){ ?>
				<div class="col-md-12 title-team">
					<h2 class="style_h2 text_center"><?php echo $title_section_team; ?></h2>
				</div>
			<?php } ?>

			<div class="row-teams">
			<?php if(!empty($team)){
					$count = 0; $count_row = 1; ?>
					<div class="row-team">
					<?php foreach($team as $member){ 
							$name 		= $member['name'];
							$ocupation 	= $member['ocupation'];
							$image  	= $member['image']; 
							if(!empty($name)){ 
								$count++;  ?>
								<div class="single-team <?php /*if($count_row > (intval(sizeof($team)/4)/2) ) { echo 'second-part'; } */ ?>">
									<div class="single-team-row">
										<div class="single-team-img" style="background-image:url(<?php if(!empty($image)){ echo $image['sizes']['team_icon']; } else { echo get_template_directory_uri(). '/images/no-user.png';  } ?>)"></div>
										<div class="single-team-text">
											<div class="vertical-align">
												<p class="team-name"><?php echo $name; ?></p> 
												<p class="team-ocupation"><?php echo $ocupation; ?></p>
											</div>
										</div>
									</div>
								</div>
								<?php //if($count % 4 == 0){ echo '</div><div class="row-team">';  $count_row++; } ?>
							<?php } ?>
					<?php } ?>
					</div>
			<?php } ?>
			</div>
		</div>
	</div>
	<div class="blue-half">
		<div class="position_relative product_triangle_1_parent_parent">
			<div class="product_triangle_1_parent">
				<div class="product_triangle_1"></div>
			</div>	
		</div>
	</div>
</section>

<?php get_footer();
