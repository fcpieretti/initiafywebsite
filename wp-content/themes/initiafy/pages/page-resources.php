<?php /* Template Name: Page Resource */

//get_header();
get_template_part( 'parts/header-type');
$page_id = get_the_ID(); ?>

<section class="generic-blog-wrapper generic-resources-wrapper">
	<div class="container">
		<div class="row"> 
			<div class="col-md-12 resources-mtitle">
				<h1><?php echo get_the_title(); ?></h1>
			</div> 

			<?php  $terms = get_terms( 'resource-category' );
				if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){ ?>
				<div class="col-md-12 resources-list">
				    <ul>
				    	<?php $resources_page = get_field('resources_page', 'options');
				    	if(!empty($resources_page)){ ?>
				    		<li class="active"><a href="<?php echo get_permalink($resources_page); ?>" title=""><?php _e('All', THEME_TEXT_DOMAIN); ?></a></li>
				    	<?php } ?>
				    	<?php 
				    		foreach ( $terms as $term ) {
					    		$term_link = get_term_link( $term ); ?>
					       		<li><a href="<?php echo $term_link; ?>" title="<?php echo $term->name; ?>"><?php echo $term->name; ?></a></li>
					    <?php  } ?>
				    </ul> 
				</div>
			<?php } ?>
		</div>		

	<?php 
		$no = get_field('number_of_resource_per_page');
		$args =  array( 
            'ignore_sticky_posts' 	=> true, 
            'post_type'           	=> 'resource',
            'order'              	=> 'DESC',
            'posts_per_page'		=> $no
		);   

    	$args['paged'] = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
	 	$loop = new WP_Query( $args ); 
		$count = 0;

 		if ($loop->have_posts()) {  ?>
 			<div class="row">	
 			<?php  while ($loop->have_posts())	{  
 				$loop->the_post(); 
 				$resource_id = get_the_ID();
 				$count++; ?>

	 			<div class="col-md-4 col-sm-12 col-xs-12 single-res">				
					<div class="generic-post-box">
						<div class="generic-featured-post-img" style="background-image: url(<?php 
							$img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'single-small');
							if($img[0]){
								echo $img[0]; 
							} else { 
								echo get_template_directory_uri().'/images/no-post-small.jpg';
							} ?>);"><a href="<?php echo get_permalink($resource_id); ?>" title="<?php echo get_the_title(); ?>"></a></div>
						<article class="post-short-description">
							<h4><a href="<?php echo get_permalink($resource_id); ?>" title="<?php echo get_the_title($resource_id); ?>"><?php  
									$title = get_the_title($resource_id); 
									if(strlen($title) > 70){ echo substr($title, 0, 70).'...'; } else { echo $title; } ?></a></h4>
							<h4 class="tablet-second-title"><a href="<?php echo get_permalink($resource_id); ?>" title="<?php echo get_the_title($resource_id); ?>"><?php echo get_the_title($resource_id); ?></a></h4>
							<i><?php echo get_the_date('d F Y'); ?></i>
							<div class="excerpt-wrapp">
								<p><?php $excerpt = get_the_excerpt($id); if(strlen($excerpt) > 180){ echo substr($excerpt, 0, 180).'...'; } elseif(strlen($excerpt) <= 180 && strlen($excerpt) != 0) { echo $excerpt; } else { echo substr(get_the_content($id), 0, 180); } ?></p>
							</div>
							<?php 
								$terms = get_the_terms( $resource_id, 'resource-category' );
								$draught_ids = array();
								if ( $terms && ! is_wp_error( $terms ) ) : 
								   
								    foreach ( $terms as $term ) {
								        $draught_ids[] = $term->term_id;
								    }
								?>
							<?php endif;  ?>

							<?php 
							if(!empty($draught_ids)){
								$color 			= get_field('color', 'resource-category_' . $draught_ids[0] );
							} else {
								$color 			= '#000';
							}
											
							$button_label 	= get_field('button_label', $resource_id); 
							if(!empty($button_label)){ ?>		
							<div class="button-wrapp-color">	
								<a href="<?php echo get_permalink($resource_id); ?>" data-color="<?php echo $color; ?>" style="<?php echo 'color:'.$color .';' ; echo 'border: 3px solid '. $color .'; '; ?>" class="resources-width-btn resources-hover" ><?php echo $button_label; ?></a>
							</div>
							<?php } ?>
						</article>
					</div>
				</div> 

				<?php //if($count % 3 == 0){ echo '</div><div class="row">'; } ?>

 			<?php }	?>

 			<?php 	$title_request 			= get_field('title_request', $page_id);
 					$subtitle_request 		= get_field('subtitle_request', $page_id);
 					$button_label_request 	= get_field('button_label_request', $page_id);
 					$button_link_request 	= get_field('button_link_request', $page_id);
 					$turn_on 				= get_field('turn_on', $page_id); 

 					if(!empty($turn_on)){
	 					if(!empty($title_request) && !empty($subtitle_request)){  ?>
							<div class="col-md-4 col-sm-6 col-xs-12 post-tablet-hide single-res">
								<div class="demo-request-box">
									<div class="demo-req-wrapper">
										<?php if(!empty($title_request)){ ?>
											<h3><?php echo $title_request; ?></h3>
										<?php } ?>
										<?php if(!empty($subtitle_request)){ ?>
											<h4><?php echo $subtitle_request; ?></h4>
										<?php } ?>

										<?php if(!empty($button_label_request) && !empty($button_link_request)){ ?>
											<a href="<?php echo $button_link_request; ?>" class="green-demo-btn"><?php echo $button_label_request; ?></a>
										<?php } ?>
									</div>
								</div>
							</div>
						<?php } ?>
					<?php } ?>
 
			</div>		
 			
 			<div class="row">
				<div class="col-md-12">
					<div class="text-center">
						<?php /*<a href="" class="more-posts-btn">Load more</a>*/?>
						<?php  wp_pagenavi(array( 'query' => $loop )); ?>
					</div>			

					<?php if(!empty($turn_on)){
							if(!empty($title_request) && !empty($subtitle_request)){ ?>	
								<div class="tablet-mobile-demo-request demo-request-box">
									<div class="demo-req-wrapper">
										<?php if(!empty($title_request)){ ?>
											<h3><?php echo $title_request; ?></h3>
										<?php } ?>

										<?php if(!empty($subtitle_request)){ ?>
											<h4><?php echo $subtitle_request; ?></h4>
										<?php } ?>

										<?php if(!empty($button_label_request) && !empty($button_link_request)){ ?>
											<a href="<?php echo $button_link_request; ?>" class="green-demo-btn"><?php echo $button_label_request; ?></a>
										<?php } ?>
									</div>
								</div>
							<?php } ?>
					<?php } ?>
				</div>
			</div>
		<?php }	?>
		<?php wp_reset_query(); ?>	
	
</section>

<?php get_template_part( 'parts/newsletter'); ?> 

<?php get_footer();
