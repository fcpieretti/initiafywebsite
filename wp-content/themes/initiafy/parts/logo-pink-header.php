<?php 	$logo_svg = get_field('logo_svg', 'options');
		if(!empty($logo_svg)){  ?>
			<a href="<?php echo get_home_url();?>" class="header_logo pink_logo_view">
				<img src="<?php echo $logo_svg['sizes']['logo_size']; ?>" alt="<?php echo get_bloginfo('name'); ?>">
			</a>
<?php } ?>

