<?php /* Template Name: Page Contact */
get_template_part( 'parts/header-type');
// get_header(); ?>

<section class="generic-blog-wrapper generic-resources-wrapper generic-contact-wrapper">
	<div class="red_circle_background white_bg mobile_contact_header">
		<div class="red_circle"></div>
		<?php 	$intro_title = get_field('intro_title');
				if(!empty($intro_title)){ ?>
				<div class="row">
					<div class="col-md-12 text_center">
						<article class="heading-text">
							<h1><?php echo $intro_title; ?></h1>									
						</article>
					</div>
				</div>
			<?php } ?>
	</div>
	
	<?php 	$email_address = get_field('email_address');
			if(!empty($email_address)){ ?>
				<a href="mailto:<?php echo $email_address; ?>" class="contact-welcome-link mobile_link_display"><i class="fa fa-envelope-o" aria-hidden="true"></i> <?php echo $email_address; ?></a>
	<?php } ?>
	<div class="container">
		<div class="row">
			<div class="col-md-7 wrapp-maps">
				<div class="contact-welcome">
					<?php 	$intro_title = get_field('intro_title');
							if(!empty($intro_title)){ ?>
								<h1><?php echo $intro_title; ?></h1>
					<?php } ?>
					<?php 	$email_address = get_field('email_address');
						if(!empty($email_address)){ ?>
							<a href="mailto:<?php echo $email_address; ?>" class="contact-welcome-link desktop_link_display"><i class="fa fa-envelope-o" aria-hidden="true"></i> <?php echo $email_address; ?></a>
					<?php } ?>
				</div>
				<?php $box = get_field('locations'); 
						if(!empty($box)){
							$count = 0; ?>
							<div class="location_row">
							<?php  foreach ($box as $box_item) {
									$location 	= $box_item['location'];
									$phone 		= $box_item['phone']; 
									$address 	= $box_item['address'];
									$location_on_maps = $box_item['location_on_maps']; 
									$count++; ?>
								<div class="col-md-6">
									<div class="map-location-box">
										<div class="map-wrapper">
											<?php  if( !empty($location_on_maps) ): ?>
												<div class="acf-map">
													<div class="marker" data-lat="<?php echo $location_on_maps['lat']; ?>" data-lng="<?php echo $location_on_maps['lng']; ?>"></div>
												</div>
											<?php endif; ?>
										</div>
										<div class="location-info">
											<strong><?php echo $location; ?></strong>
											<a href="tel:<?php echo $phone; ?>"><i class="fa fa-phone" aria-hidden="true"></i> <?php echo $phone; ?></a>
											<a href="https://www.google.com/maps/place/<?php echo str_replace(" ", "+", $address); ?>" " target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo $address; ?></a>
										</div>
									</div>
								</div>
								<?php if($count %2 == 0) { echo '</div><div class="location_row row_remove_margins">'; }?>
							<?php }  ?>
							</div>
					<?php } ?>
			</div>
			
			<?php 	$title_contact_form = get_field('title_contact_form');
					$code_for_contact_form = get_field('code_for_contact_form');

					if(!empty($title_contact_form) || !empty($code_for_contact_form)){ ?>
						<div class="col-md-5 wrapp-cntform">
							<div class="register-form-wrapper contact-submit-form contact-form-wrapp">
								<?php echo $title_contact_form; ?>

								<div class="register-form-wrapper-sec">
									<?php echo $code_for_contact_form; ?>
								</div>
							</div>
						</div>
				<?php } ?>
		</div>
	</div>

	<?php 	$title_contact_form = get_field('title_contact_form');
			$code_for_contact_form = get_field('code_for_contact_form');

			if(!empty($title_contact_form) || !empty($code_for_contact_form)){ ?>
				<div class="mobile_contact_form">
					<div class="register-form-wrapper mobile-submit-form">
						<?php echo $title_contact_form; ?>

						<div class="register-form-wrapper-sec">
							<?php echo $code_for_contact_form; ?>
						</div>
					</div>
				</div>
			<?php } ?>
</section>
<script type="text/javascript">
(function($) {
	function new_map( $el ) {
		var $markers = $el.find('.marker');
		var args = {
			zoom		: 16,
			center		: new google.maps.LatLng(0, 0),
			mapTypeId	: google.maps.MapTypeId.ROADMAP
		};
	   	
		var map = new google.maps.Map( $el[0], args);
		map.markers = [];
		$markers.each(function(){
	    	add_marker( $(this), map );
		});

		center_map( map );
		return map;
	}

	function add_marker( $marker, map ) {
		// var
		var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );
		// create marker
		var marker = new google.maps.Marker({
			position	: latlng,
			map			: map
		});
		// add to array
		map.markers.push( marker );
		// if marker contains HTML, add it to an infoWindow
		if( $marker.html() ){
			// create info window
			var infowindow = new google.maps.InfoWindow({
				content		: $marker.html()
			});
			// show info window when marker is clicked
			google.maps.event.addListener(marker, 'click', function() {
				infowindow.open( map, marker );
			});
		}
	}

	function center_map( map ) {
		// vars
		var bounds = new google.maps.LatLngBounds();
		// loop through all markers and create bounds
		$.each( map.markers, function( i, marker ){
			var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );
			bounds.extend( latlng );
		});
		// only 1 marker?
		if( map.markers.length == 1 ){
			// set center of map
		    map.setCenter( bounds.getCenter() );
		    map.setZoom( 16 );
		}
		else{
			// fit to bounds
			map.fitBounds( bounds );
		}
	}

	// global var
	var map = null;
	$(document).ready(function(){
		$('.acf-map').each(function(){
			// create map
			map = new_map( $(this) );
		});
	});
})(jQuery);
</script>

<?php get_footer();
