<form  method="get" class="mobile-search" action="<?php echo home_url( '/' ); ?>">
    <input type="search" class="search-field" placeholder="<?php _e('Search for...', THEME_TEXT_DOMAIN); ?>" value="<?php echo get_search_query() ?>" name="s"  title="<?php _e('Search for:', THEME_TEXT_DOMAIN); ?>" required />
    <input type="submit" class="search-submit" id="searchsubmit-second-2" value="" />
</form>	