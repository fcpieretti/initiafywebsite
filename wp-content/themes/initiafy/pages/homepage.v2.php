<?php /* Template Name: Homepage Simple V2 */
$header_img_url = ''.get_template_directory_uri().'/images/workers.jpg';
get_header();

$hero_image_fx1 = get_field('hero_image_fx1');
$hero_intro_tex = get_field('hero_intro_text_fx1');
$type_of_hero 	= get_field('type_of_hero'); 

if(!empty($hero_image_fx1) && !empty($hero_intro_tex)){ ?>
	<section class="hero hero_header">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					
					<?php 
					$video = false;
					switch ($type_of_hero) {
						case 'image':
							$video = false;
							break;
						case 'video':
							$video = true;
							break;
					} ?>

					<?php $static_image = get_field('static_image_background'); ?>
					<div class="img_backgroundV2" style="background-image: url('<?php if($video == true){ echo $static_image['sizes']['hero'];  } else { if(!empty($hero_image_fx1)){ echo $hero_image_fx1['sizes']['hero']; } } ?>');" >
						<div class="black_overlay"></div>
						<?php if($video == true){  ?>
							<div class="intro-section">
								<div class="video-background">
				  					<div class="video-foreground">
				  						
										<?php 	$type = get_field('type');
												switch ($type) {
													case 'youtube':

														$video_id = get_field('youtube_id_for_video'); ?>

														<iframe id="video" src="//www.youtube.com/embed/<?php echo $video_id; ?>?enablejsapi=1&html5=1&controls=0&showinfo=0&autohide=1&autoplay=1&rel=0&loop=1&playlist=<?php echo $video_id; ?>" frameborder="0" allowfullscreen></iframe>
														
											<?php	break; ?>
											<?php	case 'vimeo':

														$vide_id = get_field('vimeo_id_for_video'); ?>

														<div class="iframe-wrapper">
											      			<iframe src="//player.vimeo.com/video/<?php echo $vide_id; ?>?api=1&background=1&controls=0&showinfo=0&rel=0&autoplay=1&loop=1&playlist=<?php echo $vide_id; ?>&autohide=1" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen id="video"></iframe>
											      		</div>
														
											<?php	break;
												
												}  ?>
									</div>
								</div>
							</div>
						<?php } ?>

						<div class="hero_content <?php if($video == true) { echo 'with_video'; }?>">
							<div class="container container-small">
								<div class="col-md-12 text_center">
									<?php if(!empty($hero_intro_tex)){ ?>
										<div class="individual-text-heroV2">
											<h1><?php echo $hero_intro_tex; ?></h1>
										</div>
									<?php } ?>

									<?php 	$add_buttons_fx1 = get_field('add_buttons_fx1');
											if(!empty($add_buttons_fx1)) {
												$buttons_fx1 = get_field('buttons_fx1');
												if(!empty($buttons_fx1)){ ?>
													<div class="btn_group">
														<?php foreach($buttons_fx1 as $button){ 
																$button_label 	= $button['button_label'];
																$button_color 	= $button['button_color'];
																$button_type 	= $button['button_type'];

																switch ($button_type) {
																 	case 'video-youtube':
																 		$button_video 	= $button['button_video'];
																 		if(!empty($button_video)){ ?>
																 			
																	 		<a href="#generic_video_popup" class="btn rounded_btn red_btn" data-iziModal-open="#generic_video_popup"><?php echo $button_label; ?></a>
																			
																			<div id="generic_video_popup" class="video_popup_full general_popup_item" data-izimodal-title="" data-izimodal-iframeURL="https://www.youtube.com/embed/<?php echo $button_video; ?>" style="display: none;"></div>

																	 	<?php } ?>

														<?php  			break;
																	case 'video-vimeo':
																		$button_video_vimeo_id =  $button['button_video_vimeo_id'];
																		if(!empty($button_video_vimeo_id)){  ?>

																			<a href="#generic_video_popup" class="btn rounded_btn red_btn btn-why-initiafy-home-page" data-iziModal-open="#generic_video_popup"><?php echo $button_label; ?></a >
																			
																			<div id="generic_video_popup" class="video_popup_full general_popup_item" data-izimodal-title="" data-izimodal-iframeURL="https://player.vimeo.com/video/<?php echo $button_video_vimeo_id; ?>?autoplay=1" style="display: none;"></div>

																		<?php } ?>
														<?php			break;
																 	case 'link':
																 		$button_link 	= $button['button_link'];
																 		if(!empty($button_link)){   ?>
																 			<a href="<?php echo $button_link; ?>" title="" class="btn rounded_btn <?php echo $button_color; ?>_btn btn-request-demo-home-page"><?php echo $button_label; ?></a>
																 		<?php } ?>

														<?php  			break;
																 	default: break;
																} ?>
														<?php } ?>
													</div>
											<?php } ?>
									<?php } ?>
								</div>
							</div>
						</div>
						<div class="scroll_down_arrow_parentV2">
							<div class="container">
								<div class="row">
									<div class="col-md-12 text_center">
										<a href="#first-section" class="scroll_down_arrow">
											<img src="<?php echo get_template_directory_uri();?>/images/scroll_down_arrow.png" alt="Scroll Down">
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php } ?>

<?php 
	$count = 0; 
	if( have_rows('flexible_content') ): 
    while ( have_rows('flexible_content') ) : the_row(); $count++;
        if( get_row_layout() == 'logos' ):
        	$logos = get_sub_field('logos');
        	if(!empty($logos)){   ?>
		        <section <?php if($count==1){ echo 'id="first-section"'; }?> class="partners_logos">
					<div class="container container-small-2">
						<div class="row">
							<div class="col-md-12 list-logos-complete">
								<div class="onload_slider">
									<div class="onload_loading onload_loading_top"> 
										<img src="<?php echo get_template_directory_uri(); ?>/images/ajax-loader.gif" alt="">
									</div>
									<div class="onload_hide">
										<div class="logos-slider">
										<?php foreach( $logos as $logo){ 
												$logo_img 		= $logo['logo'];
												$external_link 	= $logo['external_link'];
												if(!empty($logo_img)){ ?>
													<div class="single-logo text_center text_left_sm">
														<?php if(!empty($external_link)){ ?>
															<a href="<?php echo $external_link; ?>" title="" target="_blank">
														<?php } ?>
															<img src="<?php echo $logo_img['sizes']['logo_partners']; ?>" alt="<?php echo $logo_img['alt']; ?>">
														<?php if(!empty($external_link)){ ?>
															</a>
														<?php } ?>
													</div>	
												<?php } ?>
										<?php } ?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>	
			<?php } ?>
<?php 	elseif( get_row_layout() == 'simple_text' ): 
			$content_fx_2 = get_sub_field('content_fx_2');
			if(!empty($content_fx_2)){ ?>
				<section <?php if($count==1){ echo 'id="first-section"'; } ?> class="why_us_title">
					<div class="container container-small">
						<div class="row">
							<div class="col-md-12 general_content general_style_h2">
								<?php echo $content_fx_2; ?>
							</div>
						</div>
					</div>
				</section>
			<?php } ?>
<?php 	elseif( get_row_layout() == 'section_final_image_left_text_right' ):
				$color_final1	 	= get_sub_field('color_final1');
				$title_final1 		= get_sub_field('title_final1');
				$content_final1 	= get_sub_field('content_final1');
				$image_final1 		= get_sub_field('image_final1');
				$add_button_final1  = get_sub_field('add_button_final1');
				$button_label_final1 = get_sub_field('button_label_final1');
				$button_link_final1 = get_sub_field('button_link_final1');
				$remove_shape_top_final1	= get_sub_field('remove_shape_top_final1');
				$remove_shape_bottom_final1 = get_sub_field('remove_shape_bottom_final1'); ?>

				<section class="section-timg-final type_1">
					<?php if(empty($remove_shape_top_final1)){  ?>
						<div class="product_triangle_1_parent_parent <?php echo $color_final1; ?>_color">
							<div class="product_triangle_3_parent">
								<div class="product_triangle_3"></div>
							</div>
						</div>
					<?php } ?>

					<div class="wrapper-container <?php echo $color_final1; ?>_color_bg <?php if(empty($remove_shape_top_final1)){ echo 'no-margin-top'; } if(empty($remove_shape_bottom_final1)){ echo ' no-margin-bottom'; }   ?>">
						<div class="container container-small-3 text_center">
							<div class="row">
								<div class="col-md-6 vcenter">
									<?php if(!empty($title_final1)){ ?>
										<h3 class="style_h2"><?php echo $title_final1; ?></h3>
									<?php } ?>

									<?php if(!empty($content_final1)){ ?>
									<div class="general_content">
										<?php echo $content_final1; ?>
									</div>
									<?php } ?>

									<?php if(!empty($add_button_final1)){ 
										if(!empty($button_label_final1) && !empty($button_link_final1))?>
										<div class="button-timg">
											<a href="<?php echo $button_link_final1; ?>" class="btn rounded_btn green_btn"><?php echo $button_label_final1; ?></a>
										</div>
									<?php } ?>
								</div>
								<?php if(!empty($image_final1)){  ?>
									<div class="col-md-6 vcenter img-small-round text_centred img_prod">
										<img src="<?php echo $image_final1['sizes']['image_list']; ?>" alt="<?php echo $image_final1['alt']; ?>">
									</div>
								<?php } ?>
								<div class="clear"></div>
							</div>
						</div>
					</div>

					<?php if(empty($remove_shape_bottom_final1)){  ?>
						<div class="first_black_triangle_parent <?php echo $color_final1; ?>_color">
							<div class="first_black_triangle_bg to_bottom">
								<div class="first_white_triangle_bg"></div>
							</div>	
						</div>
					<?php } ?>
				</section>


<?php 	elseif( get_row_layout() == 'section_final_image_right_text_left' ):
				$color_final2 		= get_sub_field('color_final2');
				$title_final2 		= get_sub_field('title_final2');
				$content_final2		= get_sub_field('content_final2'); 
				$image_final2		= get_sub_field('image_final2');
				$add_button_final2 	= get_sub_field('add_button_final2');
				$button_label_final2= get_sub_field('button_label_final2');
				$button_link_final2 = get_sub_field('button_link_final2');
				$remove_shape_top_final2	= get_sub_field('remove_shape_top_final2');
				$remove_shape_bottom_final2 = get_sub_field('remove_shape_bottom_final2'); ?>


				<section class="section-timg-final type_2">
					
					<?php if(empty($remove_shape_top_final2)){  ?>
						<div class="product_triangle_1_parent_parent <?php echo $color_final2; ?>_color">
							<div class="product_triangle_3_parent">
								<div class="product_triangle_3"></div>
							</div>
						</div>
					<?php } ?>

					<div class="wrapper-container <?php echo $color_final2; ?>_color_bg  <?php if(empty($remove_shape_top_final2)){ echo 'no-margin-top'; } if(empty($remove_shape_bottom_final2)){ echo ' no-margin-bottom'; }   ?>">
						<div class="container container-small-3 text_center">
							<div class="row">

								<div class="col-md-6 col-md-push-6 vcenter">
									<?php if(!empty($title_final2)){ ?>
										<h3 class="style_h2"><?php echo $title_final2; ?></h3>
									<?php } ?>

									<?php if(!empty($content_final2)){ ?>
									<div class="general_content">
										<?php echo $content_final2; ?>
									</div>
									<?php } ?>

									<?php if(!empty($add_button_final2)){ 
										if(!empty($button_label_final2) && !empty($button_link_final2))?>
										<div class="button-timg">
											<a href="<?php echo $button_link_final2; ?>" class="btn rounded_btn green_btn btn-learn-more-demo-home-page"><?php echo $button_label_final2; ?></a>
										</div>
									<?php } ?>
								</div>

								<?php if(!empty($image_final2)){  ?>
									<div class="col-md-6 col-md-pull-6 vcenter img-small-round text_centred">
										<img src="<?php echo $image_final2['sizes']['image_list']; ?>" alt="<?php echo $image_final2['alt']; ?>">
									</div>
								<?php } ?>

								
								<div class="clear"></div>
							</div>
						</div>
					</div>

					<?php if(empty($remove_shape_bottom_final2)){  ?>
						<div class="first_black_triangle_parent <?php echo $color_final2; ?>_color">
							<div class="first_black_triangle_bg to_bottom">
								<div class="first_white_triangle_bg"></div>
							</div>	
						</div>
					<?php } ?>
				</section>


<?php 	elseif( get_row_layout() == 'button' ):
				$button_label_fx6 	= get_sub_field('button_label_fx6');
				$title_popup_fx6	= get_sub_field('title_popup_fx6');
				$code 				= get_sub_field('shortcode_for_modal_fx6');  /* NOT done! */ ?>

				<section <?php if($count==1){ echo 'id="first-section"'; }?> class="after_big_section_btn">
					<div class="container">
						<div class="row">
							<div class="col-md-12 text_center demo_req_popup">
								<?php if(!empty($button_label_fx6)){ ?>
									<a href="#resources_form" data-iziModal-open="#modal_reques_form" class="btn rounded_btn blue_btn"><?php echo $button_label_fx6; ?></a>
								<?php } ?>
								
								<?php // <!-- form request popup --> ?>
								<div id="modal_reques_form" class="resources_form general_popup_item" data-izimodal-title="" style="display: none;">
									<div class="modal-req-popup">
										<?php if(!empty($title_popup_fx6)){ ?>
											<h2><?php echo $title_popup_fx6; ?></h2>
										<?php } ?>
										
										<?php if(!empty($code)){ ?>
										<div class="register-form-wrapper-sec">
											<?php echo $code; ?>
										</div>
										<?php } ?>
									</div>
								</div>
								<?php //<!-- //form request popup -->?>
							</div>
						</div>
					</div>
				</section>

	<?php endif;
    endwhile;
endif; ?>

<?php // <!-- End of big section "why us" --> ?>
<?php get_footer();
