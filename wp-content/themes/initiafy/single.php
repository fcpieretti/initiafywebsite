<?php 
$header_transparent = true;
$img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'hero-single');
$project_id = get_the_ID(); 
$header_img_url =  $img[0];
get_header('single');?>

<section class="hero blog_hero hero_header">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				
				<div class="img_background" style="background-image: url('<?php  if($img[0]){ echo $img[0]; } else { $general = get_field('general_top_image', 'options');; if(!empty($general)){ echo $general['sizes']['hero-single'];  }  }?>');" alt="<?php echo get_the_title(); ?>">
					<div class="dark_blue_overlay"></div>
					<div class="hero_content">
						<div class="container container-small">
							<div class="col-md-12 text_center">
								<div class="blog-post-title">
									<h1><?php echo get_the_title(); ?></h1>
									<h6><?php echo get_the_date('d F Y'); ?></h6>
								</div>								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php /*
<section class="tablet-blog-hero">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="mobile-heading-wrapper">
					<h1><?php echo get_the_title(); ?></h1>
					<h6><?php echo get_the_date('d F Y'); ?></h6>		
					
					<div class="mobile-featured-post-img">
						<img src="<?php $img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'hero-single-small'); if($img[0]){ echo $img[0]; } ?>" alt="">
					</div>
					

				</div>
			</div>
		</div>
	</div>
</section>
<?php */ ?>

<section class="article-post">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="js-sticky">
					<ul class="social-icons">
						<li><a href="http://www.facebook.com/share.php?u=<?php echo get_permalink(); ?>&title=<?php echo get_the_title(); ?>" target="_blank" class="facebook-icon"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						<li><a href="http://twitter.com/home?status=<?php echo get_the_title(); ?>+<?php echo get_permalink(); ?>" target="_blank" class="twitter-icon"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
						<li><a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo get_permalink(); ?>&title=<?php echo get_the_title(); ?>&source=<?php echo site_url(); ?>" target="_blank" class="linkedin-icon"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
						<li><a href="https://plus.google.com/share?url=<?php echo get_permalink(); ?>" target="_blank" class="google-icon"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
					</ul>
					<a href="#" class="share-icon"><i class="fa fa-share-alt" aria-hidden="true"></i></a>
				</div>

				<article class="main-article general_content">
					<?php if (have_posts()) : while (have_posts()) : the_post();?>
						<?php the_content(); ?>
        			<?php endwhile; endif; ?>
				</article>

				<div class="js-sticky-end"></div>

				<div class="author-card">
					<div class="author-avatar">
						<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>" title=""><?php echo get_wp_user_avatar(get_the_author_meta('ID'), 90); ?></a>
					</div>
					<div class="about-author">
						<h5>
							<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>"><?php echo nl2br(get_the_author_meta('first_name')).' '.nl2br(get_the_author_meta('last_name')); ?>
							</a>
						</h5>
						<p><?php echo nl2br(get_the_author_meta('description')); ?></p>
					</div>
				</div>

				<?php /*
					$add_banner = get_field('add_banner');
					if(!empty($add_banner)){ 
						$banner_image 	= get_field('banner_image');
						$banner_link 	= get_field('banner_link');
						if(!empty($banner_image) && !empty($banner_link)){	 ?>
							<a href="<?php echo $banner_link; ?>" class="blog-post-card">
								<img src="<?php echo $banner_image['sizes']['banner_post']; ?>" alt="<?php echo $banner_image['alt']; ?>">
							</a>
						<?php } ?>
				<?php } */ ?>

				<div class="mobile-social-share">
					<h3><?php _e('Share this story', THEME_TEXT_DOMAIN); ?></h3>
					<ul class="social-icons">
						<li><a href="http://www.facebook.com/share.php?u=<?php echo get_permalink(); ?>&title=<?php echo get_the_title(); ?>" target="_blank" class="facebook-icon"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						<li><a href="http://twitter.com/home?status=<?php echo get_the_title(); ?>+<?php echo get_permalink(); ?>" target="_blank"  class="twitter-icon"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
						<li><a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo get_permalink(); ?>&title=<?php echo get_the_title(); ?>&source=<?php echo site_url(); ?>" target="_blank"  class="linkedin-icon"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
						<li><a href="https://plus.google.com/share?url=<?php echo get_permalink(); ?>" target="_blank" class="google-icon"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
						<li><a href="#" class="share-icon"><i class="fa fa-share-alt" aria-hidden="true"></i></a></li>
					</ul>
				</div>
			</div>
		</div>

		<div class="row row-posts-recomd">
			<div class="more-posts-thumbs"><?php 
				$custom_taxterms = wp_get_object_terms( $project_id, 'category', array('fields' => 'ids') );
				$args =  array( 
	                'ignore_sticky_posts' 	=> true, 
	                'post_type'           	=> 'post',
	                'order'              	=> 'DESC',
	               // 'orderby'				=> 'rand',
	                'posts_per_page'		=> 3,
	                'post__not_in'			=> array($project_id),
		        	'tax_query' 			=> array(
											    array(
											        'taxonomy' 	=> 'category',
											        'field' 	=> 'id',
											        'terms' 	=> $custom_taxterms
											    )
											)
				); 

			 	$loop = new WP_Query( $args );
			 	$total_loop = $loop->found_posts;	 

	 			$count = 0 ;
	 			if ($loop->have_posts()) {  ?>
	 				<div class="col-md-12">
						<h2><?php _e('You may also like...', THEME_TEXT_DOMAIN); ?></h2>
					</div>
	 				<?php  while ($loop->have_posts())	{
	 					$count++;  
	 					$loop->the_post();
	 					$id = get_the_ID();  ?>
		 				<div class="col-md-3 col-sm-6 col-xs-12 related-article related-article-height <?php if($count == 3){ echo 'hidethisarticle'; } ?>">
							<div class="generic-post-box">
								<a href="<?php echo get_permalink($id); ?>" title="<?php echo get_the_title($id); ?>" class="desktop-hidden-link"></a>
								<div class="generic-featured-post-img" style="background-image: url('<?php   $img = get_field('thumbnail_image',  $id);  
							if(!empty($img)){ echo $img['sizes']['single-small']; } else { echo get_template_directory_uri().'/images/no-post-small.jpg'; } ?>');"><a href="<?php echo get_permalink($id); ?>" title="<?php echo get_the_title($id); ?>"></a></div>
								<article class="post-short-description post-description-height">
									<h4><a href="<?php echo get_permalink($id);?>" title="<?php echo get_the_title($id); ?>"><?php echo get_the_title($id); ?></a></h4>

									<h4 class="tablet-second-title"><a href="<?php echo get_permalink($id);?>" title="<?php echo get_the_title($id); ?>"><?php echo get_the_title($id); ?></a></h4>
									<i><?php echo get_the_date('d F Y'); ?></i>
									<p><?php echo get_the_excerpt($id); ?> <a href="<?php echo get_permalink($id); ?>" title="<?php echo get_the_title($id); ?>" class="rm-post-btn"><?php _e('read more', THEME_TEXT_DOMAIN); ?></a></p>
								</article>
							</div>
						</div>
	 				<?php }	?>
				<?php }	?>
				<?php wp_reset_query(); ?>	

				<?php /*if($total_loop > 3){
					 	$blog_page = get_field('blog_page', 'options');
						if(!empty($blog_page)){ ?>
						<div class="col-md-12 button-blog-mobile">
							<div class="text-center">
								<a href="<?php echo $blog_page; ?>" class="more-posts-btn"><?php _e('Load more', THEME_TEXT_DOMAIN); ?></a>
							</div>	
						</div>
				<?php 	} 
					} */ ?>

				<?php 	$demo_content_article 	= get_field('demo_content_article', 'options');
						$add_button_article 	= get_field('add_button_article', 'options');
						$button_label_article 	= get_field('button_label_article', 'options');
						$button_link_article 	= get_field('button_link_article', 'options');

						if(!empty($demo_content_article)){ ?>
							<div class="col-md-3 col-sm-6 col-xs-12  related-article">
								<div class="demo-request-box">
									<div class="demo-req-wrapper">
										<?php echo $demo_content_article; ?>
										<?php if(!empty($add_button_article)){
												if(!empty($button_label_article) && !empty($button_link_article)){ ?>
													<a href="<?php echo $button_link_article; ?>" class="green-demo-btn"><?php echo $button_label_article; ?></a>
												<?php } ?>
										<?php } ?>
									</div>
								</div>
							</div>
				<?php } ?>
			</div>
		</div>
		
		<?php /*
		<div class="row show-on-desktops">
			<div class="col-md-12">
				<?php if($total_loop > 3){ ?>
					<?php $blog_page = get_field('blog_page', 'options');
						if(!empty($blog_page)){ ?>
							<div class="text-center">
								<a href="<?php echo $blog_page; ?>" class="more-posts-btn"><?php _e('Load more', THEME_TEXT_DOMAIN); ?></a>
							</div>	
						<?php } ?>
						
				<?php } ?>
			</div>
		</div>
		<?php */ ?>
		
	</div>
	<?php get_template_part( 'parts/newsletter'); ?> 
	
</section>
<?php get_footer();

