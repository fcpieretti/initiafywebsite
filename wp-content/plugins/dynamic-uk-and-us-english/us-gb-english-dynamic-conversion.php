<?php
/**
* @package us_gb_Dynamic_C
* @author busblog.net
* @version 1.2
*/

/*
Plugin Name: Dynamic UK and US English
Version: 1.2
Plugin URI: http://busblog.net/934
Author: busblog.net
Author URI: http://busblog.net
Description: This plugin detects a user's language from their browser. If the language is en-gb (British), it will display posts in British English, if it detects any other language it will display posts in US English.
*/

/*
Copyright busblog.net 2011

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: 

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. 

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

if ( !class_exists( 'us_gb_Dynamic' ) ) :
	require_once( 'us-gb-background.php' );
	class us_gb_Dynamic extends USGBEDC {
		// Installation tasks
		public function us_gb_Dynamic() {
			$this->USGBEDC( '1.2', 'us-gb-english-dynamic-conversion', 'ph', __FILE__, array() );
			register_uninstall_hook( __FILE__, array( __CLASS__, 'uninstall' ) );
		}
		// Delete options
		public static function uninstall() {
			delete_option( 'us_gb_d_c' );
		}
		// Override filters
		public function register_filters() {
			$filters = apply_filters( 'us_gb_d_c_filters', array( 'the_content', 'the_excerpt', 'widget_text' ) );
			foreach ( (array) $filters as $filter )
				add_filter( $filter, array( &$this, 'us_gb_Dynamic_C' ), 2 );
			$toptions = $this->get_options();
			if ( apply_filters( 'us_gb_d_c_comments', $toptions['also_comments'] ) ) {
				add_filter( 'get_comment_text', array( &$this, 'us_gb_Dynamic_C' ), 11 );
				add_filter( 'get_comment_excerpt', array( &$this, 'us_gb_Dynamic_C' ), 11 );
			}
		}
		// Initializes configurations
		public function load_config() {
			$this->name = __( 'Dynamic UK and US English' );
			$this->menu_name = __( 'Dynamic UK and US English' );
			$this->config = array(
				'writing_in' => array( 'input' => 'checkbox', 'default' => false,
						'label' => __( 'I\'m writing my posts in US English' ),
						'help' => __( 'Leave unselected if you\'re writing in GB English' )
				),
				'also_comments' => array( 'input' => 'checkbox', 'default' => false,
						'label' => __( 'Comment conversion' ),
						'help' => __( 'Convert commentors content' )
				),
				'case_sensitive' => array( 'input' => 'checkbox', 'default' => false,
						'label' => __( 'Case sensitive' ),
						'help' => __( 'If checked, case will be ignored' )
				),
				'hide_rss' => array( 'input' => 'checkbox', 'default' => false,
						'label' => __( 'Hide the busblog.net RSS feed' ),
						'help' => __( 'Checking this box will hide the RSS feed on this settings page' )
				),
				'con_text' => array( 'input' => 'textarea', 'datatype' => 'hash', 'default' => array(
						"airplane" => "aeroplane",
						"aging" => "ageing",
						"analog" => "analogue",
						"analyze" => "analyse",
						"argument" => "arguement",
						"armor" => "armour",
						"behavior" => "behaviour",
						"catalog" => "catalogue",
						"center" => "centre",
						"clamor" => "clamour",
						"color" => "colour",
						"cozy" => "cosy",
						"counseling" => "counselling",
						"criticize" => "criticise",
						"defense" => "defence",
						"dialog" => "dialogue",
						"emphasize" => "emphasise",
						"encylopedia" => "encylycopaedia",
						"endeavor" => "endeavour",
						"enrollment" => "enrolment",
						"equaling" => "equalling",
						"favor" => "favour",
						"flavor" => "flavour",
						"fueling" => "fuelling",
						"fulfill" => "fulfil",
						"generalize" => "generalise",
						"glamor" => "glamour",
						"gray" => "grey",
						"harbor" => "harbour",
						"vacation" => "holiday",
						"honor" => "honour",
						"homor" => "humour",
						"initialize" => "initialise",
						"jewelry" => "jewellery",
						"judgment" => "judgement",
						"lasagna" => "lasagne",
						"license" => "licence",
						"liter" => "litre",
						"maneuver" => "manoeuvre",
						"medieval" => "mediaeval",
						"memorize" => "memorise",
						"meter" => "metre",
						"modeling" => "modelling",
						"organize" => "organise",
						"oriented" => "orientated",
						"paralyze" => "paralyse",
						"parlor" => "parlour",
						"pretense" => "pretence",
						"pajamas" => "pyjamas ",
						"quarreling" => "quarrelling",
						"recognize" => "recognise",
						"rigor" => "rigour",
						"rumor" => "rumour",
						"savior" => "saviour",
						"savor" => "savour",
						"signaling" => "signalling",
						"skillful" => "skilful",
						"specialty" => "speciality",
						"theater" => "theatre",
						"traveling" => "travelling"
					), 'allow_html' => true, 'no_wrap' => true, 'input_attributes' => 'rows="4" cols="10"',
					'title' => 'US to GB',
					'help' => 'Used if you are writing your posts in US English'
				),
				'con_text_opposite' => array( 'input' => 'textarea', 'datatype' => 'hash', 'default' => array(
						"aeroplane" => "airplane",
						"ageing" => "aging",
						"analogue" => "analog",
						"analyse" => "analyze",
						"arguement" => "argument",
						"armour" => "armor",
						"behaviour" => "behavior",
						"catalogue" => "catalog",
						"centre" => "center",
						"clamour" => "clamor",
						"colour" => "color",
						"cosy" => "cozy",
						"counselling" => "counseling",
						"criticise" => "criticize",
						"defence" => "defense",
						"dialogue" => "dialog",
						"emphasise" => "emphasize",
						"encylycopaedia" => "encylopedia",
						"endeavour" => "endeavor",
						"enrolment" => "enrollment",
						"equalling" => "equaling",
						"favour" => "favor",
						"flavour" => "flavor",
						"fuelling" => "fueling",
						"fulfil" => "fulfill",
						"generalise" => "generalize",
						"glamour" => "glamor",
						"grey" => "gray",
						"harbour" => "harbor",
						"holiday" => "vacation",
						"honour" => "honor",
						"humour" => "homor",
						"initialise" => "initialize",
						"jewellery" => "jewelry",
						"judgement" => "judgment",
						"lasagne" => "lasagna",
						"licence" => "license",
						"litre" => "liter",
						"manoeuvre" => "maneuver",
						"mediaeval" => "medieval",
						"memorise" => "memorize",
						"metre" => "meter",
						"modelling" => "modeling",
						"organise" => "organize",
						"orientated" => "oriented",
						"paralyse" => "paralyze",
						"parlour" => "parlor",
						"pretence" => "pretense",
						"pyjamas" => "pajamas",
						"quarrelling" => "quarreling",
						"recognise" => "recognize",
						"rigour" => "rigor",
						"rumour" => "rumor",
						"saviour" => "savior",
						"savour" => "savor",
						"signalling" => "signaling",
						"skilful" => "skillful",
						"speciality" => "specialty",
						"theatre" => "theater",
						"travelling" => "traveling"
					), 'allow_html' => true, 'no_wrap' => true, 'input_attributes' => 'rows="4" cols="10"',
					'title' => 'GB to US',
					'help' => 'Used if you are writing your posts in British English'
				)
			);
		}
		// Replace text
		public function us_gb_Dynamic_C( $ftext ) {
			// Use this to not convert specific post numbers.
			// $whitelist_post_number = array( 1, 2, 3 );
			// if ( !in_array( get_the_ID(), $whitelist_post_number ) ) {
				$toptions = $this->get_options();
				$old_vals = array( "(", ")", "[", "]", "?", ".", ",", "|", "\$", "*", "^", "+", "{", "}" );
				$new_vals = array( "\(", "\)", "\[", "\]", "\?", "\.", "\,", "\|", "\\\$", "\*", "\^", "\+", "\{", "\}" );
				$con_text = apply_filters( 'us_gb_d_c', $toptions['con_text'] );
				$con_text_opposite = apply_filters( 'us_gb_d_c', $toptions['con_text_opposite'] );
				$writing_in = apply_filters( 'us_gb_d_c_writing_in', $toptions['writing_in'] );
				$case_sensitive = apply_filters( 'us_gb_d_c_case_sensitive', $toptions['case_sensitive'] );
				$is_case = ($case_sensitive) ? 's' : 'si';
				$ftext = ' ' . $ftext . ' ';
				$language = substr( strtolower ( $_SERVER["HTTP_ACCEPT_LANGUAGE"] ) , 0, 5 );
				if ( $language == 'en-gb' ) {
					if ( $writing_in ) {
						if ( !empty( $con_text ) ) {
							foreach ( $con_text as $pre_text => $nex_text ) {
								if ( strpos( $pre_text, '>' ) !== false || strpos( $pre_text, '<' ) !== false ) {
									$ftext = str_replace( $pre_text, $nex_text, $ftext );
								} else {
									$pre_text = str_replace( $old_vals, $new_vals, $pre_text );
									$ftext = preg_replace( "|(?!<.*?)$pre_text(?![^<>]*?>)|$is_case", $nex_text, $ftext );
								}
							}
						}
					}
				} else {
					if ( !$writing_in ) {
						if ( !empty( $con_text_opposite ) ) {
							foreach ( $con_text_opposite as $pre_text => $nex_text ) {
								if ( strpos( $pre_text, '<' ) !== false || strpos( $pre_text, '>' ) !== false ) {
									$ftext = str_replace( $pre_text, $nex_text, $ftext );
								} else {
									$pre_text = str_replace( $old_vals, $new_vals, $pre_text );
									$ftext = preg_replace( "|(?!<.*?)$pre_text(?![^<>]*?>)|$is_case", $nex_text, $ftext );
								}
							}
						}
					}
				}
			// } // End whitelist (remove comment if using post whitelist)
			return trim( $ftext );
		}
	}
	$GLOBALS['us_gb_d_c'] = new us_gb_Dynamic();
endif; // Class exists
?>