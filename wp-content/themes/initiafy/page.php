<?php 
//get_header();
get_template_part( 'parts/header-type'); ?>

<section class="generic-landing-page-wrapper-normal" >
	<div class="container container-small-3"> 
		<div class="row">
			<div class="col-md-12 standard-page">
				<article class="generic-landing-page-txt general_content top-margin-page">
					<?php if (have_posts()) : while (have_posts()) : the_post();?>
						<?php the_content(); ?>
					<?php endwhile; endif; ?>
				</article>
			</div>
		</div>
	</div>
</section>

<?php get_footer();
