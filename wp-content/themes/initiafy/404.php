<?php get_header();?>

<section class="error_wrapper">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="error_title">
					<?php $content_page_404 = get_field('content_page_404', 'options');
					if(!empty($content_page_404)){ echo $content_page_404; } ?>

					<form  method="get"  action="<?php echo home_url( '/' ); ?>">
		                <input type="search" placeholder="Search for..." placeholder="<?php _e('Search for...', THEME_TEXT_DOMAIN); ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php _e('Search for...', THEME_TEXT_DOMAIN); ?>" />
		                <input type="submit"  value="" />
		            </form>

					<?php $content_bottom_page_404 = get_field('content_bottom_page_404', 'options');
					if(!empty($content_bottom_page_404)){ echo $content_bottom_page_404; } ?>
				</div>
			</div>

			<?php $image_page_404 = get_field('image_page_404', 'options');
			if(!empty($image_page_404)){ ?>
			<div class="col-md-6">
				<div class="underconstruction-featured-img">
					<img src="<?php echo $image_page_404['sizes']['not_found']; ?>" alt="<?php echo $image_page_404['alt']; ?>">
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
</section>

<?php get_footer();
