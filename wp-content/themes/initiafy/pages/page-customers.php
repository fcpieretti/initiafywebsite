<?php /* Template Name: Page Customers */
// get_header();
get_template_part( 'parts/header-type');  ?>

<?php // <!-- body here needs to be #f6f6f6, so, I will use this background for sections --> ?>

<?php 	$count_flex  = 0;
 if( have_rows('flexible_content') ):
    while ( have_rows('flexible_content') ) : the_row(); $count_flex++;
        if( get_row_layout() == 'intro_video' ):
        	$title_fx1 = get_sub_field('title_fx1');
        	$content_fx1 = get_sub_field('content_fx1');
        	$image_video_fx1 = get_sub_field('image_video_fx1');
        	$video_id_fx1 = get_sub_field('video_id_fx1');	 ?> 

        	<section <?php if($count_flex==2){ echo 'id="first-section"'; } ?> class="red_circle_background">
				<div class="red_circle"></div>
				<div class="container">
					<div class="row">
						<div class="col-md-12 text_center">
							<?php if(!empty($title_fx1)){  ?>
								<h1><?php echo $title_fx1; ?></h1>
							<?php } ?>
							<?php if(!empty($content_fx1)){ ?>
								<p><?php echo $content_fx1; ?></p>
							<?php } ?>
							
							<?php if(!empty($video_id_fx1)){ ?>
								<div class="video_preview_img">
									<img src="<?php if(!empty($image_video_fx1)){ echo $image_video_fx1['sizes']['video_intro']; } else { echo get_template_directory_uri().'/images/no_video.jpg'; } ?>" alt="<?php echo $image_video_fx1['alt']; ?>">
										<?php /*
											<a data-fancybox href="https://www.youtube.com/watch?v=<?php echo $video_id_fx1; ?>" class="play_video_btn">
												<img src="<?php echo get_template_directory_uri();?>/images/white_play.png">
											</a>
									<?php */ ?> 

									<a href="#generic_video_popup" class="play_video_btn" data-iziModal-open="#generic_video_popup">
										<img src="<?php echo get_template_directory_uri();?>/images/white_play.png" class="btn-play-video-customers-page">
									</a>
									
									<div id="generic_video_popup" class="video_popup general_popup_item" data-izimodal-title="" data-izimodal-iframeURL="<?php echo $video_id_fx1; ?>?autoplay=1">
									</div>


								</div>
							<?php } ?>
							<div>
								<a href="#first-section" class="scroll_down_arrow">
									<img src="<?php echo get_template_directory_uri();?>/images/scroll_down_arrow.png" alt="Scroll Down">
								</a>
							</div>
						</div>
					</div>
				</div>
			</section>
<?php  	elseif( get_row_layout() == 'testimonials' ): ?>

			<?php $posts = get_sub_field('testimonials_fx3');
				if( $posts ):
					$count_each = 0; 
					$count_flex--;
					$total_size = sizeof($posts); //var_dump($total_size); ?>
				   
				    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT)  
			         		setup_postdata($post); 
			         		$count_each++;
			         		$count_flex++; 
			        		$testimonial_id = $post->ID; //get_the_ID(); // var_dump($post);
			        		$type_of_image = get_field('type_of_image', $testimonial_id);
			        		$type_of_image_crop = get_field('type_of_image', $testimonial_id).'_testimonialsecond';
			        		?>

				        	<section <?php if($count_flex==2){ echo 'id="first-section"'; } ?> <?php 	if($count_each == 1){ echo 'class="background_gray_black testimonial_section first-test-section" '; } 
				        					else if($count_each == $total_size ){ echo 'class="background_black testimonial_background_gray_black_tablet_mobile testimonial_section" style="padding-bottom: 0;"'; } 
				        					else { echo 'class="background_black testimonial_section"'; }  ?> >
				        		<?php if($count_each == 1){ ?>
									<div class="first_testimonial_triangle_parent" style="display: none;">
										<div class="first_testimonial_triangle" style="display: none;"></div>
									</div>
								<?php } else if($count_each == $total_size ){ ?>
									<div class="hidden-md hidden-lg">
										<div class="last_testimonial_triangle_parent" style="display: none;">
											<div class="last_testimonial_triangle" style="display: none;"></div>
										</div>
									</div>
								<?php } ?>

								<div class="container container-small-2 container-small-3">
									<div class="row">
										<?php  	$img = wp_get_attachment_image_src(get_post_thumbnail_id(), $type_of_image_crop);
												if($img[0]){
													$image_testimonial = $img[0]; 
												} else {  
													$image_testimonial = get_template_directory_uri().'/images/no-testimonial.png';
												} ?>
										<div class="<?php 
												if($type_of_image == 'large'){ echo 'col-md-4'; if($count_each %2 == 0){ echo ' col-md-push-8'; }  } 
												else if($type_of_image == 'medium'){ echo 'col-md-3'; if($count_each %2 == 0){ echo ' col-md-push-9'; }   } 
												else { echo 'col-md-2'; if($count_each %2 == 0){ echo ' col-md-push-10'; }  }  ?> vcenter2 text_center testimonial_img_wrapp ">
											<img src="<?php echo $image_testimonial; ?>">
										</div>
										<div class="<?php 
												if($type_of_image == 'large'){ echo 'col-md-8'; if($count_each %2 == 0){ echo ' col-md-pull-4'; }  } 
												else if($type_of_image == 'medium'){ echo 'col-md-9'; if($count_each %2 == 0){ echo ' col-md-pull-3'; }   } 
												else { echo 'col-md-10'; if($count_each %2 == 0){ echo ' col-md-pull-2'; } }  ?> vcenter2">
											<div class="testimonial <?php if($count_each %2 == 0){ echo 'testimonial_arrow_on_right'; } else { echo 'testimonial_arrow_on_left'; } ?> text_center">
												<?php echo $post->post_content; ?>
												<span class="testimonial_author">
													<?php echo get_the_title($testimonial_id); ?>
												</span>
												<div class="<?php if($count_each %2 == 0){ echo 'speech_right_arrow'; } else { echo 'speech_left_arrow'; } ?> hidden-xs hidden-sm"></div>
												<div class="speech_top_arrow hidden-md hidden-lg"></div>
											</div>
										</div>
									</div>
								</div>
							</section>

				    <?php endforeach; ?>
				    <?php // <!-- this is for last triangle --> ?>
						<section class="background_gray_black testimonial_section last_testimonial_triangle_parent_parent hidden-xs hidden-sm">
							<div class="last_testimonial_triangle_parent" style="display: none;">
								<div class="last_testimonial_triangle" style="display: none;">
								</div>
							</div>
						</section>
					<?php // <!-- end of big sections group --> ?>
				<?php wp_reset_postdata(); ?>
			<?php endif; ?>

<?php  	elseif( get_row_layout() == 'logos' ):
			$title_section_fx2 = get_sub_field('title_section_fx2');
			$list_of_logos_fx2 = get_sub_field('list_of_logos_fx2');  ?>

      	<section <?php if($count_flex==2){ echo 'id="first-section"'; } ?> class="gray_bg customers_logos customers_logos_inner">
			<div class="container">
				<div class="row">
					<?php if(!empty($title_section_fx2)){ ?>
						<div class="col-md-12 text_center">
							<h2><?php echo $title_section_fx2; ?></h2>
						</div>
					<?php } ?>
					<div class="clear"></div>
					<?php // <!-- use images at 247x131 px --> ?>

					<div class="onload_slider">
						<div class="onload_loading onload_loading_top"> 
							<img src="<?php echo get_template_directory_uri(); ?>/images/ajax-loader.gif" alt="">
						</div>
						<div class="onload_hide">
							<?php if ($list_of_logos_fx2){ ?>
								<?php foreach ($list_of_logos_fx2 as $box1) {
										$logo = $box1['logo'];
										$link = $box1['link'];
										if(!empty($logo)){  ?>					
											<div class="col-xs-3 col-md-2 single-logo-customers">
												<div class="customer_logo">
													<a href="<?php if(!empty($link)){ echo $link; } else { echo '#'; }?>" <?php if(!empty($link)){ echo 'target="_blank"'; } ?> style="background-image: url(<?php echo $logo['url']; ?>);">
														<?php //<img src="<?php echo $logo['url']; /*  " alt="<?php echo $logo['alt'];  ">*?>
													</a>
												</div>
											</div>
										<?php } ?>
								<?php } ?>
							<?php } ?>
						
							<div class="clear"></div>
							<?php if(sizeof($list_of_logos_fx2) > 24){ ?>
								<div class="col-md-12 text_center see_more_btn_parent">
									<a href="#" title="" id="see-more-logos"><?php _e('See more', THEME_TEXT_DOMAIN); ?></a>
								</div>
								<div class="clear"></div>
							<?php } ?>

						</div>
					</div>
					
				</div>
			</div>
		</section>
<?php 	endif;
    endwhile;
endif; ?>

<?php get_footer();