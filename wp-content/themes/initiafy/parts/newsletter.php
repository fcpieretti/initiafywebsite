<?php 	$title_newsletter = get_field('title_newsletter', 'options');
		$code_for_newsletter = get_field('code_for_newsletter', 'options');
		if(!empty($code_for_newsletter)){ ?>
			<div class="newsletter-subscribe-wrapper">
				<div class="container">
					<div class="col-xs-12">
						<div class="generic-subscribe-wrapper">
							<?php if(!empty($title_newsletter)){ ?>
								<h2><?php echo $title_newsletter; ?></h2>
							<?php } ?>
							<div class="news-form">
								<?php echo $code_for_newsletter; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
<?php } ?>