<?php /* 
Template Name: Landing Page 1
Template Post Type: page, resource 
*/
 
//get_header('landing-resources');
get_header('resources-white-landing-resources'); ?>

<section class="red_circle_background landing_page_hero white_bg">
	<div class="red_circle"></div>
</section>

<section class="generic-landing-page-wrapper generic-up-top generic-second-landing customize-landing">
	<div class="container container-small-2"> 
		<div class="row">
			<div class="col-md-7 wrapp-landing-text">
				<div class="large-title-box">
					<h1><?php 	echo get_the_title(); ?></h1>
				</div>

				<?php /*?>
					<?php 	$img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'featured_landing_simple');
							if($img[0]){ ?>
								<div class="featured-img">
									<img src="<?php echo $img[0]; ?>" alt="<?php echo get_the_title(); ?>">
								</div>
					<?php 	} */?>
				
				<article class="generic-landing-page-txt general_content">
					<?php if (have_posts()) : while (have_posts()) : the_post();?>
						<?php the_content(); ?>
					<?php endwhile; endif; ?>
				</article>
			</div>

			<?php 	$title_form = get_field('title_form'); 
					$code_form = get_field('code_form');
					if(!empty($code_form)){  ?>
						<div class="col-md-5 wrapp-landing-img">
							<div class="register-form-wrapper mobile_hide_register_form">
								<?php if(!empty($title_form)){ ?>
									<h2><?php echo $title_form; ?></h2>
								<?php } ?>
								
								<div class="register-form-wrapper-sec">
									<?php echo $code_form; ?> 
								</div>
							</div>
						</div>
			<?php 	} ?>
		</div>
	</div>
</section>

<?php $title_form_2 = get_field('title_form_2');
if(!empty($code_form)){ ?>
	<div class="mobile_contact_form demo_request_form landing_page_contact_form">
		<div class="register-form-wrapper mobile-submit-form">
			<?php if(!empty($title_form_2)){ ?>
				<h2><?php echo $title_form_2; ?></h2>
			<?php } ?>

			<div class="register-form-wrapper-sec">
				<?php echo $code_form; ?>
			</div>
		</div>
	</div>
<?php } ?>

<?php get_footer('simple-dark');
