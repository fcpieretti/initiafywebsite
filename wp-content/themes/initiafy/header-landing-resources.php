<!DOCTYPE html>
<html <?php language_attributes();?> class="no-js">
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php wp_title(); ?></title>
		<?php wp_head(); ?>
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0" />
		<?php $favicon = get_field('favicon_desktop', 'options');
			if($favicon){ ?>
		   		<link rel="icon" href="<?php echo $favicon['url']; ?>" type="<?php echo $favicon['mime_type']; ?>"  sizes="50x50">
		<?php } ?>
		<?php $favicon_iphone = get_field('favicon_iphone', 'options');
		  	if($favicon_iphone){ ?>
		  		<link rel="apple-touch-icon" href="<?php echo $favicon_iphone['url']; ?>"  sizes="60x60">
		<?php } ?>
		<?php $favicon_ipad = get_field('favicon_ipad', 'options');
		  	if($favicon_ipad){ ?>
		   		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo $favicon_ipad['url']; ?>">
		<?php } ?>
		<?php $favicon_iphone_retina = get_field('favicon_iphone_retina', 'options');
		  	if($favicon_iphone_retina){ ?>
		   		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo $favicon_iphone_retina['url']; ?>">
		<?php } ?>
		<?php $favicon_ipad_retina = get_field('favicon_ipad_retina', 'options');
		  	if($favicon_ipad_retina){ ?>
		   		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo $favicon_ipad_retina['url']; ?>">
		<?php } ?>
		<base href="<?php echo get_template_directory_uri() ?>/"><!--[if IE]></base><![endif]-->
		<?php get_template_part( 'parts/zendesk'); ?> 
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-60275625-1"></script>
		<script>
		 window.dataLayer = window.dataLayer || [];
		 function gtag(){dataLayer.push(arguments);}
		 gtag('js', new Date());

		 gtag('config', 'UA-60275625-1');
		</script>
		<!-- Hotjar Tracking Code for http://www.initiafy.com/ -->
		<script>
		   (function(h,o,t,j,a,r){
			   h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
			   h._hjSettings={hjid:388685,hjsv:6};
			   a=o.getElementsByTagName('head')[0];
			   r=o.createElement('script');r.async=1;
			   r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
			   a.appendChild(r);
		   })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
		</script>
	</head>
	<body <?php body_class();?>>
		<?php  
		global $header_img_url;	
		$res_id = get_the_ID();
		$hide_logo = get_field('hide_logo', $res_id);

		$header_class = 'transparent landing_page';?>
		<header class="<?php echo $header_class;?>">
			<?php if(empty($hide_logo)){  ?>
				<div class="header_bg_parent"> 
					<div class="dark_blue_overlay landing_page_white"></div>
				</div>
			<?php } ?>
			 
			<?php // <!-- this is the header content for +1200px --> ?>
			<div class="container">
				<div class="row">
					<div class="col-md-12 table_parent">
						<div class="table_cell text_center">
							<?php 
								if(empty($hide_logo)){ ?>
								<a href="<?php echo get_home_url();?>" class="header_logo resources_pink_logo">
									<?php get_template_part( 'parts/logo-pink-simple'); ?> 
								</a>
							<?php } else { ?>
								<div class="hide_logo_sec"></div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
			<?php // <!-- end of header for +1200px --> ?>
		</header>
