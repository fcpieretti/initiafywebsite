<?php 	$logo_svg = get_field('logo_svg', 'options');
		if(!empty($logo_svg)){  ?>
			<div class="mm-logo">
				<a href="<?php echo get_home_url();?>" title="<?php echo get_bloginfo('name');?>">
					<img src="<?php echo $logo_svg['sizes']['logo_size']; ?>" alt="<?php echo get_bloginfo('name');?>">
				</a>
			</div>
		<?php } ?>	