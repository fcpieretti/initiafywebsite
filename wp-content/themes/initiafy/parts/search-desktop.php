<div class="desktop-search-field">
	<form  method="get" class="searchform" action="<?php echo home_url( '/' ); ?>">
		<span class="closeme"><i class="fa fa-times"></i></span>
        <input type="search" class="search-field" placeholder="<?php _e('Search', THEME_TEXT_DOMAIN); ?>" value="<?php echo get_search_query() ?>" name="s"  title="<?php _e('Search for:', THEME_TEXT_DOMAIN); ?>" required />
        <input type="submit" class="search-submit" id="searchsubmit-second" value=""  />
    </form>	
</div>