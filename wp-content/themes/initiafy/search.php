<?php get_header(); ?>

<section class="search_results_container">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
			<?php 	global $wp_query;
					$searched_value = strip_tags(get_search_query());
					$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
					$query_array = query_posts( array_merge( $wp_query->query_vars, array( 'ignore_sticky_posts' => true, 'paged' => $paged) ) ); ?>

			<?php if (have_posts())	: $x = 0; ?>
				<div class="search_results_wrapper">
					<div class="title-search">
						<?php /* Search Count */ 
							//$allsearch = &new WP_Query("s=$s&showposts=-1"); 
							//echo "<pre>";
							//var_dump( ($wp_query->found_posts  ));
							//$key = wp_specialchars($s, 1); 
							$count = $wp_query->found_posts ; //$allsearch->post_count; ?>
						<h1><?php echo $count;  if($count == 1){ _e(' Result for: ', THEME_TEXT_DOMAIN);  } else { _e(' Results for: ', THEME_TEXT_DOMAIN); }  ?>  <?php the_search_query(); ?></h1>
					</div>

					<div class="listing-search-results">
						<?php while (have_posts())	: the_post(); $id = get_the_ID(); ?>

							<div class="generic-result-box">
								<div class="search-result-title">
									<?php $title_result = get_the_title($id); ?>
									<a href="<?php echo get_permalink( $id ); ?>" class="mark-result"><?php // echo get_the_title($id); 
										$title_result = preg_replace("/($searched_value)/i","<span>$1</span>",$title_result);
                                        echo $title_result; 
									?></a>
									<i><?php echo get_the_date('d F Y'); ?></i>
								</div>

								<p class="mark-result"><?php
										$content_page = get_the_content(); 	
									 	if(strlen(strip_tags($content_page))>110) {
									 		$content_page_substring = substr(strip_tags($content_page), 0, 110)."..."; 
											$content_page_substring = preg_replace("/($searched_value)/i","<span>$1</span>",$content_page_substring);
                                        	echo $content_page_substring;
									 	} else {
									 		$content_page = strip_tags($content_page);
									 		$content_page = preg_replace("/($searched_value)/i","<span>$1</span>",$content_page);
									 		echo $content_page;
									 		
									 	} 
									?><a href="<?php echo get_permalink($id); ?>" class="search-more-btn"><?php _e('read more', THEME_TEXT_DOMAIN); ?></a></p>
							</div>
						<?php endwhile; ?>
					</div>
					<div class="navigation">
						<div class="wp-pagenavi"><?php
							global $wp_query;
							$big = 999999999; 
							echo paginate_links( array(
								'base' 			=> str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
								'format' 		=> '?paged=%#%',
								'current' 		=> max( 1, get_query_var('paged') ),
								'total' 		=> $wp_query->max_num_pages,
								'type' 			=> 'list',
								'prev_next'    	=> True,
								'prev_text'    	=> "«" ,
								'next_text'    	=> "»" 
									
							) );
						?></div>
					</div>
				</div>
				<?php else : ?>	
					<?php $no_result_message = get_field('no_result_message', 'options');
					if(!empty($no_result_message)){ ?>

						<div class="no-items-available">
							<div class="row">
								<div class="col-md-6 no-results-text">
									<?php echo $no_result_message; ?>

									<div class="form-new-search">
										<form  method="get" class="searchform" action="<?php echo home_url( '/' ); ?>">
									        <input type="search" class="search-field" placeholder="<?php _e('Search', THEME_TEXT_DOMAIN); ?>" value="<?php echo get_search_query() ?>" name="s"  title="<?php _e('Search for:', THEME_TEXT_DOMAIN); ?>" />
									        <input type="submit" class="search-submit" id="searchsubmit-second" value="" style="display: none;" />
									    </form>	
									</div>

								</div>
								<div class="col-md-6 no-results-image">
									<?php 	$no_result_image = get_field('no_result_image', 'options');
											if(!empty($no_result_image)){ ?>
												<div class="no-items-available-image">
													<img src="<?php echo $no_result_image['sizes']['not_found']; ?>" alt="<?php echo $no_result_image['alt']; ?>">
												</div>
									<?php } ?>
								</div>
							</div>	
						</div>	

					<?php } ?>
				<?php endif; ?>		
		
			</div>
		</div>
	</div>
</section>

<?php get_footer();
