<?php /* Template Name: Page Thank you Newsletter */

get_header();?>

<section class="newsletter-thank-you">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<?php $intro_text = get_field('intro_text');
				if(!empty($intro_text)){ ?>
					<article class="nw-final-message">
						<?php echo $intro_text; ?>
					</article>
				<?php } ?>

				<?php 	$title_social = get_field('title_social', 'options');
						$social_list = get_field('social_list', 'options');
						if(!empty($social_list)){ ?>
							<div class="social-newslewtter-follow-us">
								<h2><?php echo $title_social; ?></h2>
								<ul class="newsletter_social">
									<?php foreach($social_list as $social_item){
											$icon = $social_item['icon'];
											$link = $social_item['link'];
											
											if(!empty($icon) && !empty($link)){  ?>
												<li>
													<a href="<?php echo $link; ?>" title="" target="_blank"><?php echo $icon; ?></a>
												</li>
											<?php } ?>
									<?php } ?>
								</ul>
							</div>
					<?php } ?>
			</div>
		</div>
		
		<?php 
			$args =  array( 
                'ignore_sticky_posts' 	=> true, 
                'post_type'           	=> 'post',
                'order'              	=> 'DESC',
                'orderby'        		=> 'rand',
                'posts_per_page'		=> 3
			);   

		 	$loop = new WP_Query( $args ); 
 			$count = 1 ;
 			if ($loop->have_posts()) {  ?>

				<div class="more-posts-thumbs newsletter-more-posts">
					<div class="row">
						<div class="col-md-12">
							<h3><?php _e('You may also like...', THEME_TEXT_DOMAIN); ?></h3>
						</div>
		 				<?php  while ($loop->have_posts())	{  $loop->the_post(); 
		 						$id = get_the_ID(); ?>

			 				<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="generic-post-box">
									<a href="<?php echo get_permalink($id); ?>" title="<?php echo get_the_title($id); ?>" class="desktop-hidden-link"></a>
									<div class="generic-featured-post-img" style="background-image: url('<?php $img = wp_get_attachment_image_src(get_post_thumbnail_id($id), 'single-small'); if($img[0]){ echo $img[0]; } else { echo get_template_directory_uri().'/images/no-post-small.jpg'; } ?>');">
										<a href="<?php echo get_permalink($id); ?>" title="<?php echo get_the_title($id); ?>" ></a>
									</div>
									<article class="post-short-description">
										<h4><a href="<?php echo get_permalink($id); ?>" title="<?php echo get_the_title($id); ?>"><?php echo get_the_title($id); ?></a></h4>
										<h4 class="tablet-second-title"><a href="<?php echo get_permalink($id); ?>" title="<?php echo get_the_title($id); ?>"><?php echo get_the_title($id); ?></a></h4>
										<i><?php echo get_the_date('d F Y'); ?></i>
										<p><?php if(strlen(strip_tags(get_the_content()))>110) 
													echo substr(strip_tags(get_the_content()), 0, 110)."..."; 
												else echo strip_tags(get_the_content());
											?><a href="<?php echo get_permalink($id); ?>" class="rm-post-btn"><?php _e('read more', THEME_TEXT_DOMAIN); ?></a></p>
									</article>
								</div>
							</div>
		 				<?php }	?>
		 			</div>
				</div>
		<?php }	?>
		<?php wp_reset_query(); ?>	
	</div>
</section>

<?php get_footer();
