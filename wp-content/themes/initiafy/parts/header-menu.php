<?php
	$menu = wp_nav_menu(array(
		'theme_location'  	=> 'header_menu',
		'container'      	=> '',
		'menu_class'		=> 'header-menu',
		'container_class' 	=> 'header-menu',
		'container_id'    	=> '',
		'echo'            	=> true,
		'items_wrap'      	=> '<ul class="%2$s">%3$s</ul>',
		'depth'           	=> 2
	));
?>