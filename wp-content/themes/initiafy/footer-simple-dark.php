<?php if( have_rows('footer_larger_screens' , 'options') ):
	 	while ( have_rows('footer_larger_screens' , 'options') ) : the_row();
	        if( get_row_layout() == 'social_list_copyright_and_links' ):
	        	$copy = get_sub_field('copyright_message_fx_1'); 
	        	if(!empty($copy)){ ?>
	        		<div class="dark-footer-wrapper">
						<div class="container">
							<div class="row">
								<div class="col-md-12">
									<footer class="simple-footer">
										<p><?php echo $copy; ?></p>
									</footer>
								</div>
							</div>
						</div>
					</div>
				<?php } ?>
        	<?php endif;
   		endwhile;
	endif; ?>
	<?php //get_template_part( 'parts/popup-newsletter'); ?> 
	<?php wp_footer(); ?>
</body>
</html>
