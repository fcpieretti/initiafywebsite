<?php 
	
	if(is_tax('resource-category')) {
		$queried_object = get_queried_object();
		$term_id_current = $queried_object->term_id;
		
		$add_top_image_header 	= get_field('add_top_image_header', 'resource-category_' . $term_id_current );
		$intro_image 			= get_field('image_hader', 'resource-category_' . $term_id_current);
		$intro_title			= get_field('content_header', 'resource-category_' . $term_id_current);

	} else {
		$add_top_image_header 	= get_field('add_top_image_header');
		$intro_image 			= get_field('image_hader');
		$intro_title			= get_field('content_header');
	}
	

	if(!empty($add_top_image_header)) {

		get_header('single'); ?>

		<section class="hero blog_hero about_hero hero_header">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">
						<div class="img_background" style="background-image: url('<?php  if(!empty($intro_image)){ echo $intro_image['sizes']['hero-single']; } ?>');" alt="<?php echo get_the_title(); ?>">
							<div class="dark_blue_overlay"></div>
							<div class="hero_content">
								<div class="container container-small">
									<div class="col-md-12 text_center">
										<div class="blog-post-title-center">
											<?php if(!empty($intro_title)){ ?>
												<div class="general_hero_text"><?php echo $intro_title; ?></div>
											<?php } ?>
										</div>								
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

	<?php } else {

		get_header();

	}

?>