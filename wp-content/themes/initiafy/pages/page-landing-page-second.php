<?php /* 
Template Name: Landing Page 2
Template Post Type: page, resource 
 */
 
//get_header('dark-landing-page'); 
//2 get_header('landing-resources');
// get_header('resources-dark-landing-resources');

get_header('single');

$res_id = get_the_ID();
$hide_logo = get_field('hide_logo', $res_id); ?>

<section class="hero blog_hero hero_header ">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="img_background" style="background-image: url('<?php  $img = get_field('background_image_ld2'); if(!empty($img)){ echo $img['sizes']['featured_large_image']; } else { $general = get_field('general_top_image', 'options');; if(!empty($general)){ echo $general['sizes']['hero-single'];  }  }?>');" alt="<?php echo get_the_title(); ?>">
					<div class="dark_blue_overlay"></div>
					<div class="hero_content">
						<div class="container container-small">
							<div class="col-md-12 text_center">
								<div class="blog-post-title">
									<h1><?php echo get_the_title(); ?></h1>
								</div>								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<?php /* // desktop_demo_req_hide ?>
<div class="landing_hero_page " style="<?php if(!empty($hide_logo)){ echo 'top:0;'; } ?> background-image: url(<?php $img = get_field('background_image_ld2'); if(!empty($img)){ echo $img['sizes']['featured_large_image']; } //wp_get_attachment_image_src(get_post_thumbnail_id(), 'featured_large_image'); if($img[0]){ echo $img[0]; } ?>);">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<article class="landing-hero-title">
					<h1><?php echo get_the_title(); ?></h1>
				</article>
			</div>
		</div>
	</div>
</div>
<?php */ ?>
<?php 	$title_form = get_field('title_form');
		$code_form_2 = get_field('code_form_2');  
?> 
 
<section class="generic-lp-wrapper-box customize-landing generic-landing-2"> <?php // desktop_demo_req_hide ?>
	<div class="container container-small-2">
		<div class="row">
			<div class="col-md-<?php if(empty($code_form_2)){ echo '12'; } else { echo '7 wrapp-landing-text'; }?> ">
				<article class="lp-wrapper-content general_content general_content_small_list">
					<?php if (have_posts()) : while (have_posts()) : the_post();?>
						<?php the_content(); ?>
					<?php endwhile; endif; ?>
				</article>
			</div>

			<?php 	

				if(!empty($code_form_2)){ ?>
					<div class="col-md-5 wrapp-landing-img">
						<div class="register-form-wrapper">
							<?php if(!empty($title_form)){ ?>
								<h2><?php echo $title_form; ?></h2>
							<?php } ?>

							<div class="register-form-wrapper-sec">
								<?php echo $code_form_2; ?>
							</div>
						</div>
					</div>
				<?php } ?>
		</div>
	</div>
</section>

<?php /*
<section class="mobile_demo_req_show">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="mobile-demo-request-heading">
					<h1><?php echo get_the_title(); ?></h1>
					<div class="general_content general_content_white">
						<?php if (have_posts()) : while (have_posts()) : the_post();?>
							<?php the_content(); ?>
						<?php endwhile; endif; ?>
					</div>
				</div>

				<?php if(!empty($code_form_2)){  ?>				
					<div class="register-form-wrapper">
						<?php if(!empty($title_form)){ ?>
							<h2><?php echo $title_form; ?></h2>
						<?php } ?>

						<div class="register-form-wrapper-sec">
							<?php echo $code_form_2; ?>
						</div>
					</div>
				<?php } ?>				
			</div>
		</div>
	</div>
</section>
<?php */?>

<?php get_footer('simple'); // get_footer('simple-dark');
