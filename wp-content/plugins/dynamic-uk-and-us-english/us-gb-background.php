<?php
/**
* @package US_GB_Background
* @author busblog.net
* @version 002
*/

/*

Thanks to http://wordpress.org/extend/plugins/text-replace/ for table concept

Copyright busblog.net 2011

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: 

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. 

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
*/

if ( !class_exists( 'USGBEDC' ) ) :
	abstract class USGBEDC {
		protected $plugin_css_version = '001';
		protected $toptions = array();
		protected $saved_options = false;
		protected $toption_names = array();
		protected $req_config = array( 'menu_name', 'name' );
		protected $saved_options_msg = '';
		// Installation tasks
		public function USGBEDC( $version, $id_base, $author_prefix, $file, $plugin_options = array() ) {
			global $pagenow;
			$id_base = sanitize_title( $id_base );
			if ( !file_exists( $file ) )
				die( sprintf( __( 'Invalid specified for bb_Plugin: %s' ), $file ) );
			$u_id_base = str_replace( '-', '_', $id_base );
			$author_prefix .= '_';
			$defaults = array(
				'admin_options_name'		=> $author_prefix . $u_id_base,
				'settings_page'				=> 'options-general',
				'show_admin'				=> true,
				'config'					=> array(),
				'disable_contextual_help'	=> true,
				'disable_update_check'		=> false,
				'hook_prefix'				=> $u_id_base . '_',
				'form_name'					=> $u_id_base,
			);
			$settings = wp_parse_args( $plugin_options, $defaults );
			foreach ( array_keys( $defaults ) as $key )
				$this->$key = $settings[$key];
			$this->id_base				= $id_base;
			$this->author_prefix		= $author_prefix;
			$this->options_page			= '';
			$this->plugin_basename		= plugin_basename( $file );
			$this->plugin_path			= plugins_url( '', $file );
			$this->plugin_file			= $file;
			$this->u_id_base			= $u_id_base; 
			$this->version				= $version;
			add_action( 'init', array( &$this, 'init' ) );
			$plugin_file = implode( '/', array_slice( explode( '/', $this->plugin_basename ), -2 ) );
			add_action( 'activate_' . $plugin_file, array( &$this, 'install' ) );
			add_action( 'deactivate_' . $plugin_file, array( &$this, 'deactivate' ) );
			add_action( 'admin_init', array( &$this, 'init_options' ) );
			if ( basename( $pagenow, '.php' ) == $this->settings_page ) {
				add_action( 'admin_head', array( &$this, 'add_header_admin_css' ) );
			}
		}
		
		// Load config
		public function init() {
			global $bb_plugin_max_css_version;
			if ( !isset( $bb_plugin_max_css_version ) || ( $bb_plugin_max_css_version < $this->plugin_css_version ) )
				$bb_plugin_max_css_version = $this->plugin_css_version;
			$this->load_config();
			$this->verify_config();
			if ( $this->disable_update_check )
				add_filter( 'http_request_args', array( &$this, 'disable_update_check' ), 5, 2 );
			if ( $this->show_admin && $this->settings_page && !empty( $this->config ) && current_user_can( 'manage_options' ) ) {
				add_action( 'admin_menu', array( &$this, 'admin_menu' ) );
			}
			$this->register_filters();
		}
		// Install
		public function install() {
			$this->options = $this->get_options();
			update_option( $this->admin_options_name, $this->options );
		}
		// Deactivate
		public function deactivate() { }
		// Disable plugin update check
		public function disable_update_check( $r, $url ) {
			if ( strpos( $url, 'http://api.wordpress.org/plugins/update-check' ) != 0 ) {
				return $r;
			}
			$plugins = unserialize( $r['body']['plugins'] );
			unset( $plugins->plugins[ plugin_basename( __FILE__ ) ] );
			unset( $plugins->active[ array_search( plugin_basename( __FILE__ ), $plugins->active ) ] );
			$r['body']['plugins'] = serialize( $plugins );
			return $r;
		}
		// Initialises options
		public function init_options() {
			register_setting( $this->admin_options_name, $this->admin_options_name, array( &$this, 'sanitise_inputs' ) );
			add_settings_section( 'default', '', array( &$this, 'draw_default_section' ), $this->plugin_file );
			add_filter( 'whitelist_options', array( &$this, 'whitelist_options' ) );
			foreach ( $this->get_option_names( false ) as $topt )
				add_settings_field( $topt, $this->get_option_label( $topt ), array( &$this, 'display_option' ), $this->plugin_file, 'default', $topt );
		}
		// Whitelist options
		public function whitelist_options( $toptions ) {
			$added = array( $this->admin_options_name => array( $this->admin_options_name ) );
			$toptions = add_option_whitelist( $added, $toptions );
			return $toptions;
		}
		// Draw
		public function draw_default_section() { }
		// Get label
		public function get_option_label( $topt ) {
			return isset( $this->config[$topt]['label'] ) ? $this->config[$topt]['label'] : '';
		}
		// Check inputs
		public function sanitise_inputs( $inputs ) {
			do_action( $this->get_hook( 'before_save_options' ), $this );
			if ( isset( $_POST['Reset'] ) ) {
				$toptions = $this->reset_options();
				add_settings_error( 'general', 'settings_reset', __( 'Settings reset.' ), 'updated' );
			} else {
				$toptions = $this->get_options();
				$toption_names = $this->get_option_names();
				foreach ( $toption_names as $topt ) {
					if ( !isset( $inputs[$topt] ) ) {
						if ( $this->config[$topt]['input'] == 'checkbox' )
							$toptions[$topt] = '';
						elseif ( ( $this->config[$topt]['required'] === true ) ) {
							$msg = sprintf( __( 'A value is required for: "%s"' ), $this->config[$topt]['label'] );
							add_settings_error( 'general', 'setting_required', $msg, 'error' );
						}
					}
					else {
						$val = $inputs[$topt];
						$error = false;
						if ( empty( $val ) && ( $this->config[$topt]['required'] === true ) ) {
							$msg = sprintf( __( 'A value is required for: "%s"' ), $this->config[$topt]['label'] );
							$error = true;
						} else {
							$input = $this->config[$topt]['input'];
							switch ( $this->config[$topt]['datatype'] ) {
								case 'checkbox':
									break;
								case 'int':
									if ( !empty( $val ) && ( !is_numeric( $val ) || ( intval( $val ) != round( $val ) ) ) ) {
										$msg = sprintf( __( 'Expected integer value for: %s' ), $this->config[$topt]['label'] );
										$error = true;
										$val = '';
									}
									break;
								case 'array':
									if ( empty( $val ) )
										$val = array();
									elseif ( $input == 'text' )
										$val = explode( ',', str_replace( array( ', ', ' ', ',' ), ',', $val ) );
									else
										$val = array_map( 'trim', explode( "\n", trim( $val ) ) );
									break;
								case 'hash':
									if ( !empty( $val ) && $input != 'select' ) {
										$new_values = array();
										foreach ( explode( "\n", $val ) as $line ) {
											list( $shortcut, $ftext ) = array_map( 'trim', explode( "=>", $line, 2 ) );
											if ( !empty( $shortcut ) )
												$new_values[str_replace( '\\', '', $shortcut )] = str_replace( '\\', '', $ftext );
										}
										$val = $new_values;
									}
									break;
							}
						}
						if ( $error )
							add_settings_error( 'general', 'setting_not_int', $msg, 'error' );
						$toptions[$topt] = $val;
					}
				}
				$toptions = apply_filters( $this->get_hook( 'before_update_option' ), $toptions, $this );
			}
			$toptions['_version'] = $this->version;
			return $toptions;
		}
		
		// Reset
		public function reset_options() {
			$toptions = $this->get_options( false );
			return $toptions;
		}
		// Abstract load
		abstract protected function load_config();
		
		// Plugin specific
		protected function get_hook( $hook ) {
			return $this->hook_prefix . '_' . $hook;
		}
		// Verify config
		protected function verify_config() {
			foreach ( $this->req_config as $config ) {
				if ( empty( $this->$config ) )
					die( "The plugin configuration option '$config' must be supplied." );
			}
			if ( empty( $this->config ) )
				$this->show_admin = false;
			else {
				foreach ( $this->get_option_names( true ) as $topt ) {
					foreach ( array( 'datatype', 'default', 'input', 'input_attributes', 'options', 'output', 'help', 'required' ) as $attrib ) {
						if ( !isset( $this->config[$topt][$attrib] ) )
							$this->config[$topt][$attrib] = '';
					}
					$this->config[$topt]['allow_html'] = false;
					$this->config[$topt]['class'] = array();
				}
			}
		}
		// CSS Header
		public function add_header_admin_css() {
		
			global $bb_plugin_max_css_version, $bb_plugin_css_out;
			if ( ( $bb_plugin_max_css_version != $this->plugin_css_version ) || ( isset( $bb_plugin_css_out ) && $bb_plugin_css_out ) )
				return;
			$bb_plugin_css_out = true;
			echo "<style type=\"text/css\">
			.form-table th {
			width:250px;
			}
			.wrap {
			margin-bottom:30px !important;
			}
			.bb-form h3 {
			display:none;
			}
			.bb-input-help {
			color:#777;
			font-size:11px;
			}
			h3 {
			cursor:default !important;
			}
			.breakp {
			margin-bottom:5px;
			}
			p {
			padding:2px 10px;
			}
			.bb-input-title {
			color:#444;
			font-size:22px;
			}
			.bb-textarea,.bb-inline_textarea {
			width:60%;
			margin-right:30px;
			margin-top:-10px;
			float:left;
			}
			#controls {
			margin-top:20px;
			}
			.postbox {
			margin:5px 0 2px;
			}
			#version {
			float:right;
			color:#777;
			}
			</style>";
		}
		// Settings link
		public function admin_menu() {
			add_filter( 'plugin_action_links_' . $this->plugin_basename, array( &$this, 'plugin_action_links' ) );
			switch ( $this->settings_page ) {
				case 'options-general' :
					$function_root = 'options';
					break;
				case 'themes' :
					$function_root = 'theme';
					break;
				default :
					$function_root = $this->settings_page;
			}
			$menu_function = 'add_' . $function_root . '_page';
			if ( function_exists( $menu_function ) ) {
				$this->options_page = call_user_func( $menu_function, $this->name, $this->menu_name, 'manage_options', $this->plugin_basename, array( &$this, 'options_page' ) );
			}
		}
		
		// Register filters
		public function register_filters() { }
		// Settings link
		public function plugin_action_links( $act_address ) {
			$settings_link = '<a href="' . $this->settings_page . '.php?page='.$this->plugin_basename.'">' . __( 'Settings' ) . '</a>';
			array_unshift( $act_address, $settings_link );
			return $act_address;
		}
		// Is option valid
		protected function is_option_valid( $topt ) {
			global $wp_version;
			$valid = true;
			$ver_opter = array( 'wpgt' => '>', 'wpgte' => '>=', 'wplt' => '<', 'wplte' => '<=' );
			foreach ( $ver_opter as $ver_check => $ver_opt ) {
				if ( isset( $this->config[$topt][$ver_check] )
					&& !empty( $this->config[$topt][$ver_check] )
					&& !version_compare( $wp_version, $this->config[$topt][$ver_check], $ver_opt ) ) {
						$valid = false;
						break;
				}
			}
			return $valid;
		}
		// Option names
		protected function get_option_names( $include_non_options = false ) {
			if ( !$include_non_options && !empty( $this->option_names ) )
				return $this->option_names;
			if ( $include_non_options )
				return array_keys( $this->config );
			$this->option_names = array();
			foreach ( array_keys( $this->config ) as $topt ) {
				if ( isset( $this->config[$topt]['input'] ) && $this->config[$topt]['input'] != '' && $this->config[$topt]['input'] != 'none' && $this->is_option_valid( $topt ) )
					$this->option_names[] = $topt;
			}
			return $this->option_names;
		}
		// Options
		protected function get_options( $with_current_values = true ) {
			if ( $with_current_values && !empty( $this->options ) ) {
				return $this->options;
			}
			$toptions = array();
			$toption_names = $this->get_option_names( !$with_current_values );
			foreach ( $toption_names as $topt ) {
				$toptions[$topt] = $this->config[$topt]['default'];
			}
			if ( !$with_current_values ) {
				return $toptions;
			}
			$this->options = wp_parse_args( get_option( $this->admin_options_name ), $toptions );
			foreach ( $toption_names as $topt ) {
				if ( $this->config[$topt]['allow_html'] == true ) {
					if ( is_array( $this->options[$topt] ) ) {
						foreach ( $this->options[$topt] as $key => $val ) {
							$new_key = wp_specialchars_decode( $key, ENT_QUOTES );
							$new_val = wp_specialchars_decode( $val, ENT_QUOTES );
							$this->options[$topt][$new_key] = $new_val;
							if ( $key != $new_key )
								unset( $this->options[$topt][$key] );
						}
					} else {
						$this->options[$topt] = wp_specialchars_decode( $this->options[$topt], ENT_QUOTES );
					}
				}
			}
			return apply_filters( $this->get_hook( 'options' ), $this->options );
		}
		// Submit name
		protected function get_form_submit_name( $prefix ) {
			return $prefix . '_' . $this->u_id_base;
		}
		// Post URL
		protected function form_action_url() {
			return $_SERVER['PHP_SELF'] . '?page=' . $this->plugin_basename;
		}
		// Page submitted?
		protected function is_submitting_form( $prefix ) {
			return ( isset( $_POST['option_page'] ) && ( $_POST['option_page'] == $this->admin_options_name ) );
		}
		// Settings page?
		protected function is_plugin_admin_page() {
			global $pagenow;
			return ( basename( $pagenow, '.php' ) == $this->settings_page && isset( $_REQUEST['page'] ) && $_REQUEST['page'] == $this->plugin_basename );
		}
		
		// Main page output
		public function options_page() {
			$toptions = $this->get_options();
			if ( function_exists( 'settings_errors' ) )
				settings_errors();
			if ( $this->saved_options )
				echo "<div id='message' class='updated fade'><p><strong>" . $this->saved_options_msg . '</strong></p></div>';
			echo "<div class='wrap'>\n";
			echo '<div id="icon-edit" class="icon32 icon32-posts-post"><br /></div> <h2>Dynamic UK and US English <a href="#settings" class="button add-new-h2">Scroll Down to Settings</a> </h2>
			<div class="metabox-holder" > <div class="postbox" id="push-area"> <h3>Introduction</h3> <p><strong>What is Dynamic UK and US English?</strong><br />This plugin detects a user\'s language from their browser. If the language is en-gb (British), it will display posts in British English, if it detects any other language it will display posts in US English.</p> <p><strong>Why is it useful?</strong><br />British readers can feel <a href="http://busblog.net/920" target="_blank">alienated</a> when reading blogs written in US English and vice versa. This plugin will let them feel more at home.</p> <p><strong>Who is this useful for?</strong><br />Every single English WordPress based website.</p> <p><strong>How can I support the creator?</strong><br />The best thing you could do would be to subscribe to the <a href="http://busblog.net/feed" target="_blank">busblog.net RSS Feed</a>!</p> <p><strong>Where can I get support?</strong><br /><a href="http://busblog.net/934" target="_blank">Dynamic UK and US English Plugin Page</a></p> </div> </div>';
			$toptions = $this->get_options();
			$hide_rss = apply_filters( 'us_gb_d_c_hide_rss', $toptions['hide_rss'] );
			if ( !$hide_rss ) {
				echo '<div class="metabox-holder" > <div class="postbox" id="push-area"> <h3>Latest from busblog.net</h3>';
				$rss = new DOMDocument();
				$rss->load( 'http://feeds.feedburner.com/busblogdotnet' );
				$feed = array();
				foreach ( $rss->getElementsByTagName( 'item' ) as $node ) {
					$item = array ( 
						'title' => $node->getElementsByTagName( 'title' )->item(0)->nodeValue,
						'desc' => $node->getElementsByTagName( 'description' )->item(0)->nodeValue,
						'link' => $node->getElementsByTagName( 'link' )->item(0)->nodeValue,
						'date' => $node->getElementsByTagName( 'pubDate' )->item(0)->nodeValue,
						);
					array_push($feed, $item);
				}
				for( $x=0; $x<3; $x++ ) {
					$title = str_replace( ' & ', ' &amp; ', $feed[$x]['title'] );
					$link = $feed[$x]['link'];
					$description = $feed[$x]['desc'];
					$date = date( 'l F d, Y', strtotime( $feed[$x]['date'] ) );
					echo '<p><strong><a target="_blank" href="'.$link.'" title="'.$title.'">'.str_replace( "â€¦", "...", $title ).'</a></strong><br />';
					echo '<small><em>Posted on '.$date.'</em></small><br />';
					echo substr( str_replace( "â€™", "'", $description ), 0, 140 ).'...</p>';
				}
				echo '</div></div>';
			}
			echo '<div class="metabox-holder"><div class="postbox" id="push-area"> 
			<h3>Settings</h3>';
			do_action( $this->get_hook( 'before_settings_form' ), $this );
			echo "<form action=\"options.php\" method=\"post\" class=\"bb-form\" id=\"settings\">";
			settings_fields( $this->admin_options_name );
			do_settings_sections( $this->plugin_file );
			echo '<br class="breakp" /></div></div><h2 id="version">1.2</h2><div id="controls"><input type="submit" name="Submit" class="button-primary" value="' . esc_attr__( 'Save Changes' ) . '" /> <input type="submit" name="Reset" class="button" value="' . esc_attr__( 'Reset Settings' ) . '" /></div></form>';
		}
		// Display options
		public function display_option( $topt ) {
			do_action( $this->get_hook( 'pre_display_option' ), $topt );
			$toptions = $this->get_options();
			foreach ( array( 'datatype', 'input' ) as $attrib )
				$$attrib = isset( $this->config[$topt][$attrib] ) ? $this->config[$topt][$attrib] : '';
			if ( $input == '' || $input == 'none' )
				return;
			elseif ( $input == 'custom' ) {
				do_action( $this->get_hook( 'custom_display_option' ), $topt );
				return;
			}
			$value = isset( $toptions[$topt] ) ? $toptions[$topt] : '';
			$popt = $this->admin_options_name . "[$topt]";
			
			if ( $input == 'multiselect' ) {
				$popt .= '[]';
			} elseif ( $datatype == 'array' ) {
				if ( !is_array( $value ) )
					$value = '';
				else {
					if ( $input == 'textarea' || $input == 'inline_textarea' )
						$value = implode( "\n", $value );
					else
						$value = implode( ', ', $value );
				}
			} elseif ( $datatype == 'hash' && $input != 'select' ) {
				if ( !is_array( $value ) )
					$value = '';
				else {
					$new_value = '';
					foreach ( $value as $shortcut => $replacement )
						$new_value .= "$shortcut => $replacement\n";
					$value = $new_value;
				}
			}
			
			$attributes = $this->config[$topt]['input_attributes'];
			$this->config[$topt]['class'][] = 'bb-' . $input;
			if ( ( 'textarea' == $input || 'inline_textarea' == $input ) && $this->config[$topt]['no_wrap'] ) {
				$attributes .= ' wrap="off"';
			}
			elseif ( in_array( $input, array( 'text', 'long_text', 'short_text' ) ) ) {
				$this->config[$topt]['class'][]  = ( ( $input == 'short_text' ) ? 'small-text' : 'regular-text' );
				if ( $input == 'long_text' )
					$this->config[$topt]['class'][] = ' long-text';
			}
			$class = implode( ' ', $this->config[$topt]['class'] );
			$attribs = "name='$popt' id='$topt' class='$class' $attributes";
			
			if ( $input == 'textarea' || $input == 'inline_textarea' ) {
				if ( $input == 'textarea' )
					echo "</td><tr><td colspan='2'>";
				echo "<textarea $attribs>$value</textarea>\n";
			} elseif ( $input == 'select' ) {
				echo "<select $attribs>";
				if ( $this->config[$topt]['datatype'] == 'hash' ) {
					foreach ( (array) $this->config[$topt]['options'] as $sopt => $sval )
						echo "<option value='$sopt' " . selected( $value, $sopt, false ) . ">$sval</option>\n";
				} else {
					foreach ( (array) $this->config[$topt]['options'] as $sopt )
						echo "<option value='$sopt' " . selected( $value, $sopt, false ) . ">$sopt</option>\n";
				}
				echo "</select>";
			} elseif ( $input == 'multiselect' ) {
				echo '<fieldset class="bb-fieldset">' . "\n";
				foreach ( (array) $this->config[$topt]['options'] as $sopt )
					echo "<input type='checkbox' $attribs value='$sopt' " . checked( in_array( $sopt, $value ), true, false ) . ">$sopt</input><br />\n";
				echo '</fieldset>';
			} elseif ( $input == 'checkbox' ) {
				echo "<input type='$input' $attribs value='1' " . checked( $value, 1, false ) . " />\n";
			} else {
				echo "<input type='$input' $attribs value='" . esc_attr( $value ) . "' />\n";
			}
			
			if ( $title = apply_filters( $this->get_hook( 'option_title'), $this->config[$topt]['title'], $topt ) )
				echo "<br /><span class='bb-input-title'>$title</span>\n";
			
			if ( $help = apply_filters( $this->get_hook( 'option_help'), $this->config[$topt]['help'], $topt ) )
				echo "<br /><span class='bb-input-help'>$help</span>\n";
		
			do_action( $this->get_hook( 'post_display_option' ), $topt );
		}
	}
endif; // Class exists
?>