<?php /* Template Name: Homepage */
$header_img_url = ''.get_template_directory_uri().'/images/workers.jpg';
get_header();

$hero_image_fx1 = get_field('hero_image_fx1');
$hero_intro_tex = get_field('hero_intro_text_fx1');

if(!empty($hero_image_fx1) && !empty($hero_intro_tex)){ ?>
	<section class="hero">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="black_overlay"></div>
					<div class="img_background" style="background-image: url('<?php if(!empty($hero_image_fx1)){ echo $hero_image_fx1['sizes']['hero']; } ?>');" alt="<?php echo $hero_image_fx1['alt']; ?>">
						<div class="hero_content">
							<div class="container container-small">
								<div class="col-md-12 text_center">
									<?php if(!empty($hero_intro_tex)){ ?>
										<h1><?php echo $hero_intro_tex; ?></h1>
									<?php } ?>

									<?php 	$add_buttons_fx1 = get_field('add_buttons_fx1');
											if(!empty($add_buttons_fx1)) {
												$buttons_fx1 = get_field('buttons_fx1');
												if(!empty($buttons_fx1)){ ?>
													<div class="btn_group">
														<?php foreach($buttons_fx1 as $button){ 
																$button_label 	= $button['button_label'];
																$button_color 	= $button['button_color'];
																$button_type 	= $button['button_type'];

																switch ($button_type) {
																 	case 'video':
																 		$button_video 	= $button['button_video'];
																 		if(!empty($button_video)){ ?>
																	 		<a href="https://www.youtube.com/embed/<?php echo $button_video; ?>?" class="fancybox btn rounded_btn <?php echo $button_color; ?>_btn"><?php echo $button_label; ?></a>	
																	 	<?php } ?>

														<?php  			break;
																 	case 'link':
																 		$button_link 	= $button['button_link'];
																 		if(!empty($button_link)){   ?>
																 			<a href="<?php echo $button_link; ?>" title="" class="btn rounded_btn <?php echo $button_color; ?>_btn"><?php echo $button_label; ?></a>
																 		<?php } ?>

														<?php  			break;
																 	default: break;
																} ?>
														<?php } ?>
													</div>
											<?php } ?>
									<?php } ?>
								</div>
							</div>
						</div>
						<div class="scroll_down_arrow_parent">
							<div class="container">
								<div class="row">
									<div class="col-md-12 text_center">
										<a href="#" class="scroll_down_arrow">
											<img src="<?php echo get_template_directory_uri();?>/images/scroll_down_arrow.png" alt="Scroll Down">
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php } ?>


<?php if( have_rows('flexible_content') ):
    while ( have_rows('flexible_content') ) : the_row();
        if( get_row_layout() == 'logos' ):
        	$logos = get_sub_field('logos');
        	if(!empty($logos)){   ?>
		        <section class="partners_logos">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<?php foreach( $logos as $logo){ 
										$logo_img 		= $logo['logo'];
										$external_link 	= $logo['external_link'];
										if(!empty($logo_img)){ ?>
											<div class="col-xs-2 text_center text_left_sm">
												<?php if(!empty($external_link)){ ?>
													<a href="<?php echo $external_link; ?>" title="" target="_blank">
												<?php } ?>
													<img src="<?php echo $logo_img['sizes']['logo_partners']; ?>" alt="<?php echo $logo_img['alt']; ?>">
												<?php if(!empty($external_link)){ ?>
													</a>
												<?php } ?>
											</div>	
										<?php } ?>
								<?php } ?>
							</div>
						</div>
					</div>
				</section>	
			<?php } ?>
<?php 	elseif( get_row_layout() == 'simple_text' ): 
			$content_fx_2 = get_sub_field('content_fx_2');
			if(!empty($content_fx_2)){ ?>
				<section class="why_us_title">
					<div class="container container-small">
						<div class="row">
							<div class="col-md-12 general_content">
								<?php echo $content_fx_2; ?>
							</div>
						</div>
					</div>
				</section>
			<?php } ?>

<?php 	elseif( get_row_layout() == 'section_more_white_less_black' ):

			$title_fx3 		= get_sub_field('title_fx3');
			$content_fx3 	= get_sub_field('content_fx3');
			$image 			= get_sub_field('image_fx3');

			if(!empty($content_fx3) && !empty($title_fx3)){ ?>
				<section class="list_black_form_bg background_white_black-desktop background_white_black-tablet first_black_triangle_parent position_relative">
					<div class="first_black_triangle_bg" style="display: none;">
						<div class="first_white_triangle_bg" style="display: none;"></div>
					</div>
					<div class="container container-small text_center">
						<div class="row">
							<div class="col-md-6 vcenter">
								<h3 class="style_h2"><?php echo $title_fx3; ?></h3>
								<p><?php echo $content_fx3; ?></p>
							</div>
							<?php if(!empty($image)){ ?>
								<div class="col-md-6 vcenter">
									<img src="<?php echo $image['sizes']['image_list']; ?>" alt="<?php echo $image['alt'];?>">
								</div>
							<?php } ?>
							<div class="clear"></div>
						</div>
					</div>
				</section>
			<?php } ?>

<?php 	elseif( get_row_layout() == 'section_black_image_left' ):

				$title_fx4 		= get_sub_field('title_fx4');
				$content_fx4 	= get_sub_field('content_fx4');
				$image_fx4 		= get_sub_field('image_fx4');
				$add_button 	= get_sub_field('add_button_fx4');
				$button_label 	= get_sub_field('button_label_fx4');
				$button_link 	= get_sub_field('button_link_fx4');

				if(!empty($title_fx4) && !empty($content_fx4)){ ?>
				<section class="list_black_form_bg background_black-desktop background_black-tablet">
					<div class="container container-small text_center">
						<div class="row">
							<div class="col-md-6 col-md-push-6 vcenter">
								<?php if(!empty($title_fx4)){ ?>
									<h3 class="style_h2"></h3>				
								<?php } ?>

								<?php if(!empty($content_fx4)){ ?>
									<p><?php echo $content_fx4; ?></p>
								<?php } ?>

								<?php if(!empty($add_button)){ 
									if(!empty($button_label) && !empty($button_link))?>
										<a href="<?php echo $button_link; ?>" class="btn rounded_btn green_btn hidden-xs hidden-sm"><?php echo $button_label; ?></a>
								<?php } ?>
							</div>
							<?php if(!empty($image_fx4)){ ?>
								<div class="col-md-6 col-md-pull-6 vcenter">
									<img src="<?php echo $image_fx4['sizes']['image_list']; ?>" alt="<?php echo $image_fx4['alt']; ?>">
								</div>
							<?php } ?>
							<div class="clear"></div>
						</div>
					</div>
				</section>
				<?php } ?>
				
<?php 	elseif( get_row_layout() == 'section_black_image_right' ):
				$title_fx5 		= get_sub_field('title_fx5');
				$content_fx5 	= get_sub_field('content_fx5');
				$image_fx5 		= get_sub_field('image_fx5'); 

				if(!empty($title_fx5) && !empty($content_fx5)){ ?>
					<section class="list_black_form_bg background_black-desktop background_black_white-tablet">
						<div class="hidden-md hidden-lg">
							<div class="position_relative">
								<div class="desktop_only_black_triangle_bg" style="display: none;">
									<div class="custom_triangle_padding"></div>
									<div class="desktop_only_white_triangle_bg" style="display: none;"></div>
								</div>
							</div>
						</div>
						<div class="container container-small text_center">
							<div class="row">
								<div class="col-md-6 vcenter">
									<?php if(!empty($title_fx5)){ ?>
										<h3 class="style_h2"><?php echo $title_fx5; ?></h3>
									<?php } ?>

									<?php if(!empty($content_fx5)){ ?>
										<p><?php echo $content_fx5; ?></p>
									<?php } ?>
								</div>
								<?php if(!empty($image_fx5)){  ?>
									<div class="col-md-6 vcenter">
										<img src="<?php echo $image_fx5['sizes']['image_list']; ?>" alt="<?php echo $image_fx5['alt']; ?>">
									</div>
								<?php } ?>
								<div class="clear"></div>
							</div>
						</div>
					</section>
				<?php } ?>

<?php 	elseif( get_row_layout() == 'section_more_black_less_white' ): 
				$title_fx5 		= get_sub_field('title_fx7');
				$content_fx5 	= get_sub_field('content_fx7');
				$image_fx5 		= get_sub_field('image_fx7'); 

				if(!empty($title_fx5) && !empty($content_fx5)){ ?>
					<section class="list_black_form_bg background_black_white-desktop background_white-tablet position_relative">
						<div class="desktop_only_black_triangle_bg" style="display: none;">
							<div class="custom_triangle_padding"></div>
							<div class="desktop_only_white_triangle_bg" style="display: none;"></div>
						</div>
						<div class="container container-small text_center">
							<div class="row">
								<div class="col-md-6 col-md-push-6 vcenter">
									<?php if(!empty($title_fx5)){ ?>
										<h3 class="style_h2"><?php echo $title_fx5; ?></h3>
									<?php } ?>	
									<?php if(!empty($content_fx5)){ ?>
										<p><?php echo $content_fx5; ?></p>
									<?php } ?>
								</div>
								<?php if(!empty($image_fx5)){  ?>
									<div class="col-md-6 col-md-pull-6 vcenter">
										<img src="<?php echo $image_fx5['sizes']['image_list']; ?>" alt="<?php echo $image_fx5['alt']; ?>">
									</div>
								<?php } ?>
								<div class="clear"></div>
							</div>
						</div>
					</section>
				<?php } ?>

<?php 	elseif( get_row_layout() == 'button' ):
				$button_label_fx6 	= get_sub_field('button_label_fx6');
				$title_popup_fx6	= get_sub_field('title_popup_fx6');
				$code 				= get_sub_field('shortcode_for_modal_fx6');  /* NOT done! */ ?>

				<section class="after_big_section_btn">
					<div class="container">
						<div class="row">
							<div class="col-md-12 text_center demo_req_popup">
								<?php if(!empty($button_label_fx6)){ ?>
									<a href="#resources_form" data-iziModal-open="#modal_reques_form" class="btn rounded_btn blue_btn"><?php echo $button_label_fx6; ?></a>
								<?php } ?>
								
								<?php // <!-- form request popup --> ?>
								<div id="modal_reques_form" class="resources_form" data-izimodal-title="" style="display: none;">
									<div class="modal-req-popup">
										<?php if(!empty($title_popup_fx6)){ ?>
											<h2><?php echo $title_popup_fx6; ?></h2>
										<?php } ?>
										
										<?php if(!empty($code)){ ?>
										<div class="register-form-wrapper">
											<?php echo $code; ?>
											<?php /*	
											<form action="">
												<h6>First Name*</h6>
												<input type="text">
												<h6>Last Name*</h6>
												<input type="text">
												<h6>Business Email*</h6>
												<input type="email">
												<h6>Phone*</h6>
												<input type="text">
												<h6>Location*</h6>
												<span class="select-dropdown">
													<select name="">
														<option value="">Option #1</option>
														<option value="">Option #2</option>
														<option value="">Option #3</option>
														<option value="">Option #4</option>
													</select>
												</span>
												<div class="text_center">
													<input type="submit" value="See my tour">
												</div>
											</form>
											<?php */ ?>
										</div>
										<?php } ?>
									</div>
								</div>
								<?php //<!-- //form request popup -->?>
							
							</div>
						</div>
					</div>
				</section>

	<?php endif;
    endwhile;
endif; ?>

<?php // <!-- End of big section "why us" --> ?>
<?php get_footer();
