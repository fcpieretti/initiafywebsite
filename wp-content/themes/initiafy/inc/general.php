<?php defined( 'ABSPATH' ) or die( 'Direct access is forbidden!' );

/**
 * Theme Setup
 */
function theme_setup() {
	// This theme uses a custom image size for featured images, displayed on "standard" posts.
	add_theme_support('post-thumbnails'); // size name: post-thumbnail
	set_post_thumbnail_size(1400, 475, true);
}
add_action( 'after_setup_theme', 'theme_setup' );

/**
 * Enqueue scripts and styles for front-end.
 */
function theme_styles() {
	# jQuery
	wp_enqueue_script( 'jquery' );

	wp_enqueue_style( 'wpb-google-fonts', '//fonts.googleapis.com/css?family=Montserrat:300,400,400i,500,600', false );
    wp_enqueue_style( 'wpb-google-fonts-archivo', '//fonts.googleapis.com/css?family=Open+Sans:400,400i', false );
    wp_enqueue_style( 'bootstrap', get_template_directory_uri().'/css/bootstrap.min.css' );
    wp_enqueue_style( 'fancybox', get_template_directory_uri() .'/css/jquery.fancybox.min.css');
    wp_enqueue_style( 'owl-carousel', get_template_directory_uri().'/css/owl.carousel.min.css' );
    wp_enqueue_style( 'owl-carousel-theme-default', get_template_directory_uri().'/css/owl.theme.default.min.css' );
    wp_enqueue_style( 'font-awesome', get_template_directory_uri().'/css/font-awesome.min.css' );
    wp_enqueue_style( 'ion.rangeSlider', get_template_directory_uri().'/css/ion.rangeSlider.css' );    
    wp_enqueue_style( 'ion.rangeSlider.skinHTML5', get_template_directory_uri().'/css/ion.rangeSlider.skinHTML5.css' );
    wp_enqueue_style( 'sumoselect.min', get_template_directory_uri().'/css/sumoselect.min.css' );
    wp_enqueue_style( 'slick', get_template_directory_uri().'/css/slick.css' );
    wp_enqueue_style( 'jquery.bxslider', get_template_directory_uri().'/css/jquery.bxslider.css' );
    wp_enqueue_style( 'iziModal.min', get_template_directory_uri().'/css/iziModal.min.css' );


   // wp_enqueue_style('slick_style', get_template_directory_uri().'/slick/slick.css');
	//wp_enqueue_style('slick_theme_style', get_template_directory_uri().'/slick/slick-theme.css');

   // wp_enqueue_style('main', get_template_directory_uri() . '/style.css' );


    # google API Key 
	$google_api_key = get_field('google_api_key', 'options'); 
	if(!empty($google_api_key)){
		wp_register_script('googlemaps','//maps.googleapis.com/maps/api/js?key='.$google_api_key, '', '', true );
		wp_enqueue_script('googlemaps', array('jquery'));
	}

    # bootstrap
	wp_register_script('bootstrap-js', get_template_directory_uri().'/js/bootstrap.min.js', '', '', true );
	wp_enqueue_script('bootstrap-js', array('jquery'));

	# fancybox
	wp_register_script('fancybox', get_template_directory_uri().'/js/jquery.fancybox.min.js', '', '', true );
	wp_enqueue_script('fancybox', array('jquery'));

   # jquery.bxslider
	wp_register_script('jquery.bxslider', get_template_directory_uri().'/js/jquery.bxslider.js', '', '', true );
	wp_enqueue_script('jquery.bxslider', array('jquery'));

	# carousel
	wp_register_script('owl-carousel-js', get_template_directory_uri().'/js/owl.carousel.min.js', '', '', true );
	wp_enqueue_script('owl-carousel-js', array('jquery'));

	# rangeSlider
	wp_register_script('ion.rangeSlider.min', get_template_directory_uri().'/js/ion.rangeSlider.min.js', '', '', true );
	wp_enqueue_script('ion.rangeSlider.min', array('jquery'));

	# iziModal
	wp_register_script('iziModal.min', get_template_directory_uri().'/js/iziModal.min.js', array('jquery'), '', true);
	wp_enqueue_script('iziModal.min', array('jquery'));


	wp_register_script('cookie', get_template_directory_uri().'/js/jquery.cookie.js', array('jquery'), '', true);
	wp_enqueue_script('cookie', array('jquery'));


	#jquery.sumoselect.min	
   	wp_register_script('jquery.sumoselect.min', get_template_directory_uri().'/js/jquery.sumoselect.min.js', '', '', true );
	wp_enqueue_script('jquery.sumoselect.min', array('jquery'));

	# slick
 	wp_register_script('slick.min', get_template_directory_uri().'/js/slick.min.js', '', '', true );
	wp_enqueue_script('slick.min', array('jquery'));
   
    //wp_enqueue_script('script', get_template_directory_uri() . '/script.js', array('jquery'),null);

 //    #money.js
 //    wp_register_script('money', get_template_directory_uri().'/js/money.js', '', '', true );
	// wp_enqueue_script('money', array('jquery')); 

	// #accounting.min.js 
	// wp_register_script('accounting', get_template_directory_uri().'/js/accounting.min.js ', '', '', true );
	// wp_enqueue_script('accounting', array('jquery')); 

	# main javascript file
	wp_register_script('scripts', get_template_directory_uri().'/script.js', array('jquery'), '', true);
	wp_enqueue_script('scripts', array('jquery'));

	# Loads our main stylesheet.
	wp_enqueue_style( 'site-style', get_stylesheet_uri(), false, '' );

}
add_action( 'wp_enqueue_scripts', 'theme_styles' );


