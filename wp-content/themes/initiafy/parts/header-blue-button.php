<?php 
	$add_blue_button_header 			= get_field('add_blue_button_header', 'options');
	$blue_button_label_header 			= get_field('blue_button_label_header', 'options');
	$blue_button_link_header 			= get_field('blue_button_link_header', 'options');
	$type_blue_button_header 			= get_field('type_blue_button_header', 'options');
	$blue_button_page_header			= get_field('blue_button_page_header', 'options');	
	//$external 							= get_field('blue_button_external_link_header', 'options');

	if(!empty($add_blue_button_header)){

		switch ($type_blue_button_header) {
			case 'link':

				if(!empty($blue_button_page_header) && !empty($blue_button_label_header)){ ?>

					<a href="<?php echo $blue_button_page_header; ?>" class="btn rounded_btn blue_btn"><?php echo $blue_button_label_header; ?></a>
				<?php } ?>
				
		<?php	break;
			case 'video': ?>
				<?php  if(!empty($blue_button_label_header) && !empty($blue_button_link_header)){ ?>
			
					<a href="#generic_video_popup_blue" class="btn rounded_btn blue_btn" data-iziModal-open="#generic_video_popup_blue"><?php echo $blue_button_label_header; ?></a>
																					
					<div id="generic_video_popup_blue" class="video_popup general_popup_item" data-izimodal-title="" data-izimodal-iframeURL="<?php echo $blue_button_link_header; ?>" style="display: none;"></div>

				<?php } ?>
			<?php	break;
			default: break;
		} ?>


		
<?php } ?>


