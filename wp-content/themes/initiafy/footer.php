
<?php 



$page_link_name = str_replace(home_url(), '', get_permalink());
$page_link_name = str_replace('/', '', $page_link_name);

?>

<?php 	$img 				= get_field('background_image_request', 'options');
		$title_request 		= get_field('title_request', 'options');
		$content_request 	= get_field('content_request', 'options');
		$add_button_desktop = get_field('add_button_desktop_request', 'options');
		$button_label_desktop 	= get_field('button_label_desktop_request', 'options');
		$button_link_desktop 	= get_field('button_link_desktop_request', 'options');
		//$add_button_mobile_request = get_field('add_button_mobile_request', 'options');
		//$button_label_mobile_request = get_field('button_label_mobile_request', 'options'); 
		//$button_link_mobile_request = get_field('button_link_mobile_request', 'options'); ?>

		<section class="red_cta" style="background-image: url('<?php if(!empty($img)){ echo $img['sizes']['footer']; } ?>')">
			<div class="container container-small">
				<div class="row">
					<div class="col-md-5 text_center_xs text_center_sm text_left_md text_left_lg vcenter">
						<div class="red_cta_text text_center">
							<?php if(!empty($title_request)){ ?>
								<h4 class="style_h2"><?php echo $title_request; ?></h4>
							<?php } ?>

							<?php if(!empty($content_request)){ ?>
								<p><?php echo $content_request; ?></p>
							<?php } ?>
							
							
						</div>
					</div>
					<?php if(!empty($add_button_desktop)){ 
							if(!empty($button_label_desktop) && !empty($button_link_desktop)){ ?>
								<div class="col-md-7 text_center_xs text_center_sm text_right_md text_right_lg vcenter">
									<a href="<?php echo $button_link_desktop; ?>" class="btn rounded_btn green_btn btn-request-demo-footer-<?php echo $page_link_name; ?>"><?php echo $button_label_desktop; ?></a>
								</div>
							<?php } ?>
					<?php } ?>
					<div class="clear"></div>
				</div>
			</div>
		</section>

		<footer>
			<?php if( have_rows('footer_larger_screens', 'options') ): ?>
				<div class="container">
					<div class="row">
					    <?php while ( have_rows('footer_larger_screens', 'options') ) : the_row();
					        if( get_row_layout() == 'social_list_copyright_and_links' ):
					        	$title 		 	= get_sub_field('title_fx_1');
					        	$social_list 	= get_sub_field('social_list_fx_1'); 
					        	$links 			= get_sub_field('links_fx_1');
					        	$copyright_message = get_sub_field('copyright_message_fx_1'); ?>

					        	<div class="col-md-4 single-footer">
					        		<?php if(!empty($title)){ ?>
										<h5><?php echo $title; ?></h5>
									<?php } ?>

									<?php if(!empty($social_list)){ ?>
										<ul class="footer_social">
											<?php foreach($social_list as $item){
												$link = $item['link'];
												$icon = $item['icon'];
												if(!empty($link) && !empty($icon)){  ?>
													<li>
														<a href="<?php echo $link; ?>" title="" target="_blank"><?php echo $icon; ?></a>
													</li>
												<?php } ?>
											<?php } ?>
										</ul>
									<?php } ?>
									<div class="clear"></div>
									<div class="copyright_text">
										<?php if(!empty($links)){ ?>
										<ul class="copyright_menu">
											<?php foreach($links as $link){
												$page_item = $link['page_item']; 
												if(!empty($page_item)){ ?>
													<li>
														<a href="<?php echo get_permalink($page_item); ?>" title="<?php echo get_the_title($page_item);?>"><?php echo get_the_title($page_item);?></a>
													</li>
												<?php }  ?>
											<?php } ?>
										</ul>
										<?php } ?>
										<?php if(!empty($copyright_message)){ ?>
											<p><?php echo $copyright_message; ?></p>
										<?php } ?>
									</div>
								</div>

					    	<?php elseif( get_row_layout() == 'address' ):
					    			$title 			= get_sub_field('title_fx_2');
					    			$list_addresses = get_sub_field('list_addresses_fx_2'); ?>

					    		<div class="col-md-4 single-footer">
					    			<?php if(!empty($title)){ ?>
										<h5><?php echo $title; ?></h5>
									<?php } ?>

									<?php if(!empty($list_addresses)){ ?>
									<ul class="contact_phones">
										<?php foreach($list_addresses as $add){
												$location = $add['location']; 
												$phone_number = $add['phone_number'];
												if(!empty($location) && !empty($phone_number)){ ?>
													<li>
														<?php echo $location; ?> <a href="tel:<?php echo $phone_number; ?>" title=""><?php echo $phone_number; ?></a>
													</li>
												<?php } ?>
										<?php } ?>
									</ul>
									<?php } ?>
								</div>	

					    	<?php elseif( get_row_layout() == 'newsletter' ):
					    			$title_fx_3 = get_sub_field('title_fx_3');
					    			$cf 		= get_sub_field('shortcode_for_newsletter_fx_3');

					    			if(!empty($cf)){ ?>	
							    		<div class="col-md-4 text_right single-footer">
											<div class="text_left">
												<?php if(!empty($title_fx_3)){ ?>
													<h5><?php echo $title_fx_3; ?></h5>
												<?php } ?>
												<?php /*<input type="email" name="email" placeholder="Your Email">
												<input type="submit" value="SUBSCRIBE" class="btn rounded_btn red_btn">*/?>
												<div class="wrapp-news-form">
													<?php echo $cf; ?>
												</div>
											</div>
										</div>

									<?php } ?>
					       <?php endif;
					    endwhile; ?>
				    </div>
				</div>
			<?php endif; ?>
		</footer>
		<?php // get_template_part( 'parts/popup-newsletter'); ?> 
		<?php wp_footer(); ?>
		<script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 965556075;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/965556075/?guid=ON&amp;script=0"/>
		</div>
		</noscript>
		
		<script type="text/javascript">
			_linkedin_data_partner_id = "68979";
			</script><script type="text/javascript">
			(function(){var s = document.getElementsByTagName("script")[0];
			var b = document.createElement("script");
			b.type = "text/javascript";b.async = true;
			b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
			s.parentNode.insertBefore(b, s);})();
			</script>
			<noscript>
			<img height="1" width="1" style="display:none;" alt="" src="https://dc.ads.linkedin.com/collect/?pid=68979&fmt=gif
			https://dc.ads.linkedin.com/collect/?pid=68979&fmt=gif
			" />
	    </noscript>
		
	</body>
</html>
