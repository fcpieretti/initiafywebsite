<?php defined( 'ABSPATH' ) or die( 'Direct access is forbidden!' );

add_image_size('hero', 1920, 830, true);
add_image_size('image_list', 483, 483, true);
add_image_size('footer', 1920, 215, true);
add_image_size('hero-single', 1920, 830, true);
add_image_size('hero-single-small', 680, 480, true);
add_image_size('single-small', 495, 330, true);
add_image_size('banner_post', 1000, 400, true);
add_image_size('not_found', 485, 600, true);
add_image_size('video_intro', 470, 263, true);
add_image_size('logo_customers', 247, 131, true);
add_image_size('banner_blog', 304, 175, true);
add_image_size('image_testimonial', 315, 325, true);
add_image_size('featured_landing_simple', 468, 310, true);
add_image_size('logo_size', 300, 133, true);
add_image_size('featured_large_image', 1920, 445, true);
add_image_size('video_play', 470, 263, true);
add_image_size('icon_step', 48, 48, true);
add_image_size('icon_step2', 92, 92, true);
add_image_size('product_image', 630, 480, true);
add_image_size('product_image_2', 620, 640, true);
add_image_size('logo_partners', 134, 55, true);
add_image_size('request_image', 541, 282, true);
add_image_size('resource_single', 470, 310, true); 
add_image_size('company_single', 128, 128, true); 
add_image_size('icon_newsletter', 137, 137, true);

add_image_size('team_icon', 300, 300, true);

add_image_size('small_testimonial', 200, 200, true);
add_image_size('medium_testimonial', 300, 300, true);
add_image_size('large_testimonial',  483, 483, true);

add_image_size('small_testimonialsecond', 200, 200, true); 
add_image_size('medium_testimonialsecond', 250, 250, true);
add_image_size('large_testimonialsecond',  315, 315, true);




# Register option pages
if( function_exists('acf_add_options_sub_page') ){
	acf_add_options_page(array(
		'page_title' => 'Theme Options',
		'menu_title' => 'Theme Options',
		'menu_slug'  => 'theme-options',
		'capability' => 'manage_options',
	));
	acf_add_options_sub_page(array(
		'parent'     => 'theme-options',
		'title'      => 'General',
		'slug'       => 'theme-general',
		'capability' => 'manage_options'
	));
	acf_add_options_sub_page(array(
		'parent'     => 'theme-options',
		'title'      => 'Popups',
		'slug'       => 'theme-popups',
		'capability' => 'manage_options'
	));
}

/**
 * Disable the emoji's
 */
function disable_emojis() {
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );	
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );	
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
}
add_action( 'init', 'disable_emojis' );



function testimonial() {
	$labels = array(
		'name'                  => _x( 'Testimonial', 'Post Type General Name', THEME_TEXT_DOMAIN ),
		'singular_name'         => _x( 'Testimonial', 'Post Type Singular Name', THEME_TEXT_DOMAIN ),
		'menu_name'             => __( 'Testimonials', THEME_TEXT_DOMAIN ),
		'name_admin_bar'        => __( 'Testimonial', THEME_TEXT_DOMAIN),
		'archives'              => __( 'Testimonials Archives', THEME_TEXT_DOMAIN ),
		'parent_item_colon'     => __( 'Parent Testimonial:', THEME_TEXT_DOMAIN ),
		'all_items'             => __( 'All Testimonials', THEME_TEXT_DOMAIN ),
		'add_new_item'          => __( 'Add New Testimonial', THEME_TEXT_DOMAIN ),
		'add_new'               => __( 'Add New', THEME_TEXT_DOMAIN ),
		'new_item'              => __( 'New Item', THEME_TEXT_DOMAIN ),
		'edit_item'             => __( 'Edit Testimonial', THEME_TEXT_DOMAIN ),
		'update_item'           => __( 'Update Testimonial', THEME_TEXT_DOMAIN ),
		'view_item'             => __( 'View Testimonial', THEME_TEXT_DOMAIN ),
		'search_items'          => __( 'Search Testimonial', THEME_TEXT_DOMAIN ),
		'not_found'             => __( 'Not found', THEME_TEXT_DOMAIN ),
		'not_found_in_trash'    => __( 'Not found in Trash', THEME_TEXT_DOMAIN ),
		'insert_into_item'      => __( 'Insert into Testimonial', THEME_TEXT_DOMAIN ),
		'uploaded_to_this_item' => __( 'Uploaded to this Testimonial', THEME_TEXT_DOMAIN ),
		'items_list'            => __( 'Items Testimonial', THEME_TEXT_DOMAIN ),
		'items_list_navigation' => __( 'Testimonials list navigation', THEME_TEXT_DOMAIN ),
		'filter_items_list'     => __( 'Filter items Testimonial', THEME_TEXT_DOMAIN ),
	);
	
	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => false,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 	'slug' 			=> 'testimonial',
										'with_front' 	=> false ),
		'capability_type'    => 'post',
		'has_archive'        => false,
		'hierarchical'       => false,
		'menu_position'      => 26,
		'supports'           => array( 'title' ,  'thumbnail',  'editor'   ),
	);

	register_post_type( 'testimonial', $args );


	$labels = array(
		'name'                  => _x( 'Resource', 'Post Type General Name', THEME_TEXT_DOMAIN ),
		'singular_name'         => _x( 'Resource', 'Post Type Singular Name', THEME_TEXT_DOMAIN ),
		'menu_name'             => __( 'Resources', THEME_TEXT_DOMAIN ),
		'name_admin_bar'        => __( 'Resource', THEME_TEXT_DOMAIN),
		'archives'              => __( 'Resources Archives', THEME_TEXT_DOMAIN ),
		'parent_item_colon'     => __( 'Parent Resource:', THEME_TEXT_DOMAIN ),
		'all_items'             => __( 'All Resources', THEME_TEXT_DOMAIN ),
		'add_new_item'          => __( 'Add New Resource', THEME_TEXT_DOMAIN ),
		'add_new'               => __( 'Add New', THEME_TEXT_DOMAIN ),
		'new_item'              => __( 'New Item', THEME_TEXT_DOMAIN ),
		'edit_item'             => __( 'Edit Resource', THEME_TEXT_DOMAIN ),
		'update_item'           => __( 'Update Resource', THEME_TEXT_DOMAIN ),
		'view_item'             => __( 'View Resource', THEME_TEXT_DOMAIN ),
		'search_items'          => __( 'Search Resource', THEME_TEXT_DOMAIN ),
		'not_found'             => __( 'Not found', THEME_TEXT_DOMAIN ),
		'not_found_in_trash'    => __( 'Not found in Trash', THEME_TEXT_DOMAIN ),
		'insert_into_item'      => __( 'Insert into Resource', THEME_TEXT_DOMAIN ),
		'uploaded_to_this_item' => __( 'Uploaded to this Resource', THEME_TEXT_DOMAIN ),
		'items_list'            => __( 'Items Resource', THEME_TEXT_DOMAIN ),
		'items_list_navigation' => __( 'Resources list navigation', THEME_TEXT_DOMAIN ),
		'filter_items_list'     => __( 'Filter items Resource', THEME_TEXT_DOMAIN ),
	);
	
	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 	'slug' 			=> 'resource',
										'with_front' 	=> false  ),
		'capability_type'    => 'page',
		'has_archive'        => false,
		'hierarchical'       => false,
		'menu_position'      => 26,
		'supports'           => array( 'title' ,  'thumbnail',  'editor' , 'excerpt'  ),
	);

	register_post_type( 'resource', $args );


	$labels = array(
		'name'              => _x( 'Category', 'taxonomy general name' ),
		'singular_name'     => _x( 'Category', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Category' ),
		'all_items'         => __( 'All Categories' ),
		'parent_item'       => __( 'Parent Category' ),
		'parent_item_colon' => __( 'Parent Category:' ),
		'edit_item'         => __( 'Edit Category' ),
		'update_item'       => __( 'Update Category' ),
		'add_new_item'      => __( 'Add New Category' ),
		'new_item_name'     => __( 'New Category Name' ),
		'menu_name'         => __( 'Categories' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'has_archive' 		=> true,
		'rewrite'           => array( 'slug' => 'resource-category' ),
	);

	register_taxonomy( 'resource-category', array( 'resource' ), $args ); // , 'member'


	$labels = array(
		'name'                  => _x( 'Package', 'Post Type General Name', THEME_TEXT_DOMAIN ),
		'singular_name'         => _x( 'Package', 'Post Type Singular Name', THEME_TEXT_DOMAIN ),
		'menu_name'             => __( 'Packages', THEME_TEXT_DOMAIN ),
		'name_admin_bar'        => __( 'Package', THEME_TEXT_DOMAIN),
		'archives'              => __( 'Packages Archives', THEME_TEXT_DOMAIN ),
		'parent_item_colon'     => __( 'Parent Package:', THEME_TEXT_DOMAIN ),
		'all_items'             => __( 'All Packages', THEME_TEXT_DOMAIN ),
		'add_new_item'          => __( 'Add New Package', THEME_TEXT_DOMAIN ),
		'add_new'               => __( 'Add New', THEME_TEXT_DOMAIN ),
		'new_item'              => __( 'New Item', THEME_TEXT_DOMAIN ),
		'edit_item'             => __( 'Edit Package', THEME_TEXT_DOMAIN ),
		'update_item'           => __( 'Update Package', THEME_TEXT_DOMAIN ),
		'view_item'             => __( 'View Package', THEME_TEXT_DOMAIN ),
		'search_items'          => __( 'Search Package', THEME_TEXT_DOMAIN ),
		'not_found'             => __( 'Not found', THEME_TEXT_DOMAIN ),
		'not_found_in_trash'    => __( 'Not found in Trash', THEME_TEXT_DOMAIN ),
		'insert_into_item'      => __( 'Insert into Package', THEME_TEXT_DOMAIN ),
		'uploaded_to_this_item' => __( 'Uploaded to this Package', THEME_TEXT_DOMAIN ),
		'items_list'            => __( 'Items Package', THEME_TEXT_DOMAIN ),
		'items_list_navigation' => __( 'Packages list navigation', THEME_TEXT_DOMAIN ),
		'filter_items_list'     => __( 'Filter items Package', THEME_TEXT_DOMAIN ),
	);
	
	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => false,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 	'slug' 			=> 'package',		
										'with_front' 	=> false  ),
		'capability_type'    => 'post',
		'has_archive'        => false,
		'hierarchical'       => false,
		'menu_position'      => 26,
		'supports'           => array( 'title'),
	);

	register_post_type( 'package', $args );


	$labels = array(
		'name'                  => _x( 'Jobs', 'Post Type General Name', THEME_TEXT_DOMAIN ),
		'singular_name'         => _x( 'Job', 'Post Type Singular Name', THEME_TEXT_DOMAIN ),
		'menu_name'             => __( 'Jobs', THEME_TEXT_DOMAIN ),
		'name_admin_bar'        => __( 'Job', THEME_TEXT_DOMAIN),
		'archives'              => __( 'Jobs Archives', THEME_TEXT_DOMAIN ),
		'parent_item_colon'     => __( 'Parent Job:', THEME_TEXT_DOMAIN ),
		'all_items'             => __( 'All Jobs', THEME_TEXT_DOMAIN ),
		'add_new_item'          => __( 'Add New Job', THEME_TEXT_DOMAIN ),
		'add_new'               => __( 'Add New', THEME_TEXT_DOMAIN ),
		'new_item'              => __( 'New Item', THEME_TEXT_DOMAIN ),
		'edit_item'             => __( 'Edit Job', THEME_TEXT_DOMAIN ),
		'update_item'           => __( 'Update Job', THEME_TEXT_DOMAIN ),
		'view_item'             => __( 'View Job', THEME_TEXT_DOMAIN ),
		'search_items'          => __( 'Search Job', THEME_TEXT_DOMAIN ),
		'not_found'             => __( 'Not found', THEME_TEXT_DOMAIN ),
		'not_found_in_trash'    => __( 'Not found in Trash', THEME_TEXT_DOMAIN ),
		'insert_into_item'      => __( 'Insert into Job', THEME_TEXT_DOMAIN ),
		'uploaded_to_this_item' => __( 'Uploaded to this Job', THEME_TEXT_DOMAIN ),
		'items_list'            => __( 'Items Job', THEME_TEXT_DOMAIN ),
		'items_list_navigation' => __( 'Jobs list navigation', THEME_TEXT_DOMAIN ),
		'filter_items_list'     => __( 'Filter items Job', THEME_TEXT_DOMAIN ),
	);
	
	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => false,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' 			=> 'job',
										'with_front' 	=> false ),
		'capability_type'    => 'post',
		'has_archive'        => false,
		'hierarchical'       => false,
		'menu_position'      => 26,
		'supports'           => array( 'title'),
	);

	register_post_type( 'job', $args );
	
} 
add_action( 'init', 'testimonial', 0 ); 


// function locate & get currency 
function get_current_currrency($page_id){
	$location 			= file_get_contents('http://freegeoip.net/json/'.$_SERVER['REMOTE_ADDR']);
	$location 			= json_decode($location);
	$country_name 		= $location->country_name; 

	$service_url 	= 'https://restcountries.eu/rest/v2/name/'.$country_name.'?fullText=true';
   	$curl 			= curl_init($service_url);
   	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
   	$curl_response = curl_exec($curl);
   	$character = json_decode($curl_response);

   	if(!empty($character[0]->currencies[0]->code)) {
   		// if currency is not in our list of prices -> return USD
		$array_currency = array('CAD', 'GBP', 'EUR', 'USD');	
		if(in_array($character[0]->currencies[0]->code, $array_currency)) {
			$main_currency['code'] 		= $character[0]->currencies[0]->code;
   			$main_currency['symbol'] 	= $character[0]->currencies[0]->symbol;
		} else {
			$main_currency['code'] 		= 'USD';
   			$main_currency['symbol'] 	= '$';
		}	

   	} else {
   		$main_currency['code'] 		= 'USD';
   		$main_currency['symbol'] 	= '$';
   	}
 
   	curl_close($curl);
   	return $main_currency; 
}


function change_currency(){
	$error = true;

	if(isset($_POST['action']) && strcmp($_POST['action'], "change_currency") == 0) {
		if(isset($_POST['new_val']) && strlen($_POST['new_val']) > 0 && 
			isset($_POST['id']) && strlen($_POST['id']) > 0 && 
			isset($_POST['new_symbol']) && strlen($_POST['new_symbol']) > 0 && 
			isset($_POST['users_val']) && strlen($_POST['users_val']) > 0 ) {

			$currency_code 	= $_POST['new_val'];
			$new_symbol		= $_POST['new_symbol'];
			$page_id 		= $_POST['id'];
			$no_users 		= $_POST['users_val'];
			$output 		= get_new_prices($currency_code, $page_id, $no_users);
		}
	}

 	echo json_encode(array('message' => $output, 'error' => $error ));
    die();
}
add_action( 'wp_ajax_change_currency', 'change_currency' );
add_action( 'wp_ajax_nopriv_change_currency', 'change_currency' );	


function get_new_prices($currency_code, $page_id, $no_users){

	$products = array();

	if( have_rows('flexible_content', $page_id ) ):
	    while ( have_rows('flexible_content', $page_id) ) : the_row();
	        if( get_row_layout() == 'packages' ):
	        	
	        	$list_of_packages = get_sub_field('list_of_packages');
	        	if(!empty($list_of_packages)){
	        		foreach ($list_of_packages as $item) {
	        			$package_id = $item['package'];
	        			$products[$package_id] = get_packages_price($no_users, $currency_code, $package_id);
	        		}
	        	}

	        endif;
	    endwhile;
	endif;

	return $products;
}

// get price by users number, currency and package id 
function get_packages_price($no_users, $currency_code, $packages_id) {

	$value_currency = '0';
	$prices = get_field('prices', $packages_id);
	foreach ($prices as $price) {
		if(intval($price['number_of_licenses']) == $no_users ) {
			$label_currency = 'price_' . strtolower($currency_code);
			$value_currency = $price[$label_currency];
		}
	}

	return $value_currency; 
}