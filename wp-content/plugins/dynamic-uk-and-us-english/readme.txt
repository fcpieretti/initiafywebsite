=== Plugin Name ===
Contributors: busblog.net
Donate link: http://busblog.net/925
Tags: british, american, language, english, dynamic, conversion
Requires at least: 2.9
Tested up to: 3.1
Stable tag: 1.2

Dynamically display posts in US or UK English depending on the user's language.

== Description ==

[Plugin Homepage](http://busblog.net/934) | Creator [busblog.net](http://busblog.net)

**What is Dynamic UK and US English?**
A plugin which detects a user�s language using their browser. If the language is en-gb (British), it will display posts in British English, if it detects any other language it will display posts in US English. 

**Why is it useful?**
British readers can feel alienated when reading blogs written in US English and vice versa. This plugin will let them feel more at home.

**Who is this useful for?**
Every single English WordPress based website.

This plugin will change words like colour to color (and vice versa) etc. You can change and add words in settings. By default, a large number of translations are included.

== Installation ==

1. Upload the `dynamic-uk-and-us-english` folder to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Visit plugin settings 'Dynamic UK and US English' and configure.

== Frequently Asked Questions ==

= Where can I get help? =

You can find help at the [Plugin Homepage](http://busblog.net/934).

= How can I whitelist posts? =

For instructions on how to whitelist posts, see [Plugin Homepage](http://busblog.net/934).

= Which conversions are included by default? =

We have posted a [full list](http://busblog.net/920) at busblog.net.

== Screenshots ==

1. Settings Overview

== Changelog ==

= 1.2 =
* Minor visual enhancements.

= 1.1 =
* Initial release.

== Upgrade Notice ==

= 1.2 =
Minor visual enhancements.

= 1.1 =
Initial release.
