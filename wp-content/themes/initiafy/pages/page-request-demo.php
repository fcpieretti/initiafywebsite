<?php /* Template Name: Page Request Demo */
get_template_part('parts/header-type');
//get_header(); ?>

<?php 
    $title_page = get_field('title_page');
	$subtitle_page = get_field('subtitle_page');
	$locations = array();

//$title_page_mobile         = get_field('title_page_mobile');
//$subtitle_page_mobile     = get_field('subtitle_page_mobile');

if (!empty($title_page)) {?>
			<section class="red_circle_background landing_page_hero white_bg">
				<div class="red_circle"></div>
				<div class="container">
					<div class="row">
						<div class="col-md-12">

							<article class="demo-req-heading">
								<h1><?php echo $title_page; ?></h1>
								<?php if (!empty($subtitle_page)) { ?>
									<h2><?php echo $subtitle_page; ?></h2>
								<?php } ?>
							</article>
						</div>
					</div>
				</div>
			</section>

		<?php }?>

<?php if (have_rows('flexible_content')):
    while (have_rows('flexible_content')): the_row();
        if (get_row_layout() == 'content_with_form'): ?>
									<section class="request_demo_page_wrapper customize-landing">
										<div class="container container-small-2 ">
											<div class="row">
												<div class="col-md-7 wrapp-landing-text "> <?php //col-md-7 mobile_reverse wrapp-landing-request ?>
													<?php $featured_image = get_sub_field('featured_image');
        if (!empty($featured_image)) {?>
														<div class="demo-req-featured-img demo-request-img">
															<img src="<?php echo $featured_image['sizes']['request_image']; ?>" alt="<?php echo $featured_image['alt']; ?>">
														</div>
													<?php }?>
													<article class="lp-wrapper-content"><?php
        $content = get_sub_field('content');
        if (!empty($content)) {echo $content;}?>
													</article>
												</div>

												<?php $title_form = get_sub_field('title_form');
        $code_form = get_sub_field('code_form');
        if (!empty($code_form)) {?>
														<div class="col-md-5 wrapp-landing-img "> <?php // col-md-5 wrapp-landing-form ?>
															<div class="register-form-wrapper ">
																<?php if (!empty($title_form)) {?>
																	<h2><?php echo $title_form; ?></h2>
																<?php }?>

																<div class="register-form-wrapper-second">
																	<?php echo $code_form; ?>
																</div>
															</div>
														</div>
													<?php }?>
											</div>
										</div>
									</section>

									<?php
    elseif (get_row_layout() == 'testimonial'):

        $image_testimonial = get_sub_field('image_testimonial');
        $content_testimonial = get_sub_field('content_testimonial');
        $author_testimonial = get_sub_field('author_testimonial');
        $logo_testimonial = get_sub_field('logo_testimonial');
        $type_of_image2 = get_sub_field('type_of_image_testimonial') . '_testimonial';
		$type_of_image = get_sub_field('type_of_image_testimonial');
		$location = get_sub_field('location_testimonial');
		array_push($locations, $location);
        if (!empty($content_testimonial)) {?>
												<section id="<?php echo $location ?>" 
												            style="display:none;"
														     class="testimonial_block testimonial_type_black testm_margin">
													<div class="container container-small-3">
														<div class="row">
															<?php if (!empty($image_testimonial)) {?>
																<div class="col-md-<?php if ($type_of_image == 'large') {echo '6';} else if ($type_of_image == 'medium') {echo '4';} else {echo '3';}?> testimonial-av">
															<div class="testimonial-avatar">
																<img src="<?php echo $image_testimonial['sizes'][$type_of_image2]; ?>" alt="<?php echo $image_testimonial['alt']; ?>">
															</div>
														</div>
													<?php }?>
													<div class="col-md-<?php if ($type_of_image == 'large') {echo '6';} else if ($type_of_image == 'medium') {echo '8';} else {echo '9';}?> testimonial-text">
												<article>
													<?php echo $content_testimonial; ?>
													<?php if (!empty($author_testimonial)) {?>
														<span><?php echo $author_testimonial; ?></span>
													<?php }?>

													<?php if (!empty($logo_testimonial)) {?>
														<div class="company-avatar">
															<img src="<?php echo $logo_testimonial['sizes']['company_single']; ?>" alt="<?php echo $logo_testimonial['alt']; ?>">
														</div>
													<?php }?>
												</article>
											</div>
										</div>
									</div>
								</section>
							<?php }?>

		<?php 
		elseif (get_row_layout() == 'logos'):

    $logos = get_sub_field('logos');

    if ($logos) {?>
								<section class="partners_logos partners_logos_request">
									<div class="container container-small-2">
										<div class="row">
											<div class="col-md-12 list-logos-complete">
												<div class="onload_slider">
													<div class="onload_loading onload_loading_top">
														<img src="<?php echo get_template_directory_uri(); ?>/images/ajax-loader.gif" alt="">
													</div>
													<div class="onload_hide">
														<div class="logos-slider">
														<?php foreach ($logos as $box1) {
        $logo_img = $box1['image'];
        $external_link = $box1['external_link'];
        if (!empty($logo_img)) {?>
																	<div class="single-logo text_center text_left_sm">
																		<?php if (!empty($external_link)) {?>
																			<a href="<?php echo $external_link; ?>" title="" target="_blank">
																		<?php }?>
																			<img src="<?php echo $logo_img['sizes']['logo_partners']; ?>" alt="<?php echo $logo_img['alt']; ?>">
																		<?php if (!empty($external_link)) {?>
																			</a>
																		<?php }?>
																	</div>
																<?php }?>
														<?php }?>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</section>
							<?php }?>
				<?php endif;
endwhile;
endif;?>


<?php if (have_rows('flexible_content')):
    while (have_rows('flexible_content')): the_row();
        if (get_row_layout() == 'content_with_form'):

            $title_form = get_sub_field('title_form');
            $code_form = get_sub_field('code_form');
            if (!empty($code_form) && !empty($title_form)) {?>
									    		<div class="mobile_contact_form demo_request_form">
													<div class="register-form-wrapper mobile-submit-form">
														<h2><?php echo $title_form; ?></h2>

														<div class="register-form-wrapper">
															<?php echo $code_form; ?>
														</div>
													</div>
												</div>
											<?php }?>
								    	<?php endif;
    endwhile;
endif;?>

<?php 

	if (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_FORWARDED_FOR']) {
		$user_ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	} else {
		$user_ip = $_SERVER['REMOTE_ADDR'];
	}

    $ip_array = explode(',', $user_ip);
	$user_ip = $ip_array[0];
?>

<script type="text/javascript">

	jQuery(window).ready(function($) 
	{
		<?php 
			$js_array = json_encode($locations);
			echo "let javascript_array = ". $js_array . ";\n"; 
		 ?>

		jQuery.ajax(
			       "https://extreme-ip-lookup.com/json/<?php echo $user_ip ?>",
			 	{
					success: function(data) {
						let itemToShow = undefined;
						for(let index =0; index < javascript_array.length; index++) {
							if (javascript_array[index] == data.countryCode) {
								itemToShow = data.countryCode;
							}
						}

						if (itemToShow) {
							jQuery("#"+itemToShow).show();
						} else if (javascript_array.length > 0){
							jQuery("#"+ javascript_array[0]).show();
						}
					},
					error: function(error) {
						if (javascript_array.length > 0) {
							jQuery("#"+ javascript_array[0]).show();
						}
					}
				});
	});

</script>

<?php get_footer('simple');