<?php get_header(); ?>


<section class="generic-blog-wrapper">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 title-commun">
				<h1><?php /* If this is a category archive */ if (is_category()) { ?>
						<?php single_cat_title(); ?>
					<?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>
						<?php single_tag_title(); ?>
					<?php /* If this is a daily archive */ } elseif (is_day()) { ?>
						<?php the_time('F jS, Y'); ?> <?php _e('Archives', THEME_TEXT_DOMAIN); ?> 
					<?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
						<?php the_time('F, Y'); ?> <?php _e('Archives', THEME_TEXT_DOMAIN); ?> 
					<?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
						<?php the_time('Y'); ?> <?php _e('Archives', THEME_TEXT_DOMAIN); ?>
					<?php /* If this is an author archive */ } elseif (is_author()) { ?>
						<?php _e('Author: ', THEME_TEXT_DOMAIN); echo get_the_author_meta('display_name'); ?> 
					<?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
						<?php _e('Blog Archives', THEME_TEXT_DOMAIN); ?> 
					<?php } elseif( is_search() )	{ ?>
						<?php _e('Results for :', THEME_TEXT_DOMAIN); ?>  <?php the_search_query() ?>
					<?php } ?></h1>
			</div>
		</div>
		<div class="row"> 
			<div class="col-md-9 col-sm-12 col-xs-12 blog-content">
				<?php 	global $wp_query;
					$count  = 0;
					$count_divs = 0;
					$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
					query_posts( array_merge( $wp_query->query_vars, array( 'ignore_sticky_posts' => true, 'paged' => $paged) ) ); ?>

				<?php if (have_posts())	{ $x = 0; ?>
					<?php while (have_posts())	{ the_post();  
						$id = get_the_ID(); 	
						$count++; 
						$count_divs++;  ?>

						<div class="col-md-4 col-sm-12 col-xs-12 single-res-2">				
							<div class="generic-post-box">
								<a href="<?php echo get_permalink(); ?>" class="desktop-hidden-link"></a>
								<div class="generic-featured-post-img" style="background-image: url('<?php $img = get_field('thumbnail_image',  $id);  
									if(!empty($img)){ echo $img['sizes']['single-small']; } else { echo get_template_directory_uri().'/images/no-post-small.jpg'; }?>');"><a href="<?php echo get_permalink($id); ?>" title="<?php echo get_the_title($id); ?>"></a></div>
								<div class="post-short-description">
									<h4><a href="<?php echo get_permalink(); ?>" title="<?php echo get_the_title(); ?>"><?php 
											$post_title = get_the_title(); 
											if(strlen($post_title) > 55) { echo substr($post_title, 0, 55).'...'; } else { echo $post_title; }
									?></a></h4>
									<h4 class="tablet-second-title"><a href="<?php echo get_permalink(); ?>" title=""><?php echo get_the_title(); ?></a></h4>
									<i><?php echo get_the_date('d F Y'); ?></i>
									<div class="excerpt-wrapp">
										<p><?php echo get_the_excerpt($id); ?> <a href="<?php echo get_permalink($id); ?>" class="rm-post-btn"><?php _e('read more', THEME_TEXT_DOMAIN); ?></a></p>
									</div>
								</div>
							</div>
						</div>
				<?php } ?>

			<?php } ?>

				<div class="col-md-12 extra-large-pagination">
					<div class="text-center"><?php
							global $wp_query;
							$big = 999999999; 
							echo paginate_links( array(
								'base' 			=> str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
								'format' 		=> '?paged=%#%',
								'current' 		=> max( 1, get_query_var('paged') ),
								'total' 		=> $wp_query->max_num_pages,
								'type' 			=> 'list',
								'prev_next'    	=> True,
								'prev_text'    	=> "«" ,
								'next_text'    	=> "»" 
									
							) );
					?></div>		
				</div>		
		
			</div>
			<div class="col-md-3 col-sm-12 col-xs-12 blog-sidebar">
				<?php 
						$page_id 				= get_field('blog_page_id', 'options');
						$banner_title_1 		= get_field('banner_title_1', $page_id);
						$add_button_banner_1 	= get_field('add_button_banner_1', $page_id); 
						$button_title_banner_1 	= get_field('button_title_banner_1', $page_id);
						$button_link_banner_1 	= get_field('button_link_banner_1', $page_id);
						$image_banner_1 		= get_field('image_banner_1', $page_id); ?>

					<div class="single-res-full">
						<div class="training-featured-post">
							<?php if(!empty($banner_title_1)){ ?>
								<h4><?php echo $banner_title_1; ?></h4>
							<?php } ?>
							<?php if(!empty($add_button_banner_1)){ 
								if(!empty($button_title_banner_1) && !empty($button_link_banner_1)){ ?>
									<a href="<?php echo $button_link_banner_1; ?>" class="training-blue-btn"><?php echo $button_title_banner_1; ?></a>
								<?php } ?>
							<?php } ?>
							<?php if(!empty($image_banner_1)){ ?>
								<img src="<?php echo $image_banner_1['sizes']['banner_blog']; ?>" alt="<?php echo $image_banner_1['alt']; ?>">
							<?php } ?>
						</div>
					</div>


				<?php	$banner_title_2 = get_field('banner_title_2', $page_id); 
						$banner_subtitle_2 = get_field('banner_subtitle_2', $page_id); 
						$add_button_banner_2 = get_field('add_button_banner_2', $page_id);
						$button_title_banner_2 = get_field('button_title_banner_2', $page_id);
						$button_link_banner_2 = get_field('button_link_banner_2',  $page_id);  ?>

						<div class="single-res-full">
							<div class="demo-request-box">
								<div class="demo-req-wrapper">
									<?php if(!empty($banner_title_2)){  ?>
										<h3><?php echo $banner_title_2; ?></h3>
									<?php } ?>
									<?php if(!empty($banner_subtitle_2)){ ?>
										<h4><?php echo $banner_subtitle_2; ?></h4>
									<?php } ?>

									<?php if(!empty($add_button_banner_2)){ 
											if(!empty($button_title_banner_2 && !empty($button_link_banner_2))){	?>
												<a href="<?php echo $button_link_banner_2; ?>" class="green-demo-btn"><?php echo $button_title_banner_2; ?></a>
											<?php } ?>
									<?php } ?>
								</div>
							</div>
						</div>

			</div>
		</div>
	</div>
</section>

<?php get_template_part( 'parts/newsletter'); ?> 

<?php get_footer(); 