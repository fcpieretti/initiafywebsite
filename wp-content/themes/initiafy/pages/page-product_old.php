<?php 
/* Template Name: Product Page OLD */
//get_template_part( 'parts/header-type');
//get_header(); ?>

<?php // <!-- body here needs to be #f6f6f6, so, I will use this background for sections --> ?>
<?php 
		$type_of_header = get_field('type_of_header');
		switch ($type_of_header) {
			case 'red-section':
					get_header();
					$title_intro_1 			= get_field('title_intro_1');
			    	$subtitle_intro_1 		= get_field('subtitle_intro_1');
			    	$video_image_intro_1 	= get_field('video_image_intro_1');
			    	$youtube_video_id 		= get_field('youtube_video_id');  ?>

				<section  class="red_circle_background white_bg">
					<div class="red_circle"></div>
					<div class="container"> 
						<div class="row">
							<div class="col-md-12 text_center">
								<?php if(!empty($title_intro_1)){ ?>
									<h1><?php echo $title_intro_1; ?></h1>
								<?php } ?>

								<?php if(!empty($subtitle_intro_1)){ ?>
									<p><?php echo $subtitle_intro_1; ?></p>
								<?php } ?>
								
								<?php if(!empty($youtube_video_id)){ ?>
								<div class="video_preview_img">

									<img src="<?php if(!empty($video_image_intro_1)){ echo $video_image_intro_1['sizes']['video_play']; } else { echo get_template_directory_uri().'/images/no-video.jpg'; } ?>">
									
									<a href="#generic_video_popup" class="play_video_btn" data-iziModal-open="#generic_video_popup">
										<img src="<?php echo get_template_directory_uri();?>/images/white_play.png">
									</a>
									
									<div id="generic_video_popup" class="video_popup general_popup_item" data-izimodal-title="" data-izimodal-iframeURL="<?php echo $youtube_video_id; ?>">
									</div>
								</div>
								<?php } ?>

								<div>
									<a href="#first-section" class="scroll_down_arrow">
										<img src="<?php echo get_template_directory_uri();?>/images/scroll_down_arrow.png" alt="Scroll Down">
									</a>
								</div>
							</div>
						</div>
					</div>
				</section>
				
	<?php 	break;
			case 'image':
				get_header('single');
					$intro_image = get_field('image_hader'); 
					$intro_title = get_field('content_header'); ?>

				<section class="hero blog_hero about_hero hero_header">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-12">
								<div class="img_background" style="background-image: url('<?php  if(!empty($intro_image)){ echo $intro_image['sizes']['hero-single']; } ?>');" alt="<?php echo get_the_title(); ?>">
									<div class="dark_blue_overlay"></div>
									<div class="hero_content">
										<div class="container container-small">
											<div class="col-md-12 text_center">
												<div class="blog-post-title-center">
													<?php if(!empty($intro_title)){ ?>
														<div class="general_hero_text"><?php echo $intro_title; ?></div>
													<?php } ?>
												</div>								
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>

	<?php 	break;
			default: get_header(); break;
		}
?>

<?php 	
	$count = 0; 

	if( have_rows('flexible_content') ):
	    while ( have_rows('flexible_content') ) : the_row(); $count++;
	        if( get_row_layout() == 'steps' ):
	        	$title_steps_2 		= get_sub_field('title_steps_2');
	        	$icons_steps_2 		= get_sub_field('icons_steps_2'); ?>

	        	<section  <?php if($count==2){ echo 'id="first-section"'; } ?> class="normal_padding circle_next_list_section">
					<div class="container">
						<div class="row">
							<?php if(!empty($title_steps_2)){ ?>
								<div class="col-md-12 text_center">
									<h2><?php echo $title_steps_2; ?></h2>
								</div>
							<?php } ?>
							
							<div class="clear"></div>
							<?php	if ($icons_steps_2){ ?>
								<div class="text_center">
									<ul class="circle_next_list">
									<?php foreach ($icons_steps_2 as $box1) { 
											$step_name = $box1['step_name'];
											$step_icon = $box1['step_icon'];

											if(!empty($step_name)){ ?>					
												<li>
													<span class="icon" style="background-image: url(<?php if(!empty($step_icon)){  echo $step_icon['sizes']['icon_step2']; } ?>);"></span>
													<?php echo $step_name; ?>
												</li>
											<?php } ?>
									<?php } ?>
									</ul>
								</div>
							<?php } ?>
						</div>
					</div>
				</section>

			<?php elseif( get_row_layout() == 'left_content_right_image' ):
					$title_text_3 		= get_sub_field('title_text_3');
					$content_text_3 	= get_sub_field('content_text_3'); 
					$image_text_3 		= get_sub_field('image_text_3'); ?>		

				<?php // <!-- here is the big sections group--> ?>
			
				<div  <?php if($count==2){ echo 'id="first-section"'; } ?> class="section-timg section-left-content-right-image">
					<div class="for_md_lg">
						<section class="product_triangle_1_parent_parent">
							<div class="product_triangle_1_parent" style="display: none;">
								<div class="product_triangle_1" style="display: none;"></div>
							</div>
						</section>
						<section class="like_black_form_text white_p_dekstop black_bg_dekstop">
							<div class="container">
								<div class="row item_effect">
									<div class="col-md-5 vcenter item_effect_text scale_text half_small_desktop keep_small">
										<?php if(!empty($title_text_3)){ ?>
											<h2 class="style_h2"><?php echo $title_text_3; ?></h2>
										<?php } ?>
										<?php if(!empty($content_text_3)){ echo $content_text_3; } ?>
									</div>

									<?php if(!empty($image_text_3)){ ?>
										<div class="col-md-7 vcenter text_right item_effect_image scale_image half_small_desktop keep_large ">
											
											<?php if(strpos($image_text_3['url'], '.gif') !== false) { ?>
												<img src="<?php echo $image_text_3['url']; ?>" alt="<?php echo $image_text_3['alt']; ?>" 	class="product_first_image">
											<?php } else { ?>
												<img src="<?php echo $image_text_3['url'];?>" alt="<?php echo $image_text_3['alt']; ?>" 	class="product_first_image">
											<?php } ?>
											
										</div>
									<?php } ?>
								</div>
							</div>
						</section>
					</div>

					<div class="for_xs_sm">
						<section class="like_black_form_text">
							<div class="container">
								<div class="row">
									<div class="col-md-6 text_center">
										<?php if(!empty($title_text_3)){ ?>
											<h2><?php echo $title_text_3; ?></h2>
										<?php } ?>
										<?php if(!empty($content_text_3)){ echo $content_text_3; } ?>
									</div>
								</div>
							</div>
						</section>

						<?php if(!empty($image_text_3)){ ?>
							<div class="background_only_parent" style="height: 0px;">
								<div class="background_only position_relative">
									<section class="product_triangle_m1_parent_parent">
										<div class="product_triangle_m1_parent" style="display: none;">
											<div class="product_triangle_m1" style="display: none;"></div>
										</div>
									</section>
									<section class="background_body">
									</section>
									<section class="product_triangle_m2_parent_parent">
										<div class="product_triangle_m2_parent" style="display: none;">
											<div class="product_triangle_m2" style="display: none;"></div>
										</div>
									</section>
								</div>
							</div>
							<section class="like_black_form_text">
								<div class="container">
									<div class="row">
										<div class="col-md-6 text_center section-img-small">
											<?php if(strpos($image_text_3['url'], '.gif') !== false) { ?>
												<img src="<?php echo $image_text_3['url'];?>" alt="<?php echo $image_text_3['alt']; ?>" class="special_margin_on_mobile_tablet">
											<?php } else { ?>
												<img src="<?php echo $image_text_3['url'];?>" alt="<?php echo $image_text_3['alt']; ?>" class="special_margin_on_mobile_tablet">
											<?php } ?>
										</div>
									</div>
								</div>
							</section>
						<?php } ?>
					</div>
				</div>

			<?php elseif( get_row_layout() == 'left_image_right_content' ): 
					$title_text_4 			= get_sub_field('title_text_4');
					$content_text_4 		= get_sub_field('content_text_4');
					$image_text_4 			= get_sub_field('image_text_4');
					$add_button_text_4 		= get_sub_field('add_button_text_4');
					$button_label_text_4 	= get_sub_field('button_label_text_4');
					$button_link_text_4 	= get_sub_field('button_link_text_4'); 
					$type_of_button_text_4  = get_sub_field('type_of_button_text_4');
					$button_file_text_4 	= get_sub_field('button_file_text_4'); ?>		

					<div  <?php if($count==2){ echo 'id="first-section"'; } ?> class="section-timg section-left-image-right-content <?php if($count!=2){ echo 'inner-section-text-img'; } ?>">	
						<div class="for_md_lg">
							<section class="like_black_form_text second_product_image">
								<?php if($count!=2){ ?>
								<div class="position_relative product_triangle_2_parent_parent">
									<div class="product_triangle_2_parent" style="display: none;">
										<div class="product_triangle_2" style="display: none;">
										</div>
									</div>
								</div>
								<?php } ?>
								
								<div class="container">
									<div class="row item_effect">
										<?php if(!empty($image_text_4)){ ?>
											<div class="col-md-6 vcenter second_product_image_bottom item_effect_image item_effect_white_image text_left scale_image keep_large">
												<?php if(strpos($image_text_4['url'], '.gif') !== false) { ?>
													<img src="<?php echo $image_text_4['url']; ?>" alt="<?php echo $image_text_4['alt']; ?>">
												<?php } else { ?>
													<img src="<?php echo $image_text_4['url']; ?>" alt="<?php echo $image_text_4['alt']; ?>">
												<?php } ?>
											</div>
										<?php } ?>
										<div class="col-md-6 vcenter item_effect_text item_effect_white_text scale_text keep_small">
											<?php if(!empty($title_text_4)){ ?>
												<h2 class="style_h2"><?php echo $title_text_4; ?></h2>
											<?php } ?>
											 
											<?php if(!empty($content_text_4)){ echo $content_text_4; } ?>

											<br>
											<?php if(!empty($add_button_text_4)){
													if(!empty($button_label_text_4) ){ ?>
														<div class="text_center">
															<?php switch ($type_of_button_text_4) {
																case 'link': ?>
																	<a href="<?php echo $button_link_text_4; ?>" class="btn rounded_btn red_btn"><?php echo $button_label_text_4; ?></a>
															<?php 	break;
																case 'file': 
																	if(!empty($button_file_text_4 )){ 	?>

																		<a href="<?php echo $button_file_text_4['url']; ?>" target="_blank" class="btn rounded_btn red_btn"><?php echo $button_label_text_4; ?></a>

																	<?php } ?>
															<?php	break;	
																
															} ?>
															
														</div>
														<br>
													<?php } ?>
											<?php } ?>
										</div>
									</div>
								</div>
							</section>
						</div>

						<div class="for_xs_sm">
							<section class="like_black_form_text">
								<div class="container">
									<div class="row">
										<div class="col-md-6 text_center">
											<?php if(!empty($title_text_4)){ ?>
												<h2 class="style_h2"><?php echo $title_text_4; ?></h2>
											<?php } ?>
											<?php if(!empty($content_text_3)){ echo $content_text_3; } ?>

											<br>
											<?php if(!empty($add_button_text_4)){
													if(!empty($button_label_text_4) && !empty($button_link_text_4)){ ?>
														<div class="text_center">
															<a href="<?php echo $button_link_text_4; ?>" class="btn rounded_btn red_btn"><?php echo $button_label_text_4; ?></a>
														</div>
														<br>
												<?php } ?>
											<?php } ?>
										</div>
									</div>
								</div>
							</section>
 
							<?php if(!empty($image_text_4)){ ?>
								<section class="like_black_form_text sectiun_with_floating_image_tablet_mobile">
									<div class="container-fluid">
										<div class="row">
											<div class="col-md-6 section-img-small2" >
												<?php if(strpos($image_text_4['url'], '.gif') !== false) { ?>
													<img src="<?php echo $image_text_4['url']; ?>" alt="<?php echo $image_text_4['alt']; ?>">
												<?php } else { ?>
													<img src="<?php echo $image_text_4['url']; ?>" alt="<?php echo $image_text_4['alt']; ?>">
												<?php } ?>
											</div>
										</div>
									</div>
								</section>
							<?php } ?>
						</div>
					</div>

			<?php elseif( get_row_layout() == 'left_content_right_image_no_margins' ): 
					$title_text_5 		= get_sub_field('title_text_5');
					$content_text_5 	= get_sub_field('content_text_5');
					$image_text_5 		= get_sub_field('image_text_5');  ?>

					<div  <?php if($count==2){ echo 'id="first-section"'; } ?> class="section-timg section-left-content-right-image-no-margins bg_blank_section">	
						<div class="for_md_lg bg_blank_section">	
							<section class="like_black_form_text second_product_image bg_blank_section">
								<div class="position_relative product_triangle_3_parent_parent">
									<div class="product_triangle_3_parent" style="display: none;">
										<div class="product_triangle_3" style="display: none;">
										</div>
									</div>
								</div>
							</section>
							<section class="like_black_form_text white_p_dekstop white_p_tablet_mobile black_bg_dekstop">
								<div class="container">
									<div class="row item_effect">
										<div class="col-md-5 vcenter item_effect_text scale_text keep_small">
											<?php if(!empty($title_text_5)){ ?>
												<h2 class="style_h2"><?php echo $title_text_5; ?></h2>
											<?php } ?>

											<?php if(!empty($content_text_5)){ echo $content_text_5; } ?>
										</div>

										<?php if(!empty($image_text_5)){ ?>
											<div class="col-md-7 vcenter text_right section-img-small item_effect_image scale_image keep_large">
												<?php if(strpos($image_text_5['url'], '.gif') !== false) { ?>
													<img src="<?php echo $image_text_5['url']; ?>" alt="<?php echo $image_text_5['alt']; ?>">
												<?php } else { ?>
													<img src="<?php echo $image_text_5['url']; ?>" alt="<?php echo $image_text_5['alt']; ?>">
												<?php } ?>
											</div>
										<?php } ?>
									</div>
								</div>
							</section>
						</div>

						<div class="for_xs_sm">
							<section class="product_triangle_m3_parent_parent">
								<div class="product_triangle_m3_parent" style="display: none;">
									<div class="product_triangle_m3" style="display: none;"></div>
								</div>
							</section>
							<section class="like_black_form_text black_bg white_p">
								<div class="container">
									<div class="row">
										<div class="col-md-6 text_center title_not_change">
											<?php if(!empty($title_text_5)){ ?>
												<h2><?php echo $title_text_5; ?></h2>
											<?php } ?>

											<?php if(!empty($content_text_5)){ echo $content_text_5; } ?>
										</div>
									</div>
								</div>
							</section>
							<section class="product_triangle_m4_parent_parent">
								<div class="product_triangle_m4_parent" style="display: none;">
									<div class="product_triangle_m4" style="display: none;">
									</div>
								</div>
							</section>

							<?php if(!empty($image_text_5)){ ?>
								<section class="like_black_form_text">
									<div class="container">
										<div class="row">
											<div class="col-md-6 text_center section-img-small">
												<?php if(strpos($image_text_4['url'], '.gif') !== false) { ?>
													<img src="<?php echo $image_text_5['url']; ?>" alt="<?php echo $image_text_5['alt']; ?>">
												<?php } else { ?>
													<img src="<?php echo $image_text_5['url']; ?>" alt="<?php echo $image_text_5['alt']; ?>">
												<?php } ?>
											</div>
										</div>
									</div>
								</section> 
							<?php } ?>
						</div>
					</div>

	        <?php  endif;
	    endwhile;
	endif; ?>

<?php get_footer();
