<!DOCTYPE html>
<html <?php language_attributes();?> class="no-js">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<?php wp_head();?>
		<base href="<?php echo get_template_directory_uri() ?>/"><!--[if IE]></base><![endif]-->
	</head>
	<body <?php body_class();?>>
		<?php  
		global $header_img_url;	
		$header_class = 'transparent resources_landing_page';?>
		<header class="<?php echo $header_class;?>">
				<div class="header_bg_parent">
					<div class="dark_blue_overlay resorces_landing_page_header"></div>
				</div>
			
			<!-- this is the header content for +1200px -->
			<div class="container">
				<div class="row">
					<div class="col-md-12 table_parent">
						<div class="table_cell text_center">
							<a href="<?php echo get_home_url();?>" class="header_logo resources_pink_logo">
								<img src="<?php echo get_template_directory_uri();?>/images/Initiafy-logo-pink.svg" alt="<?php echo get_bloginfo('name');?>">
							</a>
						</div>
					</div>
				</div>
			</div>
			<!-- end of header for +1200px -->
		</header>
