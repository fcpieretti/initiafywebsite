<?php /* Template Name: Product Page */
//get_template_part( 'parts/header-type');
//get_header(); ?>

<?php // <!-- body here needs to be #f6f6f6, so, I will use this background for sections --> ?>
<?php 
		$type_of_header = get_field('type_of_header');
		switch ($type_of_header) {
			case 'red-section':
					get_header();
					$title_intro_1 			= get_field('title_intro_1');
			    	$subtitle_intro_1 		= get_field('subtitle_intro_1');
			    	$video_image_intro_1 	= get_field('video_image_intro_1');
			    	$youtube_video_id 		= get_field('youtube_video_id');  ?>

				<section  class="red_circle_background white_bg">
					<div class="red_circle"></div>
					<div class="container"> 
						<div class="row">
							<div class="col-md-12 text_center">
								<?php if(!empty($title_intro_1)){ ?>
									<h1><?php echo $title_intro_1; ?></h1>
								<?php } ?>

								<?php if(!empty($subtitle_intro_1)){ ?>
									<p><?php echo $subtitle_intro_1; ?></p>
								<?php } ?>
								
								<?php if(!empty($youtube_video_id)){ ?>
								<div class="video_preview_img">

									<img src="<?php if(!empty($video_image_intro_1)){ echo $video_image_intro_1['sizes']['video_play']; } else { echo get_template_directory_uri().'/images/no-video.jpg'; } ?>">
									
									<a href="#generic_video_popup" class="play_video_btn" data-iziModal-open="#generic_video_popup">
										<img src="<?php echo get_template_directory_uri();?>/images/white_play.png" class="btn-play-video-product-page">
									</a>
									
									<div id="generic_video_popup" class="video_popup general_popup_item" data-izimodal-title="" data-izimodal-iframeURL="<?php echo $youtube_video_id; ?>">
									</div>
								</div>
								<?php } ?>

								<div>
									<a href="#first-section" class="scroll_down_arrow">
										<img src="<?php echo get_template_directory_uri();?>/images/scroll_down_arrow.png" alt="Scroll Down">
									</a>
								</div>
							</div>
						</div>
					</div>
				</section>
				
	<?php 	break;
			case 'image':
				get_header('single');
					$intro_image = get_field('image_hader'); 
					$intro_title = get_field('content_header'); ?>

				<section class="hero blog_hero about_hero hero_header">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-12">
								<div class="img_background" style="background-image: url('<?php  if(!empty($intro_image)){ echo $intro_image['sizes']['hero-single']; } ?>');" alt="<?php echo get_the_title(); ?>">
									<div class="dark_blue_overlay"></div>
									<div class="hero_content">
										<div class="container container-small">
											<div class="col-md-12 text_center">
												<div class="blog-post-title-center">
													<?php if(!empty($intro_title)){ ?>
														<div class="general_hero_text"><?php echo $intro_title; ?></div>
													<?php } ?>
												</div>								
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>

	<?php 	break;
			default: get_header(); break;
		}
?>

<?php 	
	$count = 0; 

	if( have_rows('flexible_content') ){
	    while ( have_rows('flexible_content') ) { the_row(); $count++;
	        if( get_row_layout() == 'steps' ){
	        	$title_steps_2 		= get_sub_field('title_steps_2');
	        	$icons_steps_2 		= get_sub_field('icons_steps_2'); ?>

	        	<section  <?php if($count==2){ echo 'id="first-section"'; } ?> class="normal_padding circle_next_list_section">
					<div class="container">
						<div class="row">
							<?php if(!empty($title_steps_2)){ ?>
								<div class="col-md-12 text_center">
									<h2><?php echo $title_steps_2; ?></h2>
								</div>
							<?php } ?>
							
							<div class="clear"></div>
							<?php	if ($icons_steps_2){ ?>
								<div class="text_center">
									<ul class="circle_next_list">
									<?php foreach ($icons_steps_2 as $box1) { 
											$step_name = $box1['step_name'];
											$step_icon = $box1['step_icon'];

											if(!empty($step_name)){ ?>					
												<li>
													<span class="icon" style="background-image: url(<?php if(!empty($step_icon)){  echo $step_icon['sizes']['icon_step2']; } ?>);"></span>
													<?php echo $step_name; ?>
												</li>
											<?php } ?>
									<?php } ?>
									</ul>
								</div>
							<?php } ?>
						</div>
					</div>
				</section>

			<?php  } elseif( get_row_layout() == 'section_final_image_right_text_left_large' ){ 
							$color_final2 		= get_sub_field('color_final2_large');
							$title_final2 		= get_sub_field('title_final2_large');
							$content_final2		= get_sub_field('content_final2_large'); 
							$image_final2		= get_sub_field('image_final2_large');
							$add_button_final2 	= get_sub_field('add_button_final2_large');
							$type_of_button_final2 = get_sub_field('type_of_button_final2_large');
							$button_label_final2= get_sub_field('button_label_final2_large');
							$button_link_final2 = get_sub_field('button_link_final2_large');
							$button_file_final2 = get_sub_field('button_file_final2_large');
							$remove_shape_top_final2	= get_sub_field('remove_shape_top_final2_large');
							$remove_shape_bottom_final2 = get_sub_field('remove_shape_bottom_final2_large'); ?>


							<section <?php if($count==2){ echo 'id="first-section"'; } ?> class="section-timg-final type_2">
								
								<?php if(empty($remove_shape_top_final2)){  ?>
									<div class="product_triangle_1_parent_parent <?php echo $color_final2; ?>_color">
										<div class="product_triangle_3_parent">
											<div class="product_triangle_3"></div>
										</div>
									</div>
								<?php } ?>

								<div class="wrapper-container <?php echo $color_final2; ?>_color_bg  <?php if(empty($remove_shape_top_final2)){ echo 'no-margin-top'; } if(empty($remove_shape_bottom_final2)){ echo ' no-margin-bottom'; }   ?>">
									<div class="container">
										<div class="row">

											<div class="col-md-5 vcenter item_effect_text scale_text keep_small  text-section-large-general">
												<?php if(!empty($title_final2)){ ?>
													<h3 class="style_h2"><?php echo $title_final2; ?></h3>
												<?php } ?>

												<?php if(!empty($content_final2)){ ?>
												<div class="general_content">
													<?php echo $content_final2; ?>
												</div>
												<?php } ?>

												<?php if(!empty($add_button_final2)){ ?>
													<br>
													<div class="button-timg text_center"><?php	
														switch ($type_of_button_final2) {
															case 'link':
																if(!empty($button_label_final2) && !empty($button_link_final2)){ ?>
																	<a href="<?php echo $button_link_final2; ?>" class="btn rounded_btn red_btn"><?php echo $button_label_final2; ?></a>
																<?php } 
																break;
															case 'file':
																if(!empty($button_label_final2) && !empty($button_file_final2)){ ?>
																	<a href="<?php echo $button_file_final2['url']; ?>"  target="_blank" class="btn rounded_btn red_btn"><?php echo $button_label_final2; ?></a>
																<?php } 
																break;
															default: break;
														} ?>
													</div>
												<?php } ?>
											</div>

											<?php if(!empty($image_final2)){  ?>
												<div class="col-md-7 vcenter text_right section-img-small item_effect_image scale_image keep_large  image-section-large-general">
													<img src="<?php echo $image_final2['url']; ?>" alt="<?php echo $image_final2['alt']; ?>" class="product_first_image">
												</div>
											<?php } ?>

											
											<div class="clear"></div>
										</div>
									</div>
								</div>

								<?php if(empty($remove_shape_bottom_final2)){  ?>
									<div class="first_black_triangle_parent <?php echo $color_final2; ?>_color">
										<div class="first_black_triangle_bg to_bottom">
											<div class="first_white_triangle_bg"></div>
										</div>	
									</div>
								<?php } ?>
							</section>
							
			<?php  } elseif( get_row_layout() == 'section_final_image_left_text_right_large' ){ 	

							$color_final1	 	= get_sub_field('color_final1_large');
							$title_final1 		= get_sub_field('title_final1_large');
							$content_final1 	= get_sub_field('content_final1_large');
							$image_final1 		= get_sub_field('image_final1_large');
							$add_button_final1  = get_sub_field('add_button_final1_large');
							$type_of_button_final1 = get_sub_field('type_of_button_final1_large');
							$button_label_final1 = get_sub_field('button_label_final1_large');
							$button_link_final1 = get_sub_field('button_link_final1_large');
							$button_file_final1 = get_sub_field('button_file_final1_large');
							$remove_shape_top_final1	= get_sub_field('remove_shape_top_final1_large');
							$remove_shape_bottom_final1 = get_sub_field('remove_shape_bottom_final1_large'); ?>

							<section <?php if($count==2){ echo 'id="first-section"'; } ?> class="section-timg-final type_1">
								<?php if(empty($remove_shape_top_final1)){  ?>
									<div class="product_triangle_1_parent_parent <?php echo $color_final1; ?>_color">
										<div class="product_triangle_3_parent">
											<div class="product_triangle_3"></div>
										</div>
									</div>
								<?php } ?>

								<div class="wrapper-container <?php echo $color_final1; ?>_color_bg <?php if(empty($remove_shape_top_final1)){ echo 'no-margin-top'; } if(empty($remove_shape_bottom_final1)){ echo ' no-margin-bottom'; }   ?>">
									<div class="container">
										<div class="row">

											<?php if(!empty($image_final1)){  ?>
												<div class="col-md-7 vcenter text_left section-img-small item_effect_image scale_image keep_large  image-section-large-general hide_on_tablet">
													<img src="<?php echo $image_final1['url']; ?>" alt="<?php echo $image_final1['alt']; ?>">
												</div>
											<?php } ?>
											
											<div class="col-md-5  vcenter item_effect_text scale_text keep_small  text-section-large-general">
												<?php if(!empty($title_final1)){ ?>
													<h3 class="style_h2"><?php echo $title_final1; ?></h3>
												<?php } ?>

												<?php if(!empty($content_final1)){ ?>
												<div class="general_content">
													<?php echo $content_final1; ?>
												</div>
												<?php } ?>

												<?php if(!empty($add_button_final1)){ ?>
													<br>
													<div class="button-timg text_center"><?php 
														switch ($type_of_button_final1) {
														case 'link':
															if(!empty($button_label_final1) && !empty($button_link_final1)){ ?>
																<a href="<?php echo $button_link_final1; ?>" class="btn rounded_btn red_btn btn-<?php echo strtolower(str_replace(' ', '-', $button_label_final1)) ?>"><?php echo $button_label_final1; ?></a>
														
															<?php } 
															break;
														case 'file': 
															if(!empty($button_label_final1) && !empty($button_file_final1)){ ?>
																<a href="<?php echo $button_file_final1['url']; ?>" target="_blank" class="btn rounded_btn red_btn"><?php echo $button_label_final1; ?></a>
															<?php } 
															break;
														default: break;
													} ?>
													</div>
												<?php } ?>
											</div>

											<?php if(!empty($image_final1)){  ?>
												<div class="col-md-7 vcenter text_left section-img-small item_effect_image scale_image keep_large  image-section-large-general show_on_tablet">
													<img src="<?php echo $image_final1['url']; ?>" alt="<?php echo $image_final1['alt']; ?>">
												</div>
											<?php } ?>

											
											
											<div class="clear"></div>
										</div>
									</div>
								</div>

								<?php if(empty($remove_shape_bottom_final1)){  ?>
									<div class="first_black_triangle_parent <?php echo $color_final1; ?>_color">
										<div class="first_black_triangle_bg to_bottom">
											<div class="first_white_triangle_bg"></div>
										</div>	
									</div>
								<?php } ?>
							</section>


	        <?php  }
	   	}
	} ?>

<?php get_footer();
