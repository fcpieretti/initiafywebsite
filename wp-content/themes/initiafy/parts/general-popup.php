<?php // <!-- newsletter subscribe popup -->
	$activate_newsletter_popup = get_field('activate_newsletter_popup');
	if(!empty($activate_newsletter_popup)){
		$content_newsletter_popup 		= get_field('content_newsletter_popup');
		$image_newsletter_popup 		= get_field('image_newsletter_popup');
		$code_for_newsletter_form_popup 	= get_field('code_for_newsletter_form_popup'); 

		$show_when_form_popup 			= get_field('show_when_form_popup');
		
		if(!empty($code_for_newsletter_form_popup)) { ?>
			<div id="modal_newsletter_popup" class="newsletter_form" data-izimodal-title="" style="display: none">
				<?php if(!empty($image_newsletter_popup)){  ?>
					<div class="header-icon">
						<img src="<?php echo $image_newsletter_popup['sizes']['icon_newsletter']; ?>" alt="<?php echo $image_newsletter_popup['alt']; ?>">
					</div>	
				<?php } ?>

				<?php if(!empty($content_newsletter_popup)){ ?>
					<div class="newsletter-title">
						<?php echo $content_newsletter_popup; ?>
					</div>
				<?php } ?>
				 
				<div class="register-form-wrapper">
					<?php echo $code_for_newsletter_form_popup; ?>
				</div>
			</div>

			<?php switch ($show_when_form_popup) {
				case 'after-x-seconds': 
						$seconds_form_popup	= get_field('seconds_form_popup'); ?>
						
						<script type="text/javascript">
							jQuery(window).load(function($) {
								if( jQuery('.newsletter_form').length >0 ) {

									setTimeout(function(){
									  	var count = 0; 
									  	// test if an other izimodal is not opened 
										jQuery( ".general_popup_item" ).each(function() {
									 		//if(jQuery(this).hasClass('isAttached')){
									 		if(jQuery(this).css('display') == 'block'){	
									 			count++;
									 		}
										});

										if(count == 0) {
											var cookie_val = jQuery.cookie('general');  
											if(cookie_val != 'false'){

												var cookie_val = jQuery.cookie('general');
										        jQuery.cookie('general', 'true', {
										            //expires: 1,
										            path: '/'
										        });

										        // show popup
										        jQuery('.newsletter_form').iziModal({
											  		zindex: 999999,
											  		width: 720,
											  		bodyOverflow: true,
											  		autoOpen: 1
											  		//autoOpen: <?php // echo intval($seconds_form_popup) * 1000; ?>,
											  	});

										        // Closing
												jQuery(document).on('closing', '.newsletter_form', function (e) {
													jQuery.cookie('general', 'false', {  path: '/' });
												});
											}  
										}

									}, <?php echo intval($seconds_form_popup) * 1000; ?> );
								}
							});
						</script>
					
		<?php 		break;
				case 'when-user-leave-page': ?>
					<script type="text/javascript">
						jQuery(window).load(function($) {
							if( jQuery('.newsletter_form').length >0 ) {

								var cookie_val = jQuery.cookie('general');  
								if(cookie_val != 'false'){

									var cookie_val = jQuery.cookie('general');
							        jQuery.cookie('general', 'true', {
							            //expires: 1,
							            path: '/'
							        });
							    
							        // show popup
							        jQuery(document).mouseleave(function () {
							       		
							       		var count = 0; 
									  	// test if an other izimodal is not opened 
										jQuery( ".general_popup_item" ).each(function() {
									 		//if(jQuery(this).hasClass('isAttached')){
									 		if(jQuery(this).css('display') == 'block'){	
									 			count++;
									 		}
										});

										if(count == 0) {	
								       		if(!jQuery('.newsletter_form').hasClass('iziModal')) {	
								       			jQuery('.newsletter_form').iziModal({
								       				zindex: 999999,
											  		width: 720,
											  		bodyOverflow: true,
											  		autoOpen: 1,
								       			});
								       		}
							       		}
									});

							        // Closing
									jQuery(document).on('closing', '.newsletter_form', function (e) {
										jQuery.cookie('general', 'false', {  path: '/' });
									});
								}  
							}
						}); 
					</script>

		<?php 		break;
				case 'on-half-scroll': ?>

					<script type="text/javascript">
						jQuery(window).load(function($) {
							jQuery(window).scroll(function () { 
								if (jQuery(window).scrollTop() > jQuery('body').height() / 2) {
									
									var count = 0; 
									jQuery( ".general_popup_item" ).each(function() {
								 		//if(jQuery(this).hasClass('isAttached')){
								 		if(jQuery(this).css('display') == 'block'){
								 			count++;
								 		}
								 		
									});

									//console.log(count);

									if(count == 0) {	
										//console.log('half');
										var cookie_val = jQuery.cookie('general');  
										if(cookie_val != 'false'){

											var cookie_val = jQuery.cookie('general');
									        jQuery.cookie('general', 'true', {
									            //expires: 1,
									            path: '/'
									        });

											if(!jQuery('.newsletter_form').hasClass('iziModal')) {	
								       			jQuery('.newsletter_form').iziModal({
								       				zindex: 999999,
											  		width: 720,
											  		bodyOverflow: true,
											  		autoOpen: 1,
								       			});
								       		}

									        // Closing
											jQuery(document).on('closing', '.newsletter_form', function (e) {
												jQuery.cookie('general', 'false', {  path: '/' });
											});
								       	}	
									}
								} 
							});
						}); 	
					</script>
		<?php 		break;
				default: break;
			} ?>

		<?php } ?>
	<?php } ?>
<?php // <!-- //newsletter subscribe popup --> ?>