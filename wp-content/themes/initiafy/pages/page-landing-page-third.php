<?php /* 
Template Name: Landing Page 3
Template Post Type: page, resource 
*/
 
//get_header('resources-dark-landing-resources'); 
get_header('resources-dark-landing-resources');?>

<section class="resources-heading" >
	<div class="container">
		<div class="row">
			<div class="col-md-12"> 
				<article class="resources-langing-title">
					<h1><?php echo get_the_title(); ?></h1>
				</article>
			</div>
		</div>
	</div>
</section>

<section class="resources-description">
	<div class="container container-small-2">
		<div class="row">
			
			<div class="col-md-6">
				<div class="resources-featured-img"><?php 
					$img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'resource_single');
					if($img[0]){ ?>
						<img src="<?php echo $img[0]; ?>" alt="<?php echo get_the_title(); ?>">	
					<?php } ?>
				</div>
				<?php /*$button_label_resource 	= get_field('button_label_resource'); 
						$iframe_form_resource 	= get_field('iframe_form_resource'); 
						if(!empty($button_label_resource) && !empty($iframe_form_resource)){ ?>
							<a href="#modal-iframe" class="resources-width-btn light_blue_btn_color resources-width-btn-popup open-iframe" ><?php echo $button_label_resource; ?></a>
				<?php 	} */ ?>
				<?php 	$button_label_resource 	= get_field('button_label_resource');
						$button_link_resource 	= get_field('button_link_resource'); 
						if(!empty($button_label_resource) && !empty($button_link_resource)){ ?>
							<a href="<?php echo $button_link_resource; ?>" class="resources-width-btn light_blue_btn_color" ><?php echo $button_label_resource; ?></a>
				<?php  	} ?>
			</div>
			
			<div class="col-md-6">
				<article class="resources-short-description general-top-content individual_resource_description">
					<div class="general_content">
						<?php if (have_posts()) : while (have_posts()) : the_post();?>
						    <?php the_content(); ?>
						<?php endwhile; endif; ?>
			   	 	</div>

					<?php 	$button_label_resource = get_field('button_label_resource'); 
							$iframe_form_resource = get_field('iframe_form_resource'); 
							if(!empty($button_label_resource) && !empty($iframe_form_resource)){ ?>
								<a href="#modal-iframe" class="resources-width-btn light_blue_btn_color resources-width-btn-popup open-iframe" ><?php echo $button_label_resource; ?></a>
					<?php } ?>
				</article>
			</div>
		</div>
	</div>
</section>

<?php /* if(!empty($iframe_form_resource)){ ?>
<div class="newsletter_form general_popup iframe-form" id="modal-iframe" data-izimodal-title=" " style="display: none">
	<div class="register-form-wrapper">
		<?php echo $iframe_form_resource; ?>
	</div>
</div> 
<?php } */ ?>

<?php get_footer('simple');  
