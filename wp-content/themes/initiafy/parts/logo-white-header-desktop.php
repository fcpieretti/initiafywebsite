<?php 	$logo_white_svg = get_field('logo_white_svg', 'options'); 
		if(!empty($logo_white_svg)){ ?>
			<a href="<?php echo get_home_url();?>" class="header_logo white_logo_view">
				<img src="<?php echo $logo_white_svg['sizes']['logo_size']; ?>" alt="<?php echo get_bloginfo('name'); ?>" class="for-desktop">							
			</a>
<?php 	}  ?>