<?php 
get_header('resources-dark-landing-resources');?>


<section class="red_circle_background landing_page_hero white_bg">
	<div class="red_circle"></div>
</section>

<section class="generic-landing-page-wrapper generic-up-top">
	<div class="container"> 
		<div class="row">
			<div class="col-md-7 wrapp-landing-text">
				<h1><?php 	echo get_the_title(); ?></h1>

				<?php /*?>
					<?php 	$img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'featured_landing_simple');
							if($img[0]){ ?>
								<div class="featured-img">
									<img src="<?php echo $img[0]; ?>" alt="<?php echo get_the_title(); ?>">
								</div>
					<?php 	} */?>
				
				<article class="generic-landing-page-txt general_content">
					<?php if (have_posts()) : while (have_posts()) : the_post();?>
						<?php the_content(); ?>
					<?php endwhile; endif; ?>
				</article>
			</div>

			<?php 	$title_form = get_field('title_form'); 
					$code_form = get_field('code_form');
					if(!empty($code_form)){  ?>
						<div class="col-md-5 wrapp-landing-img">
							<div class="register-form-wrapper mobile_hide_register_form">
								<?php if(!empty($title_form)){ ?>
									<h2><?php echo $title_form; ?></h2>
								<?php } ?>
								
								<div class="register-form-wrapper-sec">
									<?php echo $code_form; ?> 
								</div>
							</div>
						</div>
			<?php 	} ?>
		</div>
	</div>
</section>

<?php $title_form_2 = get_field('title_form_2');
if(!empty($code_form)){ ?>
	<div class="mobile_contact_form demo_request_form landing_page_contact_form">
		<div class="register-form-wrapper mobile-submit-form">
			<?php if(!empty($title_form_2)){ ?>
				<h2><?php echo $title_form_2; ?></h2>
			<?php } ?>

			<div class="register-form-wrapper-sec">
				<?php echo $code_form; ?>
			</div>
		</div>
	</div>
<?php } ?>

<?php /*


<section class="resources-heading">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<article class="resources-langing-title">
					<h1><?php echo get_the_title(); ?></h1>
				</article>
			</div>
		</div>
	</div>
</section>

<section class="resources-description">
	<div class="container container-small-2">
		<div class="row">
			<?php /*
			<div class="col-md-6">
				<div class="resources-featured-img"><?php 
					$img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'resource_single');
					if($img[0]){ ?>
						<img src="<?php echo $img[0]; ?>" alt="<?php echo get_the_title(); ?>">	
					<?php } ?>
				</div>
				<?php 	$button_label_resource = get_field('button_label_resource'); 
						$button_link_resource = get_field('button_link_resource');
						if(!empty($button_label_resource) && !empty($button_link_resource)){ ?>
							<a href="<?php echo $button_link_resource; ?>" class="resources-width-btn light_blue_btn_color" target="_blank"><?php echo $button_label_resource; ?></a>
					<?php } ?>
			</div>
			<?php *//* ?>

			<div class="col-md-12">
				<article class="resources-short-description general-top-content">
					<div class="general_content">
						<?php if (have_posts()) : while (have_posts()) : the_post();?>
						    <?php the_content(); ?>
						<?php endwhile; endif; ?>
			   	 	</div>

					<?php 	$button_label_resource = get_field('button_label_resource'); 
							$button_link_resource = get_field('button_link_resource'); 
							if(!empty($button_label_resource) && !empty($button_link_resource)){ ?>
								<a href="<?php echo $button_link_resource; ?>" class="resources-width-btn light_blue_btn_color" target="_blank"><?php echo $button_label_resource; ?></a>
					<?php } ?>

					<?php 	$button_label_resource = get_field('button_label_resource'); 
						$button_link_resource = get_field('button_link_resource');
						if(!empty($button_label_resource) && !empty($button_link_resource)){ ?>
							<a href="<?php echo $button_link_resource; ?>" class="resources-width-btn light_blue_btn_color" target="_blank"><?php echo $button_label_resource; ?></a>
					<?php } ?>
				</article>
			</div>
		</div>
	</div>
</section>
<?php */ ?>
<?php get_footer('simple');
