<?php /* Template Name: Blog */
// get_header();
get_template_part( 'parts/header-type'); 
$page_id = get_the_ID();

$no = get_field('number_of_posts_per_page');

if(empty($no)){ $no = 6; } ?>

<section class="generic-blog-wrapper">
	<div class="container">
		<div class="row"> 
			<div class="col-md-9 col-sm-12 col-xs-12 blog-content">
			   	<?php 
					$args =  array( 
		                'ignore_sticky_posts' 	=> true, 
		                'post_type'           	=> 'post',
		                'order'              	=> 'DESC',
		                'posts_per_page'		=> $no
					);   

		        	$args['paged'] = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
				 	$loop = new WP_Query( $args ); 
		 			$count = 0; 
		 			$count_divs = 0;
		 			$total_post = $loop->post_count; 

		 			if ($loop->have_posts()) {  ?>
		 				<?php  while ($loop->have_posts())	{  $loop->the_post(); $id = get_the_ID(); 	$count++;  $count_divs++; ?>
		 				<div class="col-md-4 col-sm-12 col-xs-12 single-res-2">				
							<div class="generic-post-box">
								<a href="<?php echo get_permalink(); ?>" class="desktop-hidden-link"></a>
								<div class="generic-featured-post-img" style="background-image: url('<?php $img = get_field('thumbnail_image',  $id);  
									if(!empty($img)){ echo $img['sizes']['single-small']; } else { echo get_template_directory_uri().'/images/no-post-small.jpg'; }?>');"><a href="<?php echo get_permalink($id); ?>" title="<?php echo get_the_title($id); ?>"></a></div>
								<div class="post-short-description">
									<h4><a href="<?php echo get_permalink(); ?>" title="<?php echo get_the_title(); ?>"><?php 
											$post_title = get_the_title(); 
											if(strlen($post_title) > 55) { echo substr($post_title, 0, 55).'...'; } else { echo $post_title; }
									?></a></h4>
									<h4 class="tablet-second-title"><a href="<?php echo get_permalink(); ?>" title=""><?php echo get_the_title(); ?></a></h4>
									<i><?php echo get_the_date('d F Y'); ?></i>
									<div class="excerpt-wrapp">
										<p><?php echo get_the_excerpt($id); ?> <a href="<?php echo get_permalink($id); ?>" class="rm-post-btn"><?php _e('read more', THEME_TEXT_DOMAIN); ?></a></p>
									</div>
								</div>
							</div>
						</div>
		 			<?php }	?>
				<?php }	?>
				<?php wp_reset_query(); ?>

				<div class="col-md-12 extra-large-pagination">
					<div class="text-center">
						<?php  wp_pagenavi(array( 'query' => $loop )); ?>
					</div>		
				</div>		
		
			</div>
			<div class="col-md-3 col-sm-12 col-xs-12 blog-sidebar">
				<?php 
						$banner_title_1 		= get_field('banner_title_1', $page_id);
						$add_button_banner_1 	= get_field('add_button_banner_1', $page_id); 
						$button_title_banner_1 	= get_field('button_title_banner_1', $page_id);
						$button_link_banner_1 	= get_field('button_link_banner_1', $page_id);
						$image_banner_1 		= get_field('image_banner_1', $page_id); ?>

					<div class="single-res-full">
						<div class="training-featured-post">
							<?php if(!empty($banner_title_1)){ ?>
								<h4><?php echo $banner_title_1; ?></h4>
							<?php } ?>
							<?php if(!empty($add_button_banner_1)){ 
								if(!empty($button_title_banner_1) && !empty($button_link_banner_1)){ ?>
									<a href="<?php echo $button_link_banner_1; ?>" class="training-blue-btn btn-learn-more-blog-pagebtn-learn-more-blog-page"><?php echo $button_title_banner_1; ?></a>
								<?php } ?>
							<?php } ?>
							<?php if(!empty($image_banner_1)){ ?>
								<img src="<?php echo $image_banner_1['sizes']['banner_blog']; ?>" alt="<?php echo $image_banner_1['alt']; ?>">
							<?php } ?>
						</div>
					</div>


				<?php	$banner_title_2 = get_field('banner_title_2', $page_id); 
						$banner_subtitle_2 = get_field('banner_subtitle_2', $page_id); 
						$add_button_banner_2 = get_field('add_button_banner_2', $page_id);
						$button_title_banner_2 = get_field('button_title_banner_2', $page_id);
						$button_link_banner_2 = get_field('button_link_banner_2',  $page_id);  ?>

						<div class="single-res-full">
							<div class="demo-request-box">
								<div class="demo-req-wrapper">
									<?php if(!empty($banner_title_2)){  ?>
										<h3><?php echo $banner_title_2; ?></h3>
									<?php } ?>
									<?php if(!empty($banner_subtitle_2)){ ?>
										<h4><?php echo $banner_subtitle_2; ?></h4>
									<?php } ?>

									<?php if(!empty($add_button_banner_2)){ 
											if(!empty($button_title_banner_2 && !empty($button_link_banner_2))){	?>
												<a href="<?php echo $button_link_banner_2; ?>" class="green-demo-btn btn-request-demo-blog-page"><?php echo $button_title_banner_2; ?></a>
											<?php } ?>
									<?php } ?>
								</div>
							</div>
						</div>

			</div>
		</div>
	</div>
</section>

<?php get_template_part( 'parts/newsletter'); ?> 
<?php get_template_part( 'parts/popup-newsletter'); ?> 
<?php get_footer();
