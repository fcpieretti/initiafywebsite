<?php /* Template Name: Page Pricing */

//get_header();

get_template_part( 'parts/header-type'); 
$page_id = get_the_ID();
$main_currency = get_current_currrency($page_id); // code, symbol 
$unit_of_measure = get_field('general_unit_of_measure', $page_id);

?>



<?php  	if( have_rows('flexible_content') ):
			while ( have_rows('flexible_content') ) : the_row();

		        if( get_row_layout() == 'intro' ):
		        	$intro_text_fx1 = get_sub_field('intro_text_fx1');

		        	if(!empty($intro_text_fx1)){ ?>
				        <section class="red_circle_background gray_bg">
							<div class="red_circle"></div>
							<div class="container">
								<div class="row">
									<div class="col-md-12 text_center">
										<article class="heading-text">
											<?php echo $intro_text_fx1; ?>
										</article>
									</div>
								</div>
							</div>
						</section>	
					<?php } ?>

		<?php 	elseif( get_row_layout() == 'packages' ): 

					$list_of_packages 	= get_sub_field('list_of_packages');
					$request_demo_button_name = get_sub_field('request_demo_button_name');
					$request_demo_page 	= get_sub_field('request_demo_page');

					$packages_id 		= array();
					$count 				= -1;
					if ($list_of_packages){ 
						foreach ($list_of_packages as $box1) { 	
							$package 	= $box1['package'];
							$featured 	= $box1['featured'];
							if(!empty($package)) {
								$count++;
								$packages_id[$count]['id'] = $package;
								$packages_id[$count]['featured'] = $featured;
							}			
						}
		 			} 
		 			//var_dump($packages_id); ?>

				<section class="pricing_range_slider">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="slider_range_wrapper">
									<input id="js-range-slider">
									<input type="hidden" id="users_val" value="250">
								</div>				 
								
								<div class="currency_dropdown">
									<div class="loader-currency"><i class="fa fa-spinner" aria-hidden="true"></i></div>
									<h6><?php  _e('Currency', THEME_TEXT_DOMAIN); ?></h6>
									<select name="" id="currency_selector">
										<?php 	$currency_options = get_sub_field('currency_options'); 
												foreach($currency_options as $item_currency){ 
													$code_currency 		= $item_currency['code_currency'];
													$symbol_currency 	= $item_currency['symbol_currency'];
													
													if(!empty($code_currency) && !empty($symbol_currency)){ 
													    if(strcmp($code_currency, 'EUR') != 0){ ?>
															<option value="<?php echo $code_currency; ?>"  data-symbol="<?php echo $symbol_currency; ?>" ><?php echo $symbol_currency.' '.$code_currency ; ?></option>
														<?php } else { 
														    
														    $main_currency['symbol'] = $symbol_currency;
														    $main_currency['code'] = $code_currency;
														    
														    ?>
														    <option value="<?php echo $code_currency; ?>"  data-symbol="<?php echo $symbol_currency; ?>" selected="selected" ><?php echo $symbol_currency.' '.$code_currency ; ?></option>
													<?php } } ?>
										<?php 	} ?>
									</select>
								</div>

								<div class="mobile-features-blocks">	
									<div class="m-features-thumbs" id="pager">
										<?php  $count = -1; 
										foreach($packages_id as $item){ 
											$count++;
											$id = $item['id']; ?>
											<a href="#" class="item" data-slide-index="<?php echo $count; ?>">
												<div data-id="<?php echo $id; ?>" class="featured-block change-currency">
													<div class="heading-title">
														<h4><?php echo get_the_title($id); ?></h4>
													</div>
													<div class="price-wrapper">
														<h3><span class="price-currency"><?php echo $main_currency['symbol'] ?></span> <span class="price-packet"><?php echo get_packages_price(250, $main_currency['code'], $id); ?></span></h3>
														<p><?php echo $unit_of_measure; ?></p>
													</div>
												</div>
											</a>
										<?php } ?>
									</div>
									
									<div class="scroll_down_wrapper-icon">
										<a href="#pricing_tab" class="scroll_down_arrow wide_black-icon scroll-down">
											<img src="<?php echo get_template_directory_uri();?>/images/scroll_down_arrow_black.png" alt="Scroll Down">
										</a>
									</div>
									
									<div id="scroll-down">
										<div class="m-pricing-features bxslider" id="bxslider-vertical">
											<div class="bxslider">
												<?php  	$count = -1; 
														foreach($packages_id as $item){ 
															$count++;
															$id = $item['id']; 
															$featured = $item['featured']; ?>

															<div data-item="<?php echo $count ?>" class="item">
																<div class="pricing-box change-currency" data-id="<?php echo $id; ?>" >
																	<div class="pricing-box-next">
																		<div class="pricing-heading">
																			<h4><?php echo get_the_title($id); ?></h4>
																		</div>
																	 
																		<div class="price-tag">
																			<h3><span class="price-currency"><?php echo $main_currency['symbol'] ?></span> <span class="price-packet"><?php echo get_packages_price(250, $main_currency['code'], $id); ?></span></h3>
																			<p><?php echo $unit_of_measure; ?></p>
																		</div>
																		 

																		<?php if(!empty($request_demo_button_name) && !empty($request_demo_page)){ ?>
																			<a href="<?php echo $request_demo_page; ?>" class="demo-req btn-speak-to-us-<?php echo strtolower(get_the_title($id)) ?>"><?php echo $request_demo_button_name; ?></a>
																		<?php } ?>

																		<?php 	$recomandation = get_field('recomandation', $id);	
																				$features = get_field('features', $id); 
																				if(!empty($recomandation) || !empty($features)){ ?>
																					<ul class="pricing-list pricing-list-hidden">
																						<?php if(!empty($recomandation)){ ?>
																							<li><strong><i><?php echo $recomandation; ?></i></strong></li>
																						<?php } ?>

																						<?php 
																						
																							foreach($features as $feature){
																								$feature_name = $feature['feature'];
																								if(!empty($feature_name)){  ?>
																									<li><?php echo $feature_name; ?></li>
																								<?php } ?>
																						<?php } ?>
																						
																						<?php $add_more_features = get_field('add_more_features', $id);
																						if(!empty($add_more_features)){
																							$pdf_all_features = get_field('pdf_all_features', $id); 
																							if(!empty($pdf_all_features)){ ?>
																								<li><a href="<?php echo $pdf_all_features['url']; ?>" target="_blank" class="see-features2"><?php _e('See all features', THEME_TEXT_DOMAIN); ?></a></li>
																							<?php } ?>
																						<?php } ?>
																					</ul>
																		<?php 	} ?>

																	</div>
																</div>
															</div>
												<?php 	} ?>

												
												<?php  	$count = -1; 
													foreach($packages_id as $item){ 
														$count++;
														$id = $item['id'];
														$featured = $item['featured']; ?>
														
														<div class="item">
															<div class="pricing-box change-currency" data-id="<?php echo $id; ?>">
																<div class="pricing-box-next">
																	<div class="pricing-heading">
																		<h4><?php echo get_the_title($id); ?></h4>
																	</div>

																	<?php 	$price = get_field('price', $id); 
																			if(!empty($price)){ ?>
																				<div class="price-tag">
																					<h3><span class="price-currency"><?php echo $main_currency['symbol'] ?></span> <span class="price-packet"><?php echo get_packages_price(250, $main_currency['code'], $id); ?></span></h3>
																					<p><?php echo $unit_of_measure; ?></p>
																				</div>
																	<?php 	} ?>

																	<?php 	if(!empty($request_demo_button_name) && !empty($request_demo_page)){ ?>
																				<a href="<?php echo $request_demo_page; ?>" class="demo-req btn-speak-to-us-<?php echo strtolower(get_the_title($id)) ?>"><?php echo $request_demo_button_name; ?></a>
																	<?php 	} ?>

																	<?php 	$recomandation = get_field('recomandation', $id);	
																			$features = get_field('features', $id); 
																			if(!empty($recomandation) || !empty($features)){ ?>
																				<ul class="pricing-list pricing-list-hidden">
																					<?php if(!empty($recomandation)){ ?>
																						<li><strong><i><?php echo $recomandation; ?></i></strong></li>
																					<?php } ?>

																					<?php 	foreach($features as $feature){
																							$feature_name = $feature['feature'];
																							if(!empty($feature_name)){  ?>
																								<li ><?php echo $feature_name; ?></li>
																							<?php } ?>
																					<?php } ?>
																					
																					<?php 	$add_more_features = get_field('add_more_features', $id);  
																							if(!empty($add_more_features)){
																								$pdf_all_features = get_field('pdf_all_features', $id); 
																								if(!empty($pdf_all_features)){ ?>
																									<li><a href="<?php echo $pdf_all_features['url']; ?>" target="_blank" class="see-features2"><?php _e('See all features', THEME_TEXT_DOMAIN); ?></a></li>
																								<?php } ?>
																						<?php } ?>
																				</ul>
																	<?php } ?>					
																</div>
															</div>
														</div>
												<?php } ?>
											</div>
										</div>
									</div>
								</div>

								<div class="pricing_tables_wrapper">
									<div class="row">
										<?php  	$count = -1; 
												foreach($packages_id as $item){ 
													$count++;
													$id = $item['id'];
													$featured = $item['featured']; ?>

													<div class="col-md-4">
														<div data-id="<?php echo $id; ?>" class="pricing-box change-currency <?php if($featured == true){ echo 'featured-box'; } ?>" >
															<div class="pricing-heading">
																<h4><?php echo get_the_title($id); ?></h4>
															</div>

															<div class="price-tag">
																<h3><span class="price-currency"><?php echo $main_currency['symbol'] ?></span> <span class="price-packet"><?php echo get_packages_price(250, $main_currency['code'], $id); ?></span></h3>
																<p><?php echo $unit_of_measure; ?></p>
															</div>
															
															<?php 	if(!empty($request_demo_button_name) && !empty($request_demo_page)){ ?>
																		<a href="<?php echo $request_demo_page; ?>" class="demo-req btn-speak-to-us-<?php echo strtolower(get_the_title($id)) ?>"><?php echo $request_demo_button_name; ?></a>
															<?php } ?>

															<?php 	$recomandation = get_field('recomandation', $id);	
																	$features = get_field('features', $id); 
																	if(!empty($recomandation) || !empty($features)){ ?>
																		<ul class="pricing-list pricing-list-hidden">
																			<?php if(!empty($recomandation)){ ?>
																				<li><strong><i><?php echo $recomandation; ?></i></strong></li>
																			<?php } ?>

																			<?php 	
																				foreach($features as $feature){
																					$feature_name = $feature['feature'];
																					if(!empty($feature_name)){   ?>
																						<li ><?php echo $feature_name; ?></li>
																					<?php } ?>
																			<?php } ?>

																			<?php 	$add_more_features = get_field('add_more_features', $id);   	
																					if(!empty($add_more_features)){
																						$pdf_all_features = get_field('pdf_all_features', $id); 
																						if(!empty($pdf_all_features)){ ?>
																							<li><a href="<?php echo $pdf_all_features['url']; ?>" target="_blank" class="see-features2"><?php _e('See all features', THEME_TEXT_DOMAIN); ?></a></li>
																					<?php } ?>
																			<?php } ?>
																		</ul>
															<?php 	} ?>							
														</div>
													</div>

											<?php } ?>
									</div>
								</div>

								<?php 	$cta_text 			= get_sub_field('cta_text');
										$add_button_cta 	= get_sub_field('add_button_cta');
										$button_cta_label 	= get_sub_field('button_cta_label');
										$button_cta_link 	= get_sub_field('button_cta_link');
										$external_link 		= get_sub_field('external_link'); 

										if(!empty($cta_text) || !empty($add_button_cta)){ ?>
											<div class="cta-wrapper cta-wrapper-box">
												<?php if(!empty($cta_text)){ ?>
													<h3><?php echo $cta_text; ?></h3>
												<?php } ?>

												<?php if(!empty($add_button_cta)){
														if(!empty($button_cta_label) && !empty($button_cta_link)){ ?>
															<a href="<?php echo $button_cta_link; ?>" <?php if(!empty($external_link)){ echo 'target="_blank"';} ?> class="btn rounded_btn red_btn btn-speak-to-us-red"><?php echo $button_cta_label; ?></a>
														<?php } ?>
												<?php } ?>
											</div>
								<?php 	} ?>
							</div>			
						</div>
					</div>
				</section>	

		
		<?php 	elseif( get_row_layout() == 'testimonial' ):

					$image_testimonial 		= get_sub_field('image_testimonial');
					$content_testimonial 	= get_sub_field('content_testimonial');
					$author_testimonial 	= get_sub_field('author_testimonial');
					$logo_testimonial 		= get_sub_field('logo_testimonial');
					$type_of_image2 		= get_sub_field('type_of_image_testimonial').'_testimonial';
					$type_of_image 			= get_sub_field('type_of_image_testimonial');

					if(!empty($content_testimonial)){  ?>	
						<section class="testimonial_block testimonial_type_black">
							<div class="container container-small-3">
								<div class="row">
									<?php if(!empty($image_testimonial)){ ?>
										<div class="col-md-<?php if($type_of_image == 'large'){ echo '6'; } else if($type_of_image == 'medium'){ echo '4'; } else { echo '3'; }  ?> testimonial-av">
											<div class="testimonial-avatar">
												<img src="<?php echo $image_testimonial['sizes'][$type_of_image2]; ?>" alt="<?php echo $image_testimonial['alt']; ?>">
											</div>
										</div>
									<?php } ?>
									<div class="col-md-<?php if($type_of_image == 'large'){ echo '6'; } else if($type_of_image == 'medium'){ echo '8'; } else { echo '9'; }  ?> testimonial-text">
										<article>
											<?php echo $content_testimonial; ?>
											<?php if(!empty($author_testimonial)){ ?>
												<span><?php echo $author_testimonial; ?></span>
											<?php } ?>

											<?php if(!empty($logo_testimonial)){ ?>
												<div class="company-avatar">
													<img src="<?php echo $logo_testimonial['sizes']['company_single']; ?>" alt="<?php echo $logo_testimonial['alt']; ?>">
												</div>
											<?php } ?>
										</article>
									</div>
								</div>
							</div>
						</section>
					<?php } ?>

		<?php   endif;
		    endwhile;
		endif; ?>


<?php 	$activate_notification_popup 	= get_field('activate_notification_popup_pricing' ,'options');
		$title_apply_popup				= get_field('content_notification_popup_pricing' ,'options');
		$icon_apply_popup 				= get_field('image_notification_popup_pricing' ,'options');
		$show_when_form_popup			= get_field('show_when_form_popup_pricing' ,'options'); 	
		$seconds_form_popup 			= get_field('seconds_form_popup_pricing' ,'options');

	if(!empty($activate_notification_popup) && !empty($title_apply_popup)){  ?>
	
	<div id="pricing_speak" data-izimodal-title=" " class="general_popup pricing_speak_popup" style="display: none;" >
		<?php if(!empty($icon_apply_popup)){ ?>
		    <div class="header-icon">
				<img src="<?php echo $icon_apply_popup['sizes']['icon_newsletter']; ?>" alt="<?php echo $icon_apply_popup['alt']; ?>">
			</div>
		<?php } ?>
		
		<?php if(!empty($title_apply_popup)){  ?>	
			<div class="newsletter-title">
				<?php echo $title_apply_popup; ?>
			</div>
		<?php } ?>

		<?php 	$add_button 	= get_field('add_button_form_popup_pricing', 'options');
				$button_label 	= get_field('button_label_form_popup_pricing', 'options');
				$button_link 	= get_field('button_link_form_popup_pricing', 'options');
				$external_link  = get_field('external_link_form_popup_pricing', 'options');

				if(!empty($add_button)){ 
					if(!empty($button_label) && !empty($button_link)){ ?>
						<div class="wrapp-button-poup">
							<a href="<?php echo $button_link; ?>" title="" class="btn rounded_btn red_btn" <?php if(!empty($external_link)){ echo 'target="_blank"'; } ?>><?php echo $button_label; ?></a>
						</div>

		<?php 		}
				} ?>

	</div>

	<?php switch ($show_when_form_popup) {
				case 'after-x-seconds': 
						$seconds_form_popup	= get_field('seconds_form_popup', 'options'); ?>
						<script type="text/javascript">
							jQuery(window).load(function($) {
								if( jQuery('.pricing_speak_popup').length >0 ) {
									setTimeout(function(){
									  	var count = 0; 
									  	// test if an other izimodal is not opened 
										jQuery( ".general_popup_item" ).each(function() {
									 		if(jQuery(this).css('display') == 'block'){	
									 			count++;
									 		}
										});

										if(count == 0) {
											var cookie_val = jQuery.cookie('pricing');  
											if(cookie_val != 'false'){
												var cookie_val = jQuery.cookie('pricing');
										        jQuery.cookie('pricing', 'true', {
										            //expires: 1,
										            path: '/'
										        });

										        // show popup
										        jQuery('.pricing_speak_popup').iziModal({
											  		zindex: 999999,
											  		width: 720,
											  		bodyOverflow: true,
											  		autoOpen: 1
											  	});

										        // Closing
												jQuery(document).on('closing', '.pricing_speak_popup', function (e) {
													jQuery.cookie('pricing', 'false', {  path: '/' });
												});
											}  
										}

									}, <?php echo intval($seconds_form_popup) * 1000; ?> );
								}
							});
						</script>
		<?php 	break;
				case 'when-user-leave-page': ?>
					<script type="text/javascript">
						jQuery(window).load(function($) {
							if( jQuery('.pricing_speak_popup').length >0 ) {
								var cookie_val = jQuery.cookie('pricing');  
								if(cookie_val != 'false'){
									var cookie_val = jQuery.cookie('pricing');
							        jQuery.cookie('pricing', 'true', {
							            //expires: 1,
							            path: '/'
							        });
							    
							        // show popup
							        jQuery(document).mouseleave(function () {
							       		var count = 0; 
									  	// test if an other izimodal is not opened 
										jQuery( ".general_popup_item" ).each(function() {
									 		//if(jQuery(this).hasClass('isAttached')){
									 		if(jQuery(this).css('display') == 'block'){	
									 			count++;
									 		}
										});

										if(count == 0) {	
								       		if(!jQuery('.pricing_speak_popup').hasClass('iziModal')) {	
								       			jQuery('.pricing_speak_popup').iziModal({
								       				zindex: 999999,
											  		width: 720,
											  		bodyOverflow: true,
											  		autoOpen: 1,
								       			});
								       		}
							       		}
									});

							        // Closing
									jQuery(document).on('closing', '.pricing_speak_popup', function (e) {
										jQuery.cookie('pricing', 'false', {  path: '/' });
									});
								}  
							}
						}); 
					</script>

		<?php 	break;
				case 'on-half-scroll': ?>

					<script type="text/javascript">
						jQuery(window).load(function($) {
							jQuery(window).scroll(function () { 
								if (jQuery(window).scrollTop() > jQuery('body').height() / 2) {
									var count = 0; 
									jQuery( ".general_popup_item" ).each(function() {
								 		//if(jQuery(this).hasClass('isAttached')){
								 		if(jQuery(this).css('display') == 'block'){
								 			count++;
								 		}
									});

									if(count == 0) {	
										var cookie_val = jQuery.cookie('pricing');  
										if(cookie_val != 'false'){
											var cookie_val = jQuery.cookie('pricing');
									        jQuery.cookie('pricing', 'true', {
									            //expires: 1,
									            path: '/'
									        });

											if(!jQuery('.pricing_speak_popup').hasClass('iziModal')) {	
								       			jQuery('.pricing_speak_popup').iziModal({
								       				zindex: 999999,
											  		width: 720,
											  		bodyOverflow: true,
											  		autoOpen: 1,
								       			});
								       		}
									        // Closing
											jQuery(document).on('closing', '.pricing_speak_popup', function (e) {
												jQuery.cookie('pricing', 'false', {  path: '/' });
											});
								       	}	
									}
								} 
							});
						}); 	
					</script>
		<?php 	break;
				default: break;
			} ?>
<?php 	} ?>

<input type="hidden" value="<?php echo get_the_ID(); ?>" id="page_id"> 


<?php 

	if (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_FORWARDED_FOR']) {
		$user_ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	} else {
		$user_ip = $_SERVER['REMOTE_ADDR'];
	}

	//$user_ip = '66.194.210.175'; //US
	//$user_ip = '89.101.244.87'; //Ireland
	//$user_ip ='185.86.151.11'; // England
    $ip_array = explode(',', $user_ip);
	$user_ip = $ip_array[0];
?>

<script type="text/javascript">

	jQuery(window).ready(function($) 
	{
		jQuery.ajax(
			       "https://extreme-ip-lookup.com/json/<?php echo $user_ip ?>",
			 	{
					success: function(data) {
						if (data && data.countryCode) {
							console.log(data.countryCode);
							switch(data.countryCode) {
								case 'US':
									updateprices('USD');
									break;
								case 'CA':
									updateprices('CAD');
									break;
								case 'GB':
									updateprices('GBP');
									break;	
								default:
									updateprices('EUR');
									break;	
							}
						}
						else {
							updateprices('EUR');
						}
					},
					error: function(error) {
						console.log(error);
						updateprices('EUR');
					}
				});
	});

	function updateprices(value){
		$('#currency_selector').SumoSelect();	
		$('#currency_selector')[0].sumo.selectItem(value);
		var value_selected 	= jQuery('#currency_selector').find(":selected").val();
	   	var value_symbol	= jQuery('#currency_selector').find(":selected").attr('data-symbol');
	   	var page_id 		= jQuery('#page_id').val();
	   	var users_val 		= jQuery('#users_val').val();

	   	if( typeof value_selected !== 'undefined' || value_selected != null ){
	   		update_prices(page_id, value_selected, value_symbol, users_val); 	
	   	}
	}

</script>

<?php get_footer();
