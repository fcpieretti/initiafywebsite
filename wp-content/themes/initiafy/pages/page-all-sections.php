<?php /* Template Name: General Page */
 ?>

<?php // header 
	$type_of_header = get_field('type_of_header');
	switch ($type_of_header) {
		case 'none': 
			get_header(); ?>
			<div class="empty_header"></div>

	<?php	break;
		case 'text-and-image': 
			$intro_image = get_field('intro_image_text_and_image');  
			$intro_title = get_field('intro_title_text_and_image');
			get_header('single');  ?>
			 
			<section class="hero blog_hero about_hero hero_header">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="img_background" style="background-image: url('<?php  if(!empty($intro_image)){ echo $intro_image['sizes']['hero-single']; } ?>');" alt="<?php echo get_the_title(); ?>">
								<div class="dark_blue_overlay"></div>
								<div class="hero_content">
									<div class="container container-small">
										<div class="col-md-12 text_center">
											<div class="blog-post-title-center">
												<?php if(!empty($intro_title)){ ?>
													<div class="general_hero_text"><?php echo $intro_title; ?></div>
												<?php } ?>
											</div>								
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
	<?php	break;
		case 'red-section': 
			$title_fx1 			= get_field('intro_title_red');
        	$content_fx1 		= get_field('intro_subtitle_red');
        	$image_video_fx1	= get_field('image_video_red');
        	$add_video_red 		= get_field('add_video_red');
        	$video_id_fx1 		= get_field('video_player_id_red'); 
        	get_header(); ?>
			
			<section class="red_circle_background">
				<div class="red_circle"></div>
				<div class="container">
					<div class="row">
						<div class="col-md-12 text_center">
							<?php if(!empty($title_fx1)){  ?>
								<h1><?php echo $title_fx1; ?></h1>
							<?php } ?>
							<?php if(!empty($content_fx1)){ ?>
								<p><?php echo $content_fx1; ?></p>
							<?php } ?>
							
							<?php 	if(!empty($add_video_red)){
										if(!empty($video_id_fx1)){ ?>
											<div class="video_preview_img">
												<img src="<?php if(!empty($image_video_fx1)){ echo $image_video_fx1['sizes']['video_intro']; } else { echo get_template_directory_uri().'/images/no_video.jpg'; } ?>" alt="<?php echo $image_video_fx1['alt']; ?>">

												<a href="#generic_video_popup" class="play_video_btn" data-iziModal-open="#generic_video_popup">
													<img src="<?php echo get_template_directory_uri();?>/images/white_play.png">
												</a>
												
												<div id="generic_video_popup" class="video_popup general_popup_item" data-izimodal-title="" data-izimodal-iframeURL="<?php echo $video_id_fx1; ?>">
												</div>


											</div>
										<?php } ?>
										<div>
											<a href="#" class="scroll_down_arrow">
												<img src="<?php echo get_template_directory_uri();?>/images/scroll_down_arrow.png" alt="Scroll Down">
											</a>
										</div>
										
							<?php 	} ?>
							
						</div>
					</div>
				</div>
			</section>
   <?php	break;
		case 'red-section-with-form': 

			$red_section_with_form_main_nav =  get_field('main_navigation');
			$tile_intro_red_with_form = get_field('intro_title_red');
			$red_section_with_form_pardot_form = get_field('red_section_pardot_form');
			$red_section_with_form_top_post = get_field('top_post_field');
			
			if ($red_section_with_form_main_nav == 'logo-only'){
				get_header('resources-white-landing-resources'); 
			}else{
				get_header(); 
			}	
			
			?>

			<section class="red_circle_background landing_page_hero white_bg">
				<div class="red_circle"></div>
			</section>

			<section class="generic-landing-page-wrapper generic-up-top generic-second-landing customize-landing">
				<div class="container container-small-2"> 
					<div class="row">
						<div class="col-md-7 wrapp-landing-text">
							<div class="large-title-box">
								<?php if(!empty($tile_intro_red_with_form)){  ?>
									<h1><?php echo $tile_intro_red_with_form; ?></h1>
								<?php } ?>
							</div>
							
							<article class="generic-landing-page-txt general_content">
							       <?php if(!empty($red_section_with_form_top_post)){ ?>
										<?php echo $red_section_with_form_top_post; ?>
									<?php } ?>
							</article>
						</div>

						<?php 	$title_form = get_field('title_form'); 

								if(!empty($red_section_with_form_pardot_form)){  ?>
									<div class="col-md-5 wrapp-landing-img">
										<div class="register-form-wrapper mobile_hide_register_form">
											<?php if(!empty($title_form)){ ?>
												<h2><?php echo $title_form; ?></h2>
											<?php } ?>
											
											<div class="register-form-wrapper-sec">
												<?php echo $red_section_with_form_pardot_form; ?> 
											</div>
										</div>
									</div>
						<?php 	} ?>
					</div>
				</div>
			</section>
			<?php	break;
		case 'blue-section-with-form': 

			$red_section_with_form_main_nav =  get_field('main_navigation');
			$tile_intro_red_with_form = get_field('intro_title_red');
			$red_section_with_form_pardot_form = get_field('red_section_pardot_form');
			$red_section_with_form_top_post = get_field('top_post_field');
			
			if ($red_section_with_form_main_nav == 'logo-only'){
				get_header('resources-dark-landing-resources');
			}else{
				get_header(); 
			}
			
			?>

			<section class="resources-heading">
				<div class="container">
					<div class="row">
						<div class="col-md-12"> 
							<article class="resources-langing-title">
							   <?php if(!empty($tile_intro_red_with_form)){  ?>
									<h1><?php echo $tile_intro_red_with_form; ?></h1>
								<?php } ?>
							</article>
						</div>
					</div>
				</div>
			</section>

			<section class="resources-description customize-landing">
				<div class="container container-small-2">
					<div class="row">
						
						<div class="col-md-8 wrapp-landing-text">
							<article class="generic-landing-page-txt general_content">
							       <?php if(!empty($red_section_with_form_top_post)){ ?>
										<?php echo $red_section_with_form_top_post; ?>
									<?php } ?>
							</article>

							<?php 	$button_label_resource 	= get_field('blue-section-with-form-button');
									$button_link_resource 	= get_field('blue_section_button_link'); 
									if(!empty($button_label_resource) && !empty($button_link_resource)){ ?>
										<a href="<?php echo $button_link_resource; ?>" class="resources-width-btn light_blue_btn_color" ><?php echo $button_label_resource; ?></a>
							<?php  	} ?>

						</div>
						
							<?php 	$title_form = get_field('title_form'); 

								if(!empty($red_section_with_form_pardot_form)){  ?>
									<div class="col-md-4 wrapp-landing-img">
										<div class="register-form-wrapper mobile_hide_register_form">
											<?php if(!empty($title_form)){ ?>
												<h2><?php echo $title_form; ?></h2>
											<?php } ?>
											
											<div class="register-form-wrapper-sec">
												<?php echo $red_section_with_form_pardot_form; ?> 
											</div>
										</div>
									</div>
								<?php 	} ?>
					</div>
				</div>
			</section>
	<?php	break;
		case 'text-and-image-with-form': 

		$red_section_with_form_pardot_form = get_field('red_section_pardot_form');
		$intro_image = get_field('intro_image_text_and_image');  
		$intro_title = get_field('intro_title_text_and_image');
		$header_size = get_field('header_size');
		get_header('single');  
		//get_template_part( 'parts/header-type'); 
		?>
			<section class="custom-hero hero blog_hero about_hero hero_header customize-landing" style="height:<?php echo $header_size; ?>px;">
				<div class="row">
					<div class="col-md-12">
					    <div class="row"> 
							<div class="img_background" style="background-image: url('<?php  if(!empty($intro_image)){ echo $intro_image['sizes']['hero-single']; } ?>');" alt="<?php echo get_the_title(); ?>">
								<div class="dark_blue_overlay"></div>
							</div>
							<div class="hero_content">
									<div class="container container-small-2" >
										<div class="col-md-6 text_center">
											<div class="blog-post-title-center">
												<?php if(!empty($intro_title)){ ?>
													<div class="general_hero_text"><?php echo $intro_title; ?></div>
												<?php } ?>
											</div>								
										</div>
										<?php
										$title_form = get_field('title_form');
										if(!empty($red_section_with_form_pardot_form)){  ?>
											<div class="col-md-6 wrapp-landing-img" >
												<div class="register-form-wrapper mobile_hide_register_form">
													<?php if(!empty($title_form)){ ?>
														<h2><?php echo $title_form; ?></h2>
													<?php } ?>
													
													<div class="register-form-wrapper-sec">
														<?php echo $red_section_with_form_pardot_form; ?> 
													</div>
												</div>
											</div>
										<?php 	} ?>
									</div>
							</div>
						</div>
					</div>
				</div>
 	    </section>


	<?php	break;
		case 'video':  
		
			$hero_intro_tex = get_field('hero_intro_text_fx1_header_video');
			$type_of_hero 	= get_field('type_of_hero'); 

			get_header(); ?>

			<section class="hero hero_header">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<?php $static_image = get_field('static_image_background_header_video'); ?>
							<div class="img_background" style="background-image: url('<?php echo $static_image['sizes']['hero']; ?>');" >
								<div class="black_overlay"></div>
								<div class="intro-section">
									<div class="video-background">
					  					<div class="video-foreground">
					  						
											<?php 	$type = get_field('type_copy_header_video');
													switch ($type) {
														case 'youtube':

															$video_id = get_field('youtube_id_for_video_header_video'); ?>

															<iframe id="video" src="//www.youtube.com/embed/<?php echo $video_id; ?>?enablejsapi=1&html5=1&controls=0&showinfo=0&autohide=1&autoplay=1&rel=0&loop=1&playlist=<?php echo $video_id; ?>" frameborder="0" allowfullscreen></iframe>
															
												<?php	break; ?>
												<?php	case 'vimeo':

															$vide_id = get_field('vimeo_id_for_video_header_video'); ?>

															<div class="iframe-wrapper">
												      			<iframe src="//player.vimeo.com/video/<?php echo $vide_id; ?>?api=1&controls=0&showinfo=0&rel=0&autoplay=1&loop=1&playlist=<?php echo $vide_id; ?>&autohide=1" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen id="video"></iframe>
												      		</div>
															
												<?php	break;
													
													}  ?>
										</div>
									</div>
								</div>
							

								<div class="hero_content with_video">
									<div class="container container-small">
										<div class="col-md-12 text_center">
											<?php if(!empty($hero_intro_tex)){ ?>
												<div class="individual-text-hero">
													<h1><?php echo $hero_intro_tex; ?></h1>
												</div>
											<?php } ?>

											<?php 	$add_buttons_fx1 = get_field('add_buttons_fx1_header_video');
													if(!empty($add_buttons_fx1)) {
														$buttons_fx1 = get_field('buttons_fx1_header_video');
														if(!empty($buttons_fx1)){ ?>
															<div class="btn_group">
																<?php foreach($buttons_fx1 as $button){ 
																		$button_label 	= $button['button_label'];
																		$button_color 	= $button['button_color'];
																		$button_type 	= $button['button_type'];

																		switch ($button_type) {
																		 	case 'video':
																		 		$button_video 	= $button['button_video'];
																		 		if(!empty($button_video)){ ?>
																		 			<?php /*
																			 		<a href="https://www.youtube.com/embed/<?php echo $button_video; ?>?" class="fancybox btn rounded_btn <?php echo $button_color; ?>_btn"><?php echo $button_label; ?></a>	
																			 		<?php */ ?> 

																			 		<a href="#generic_video_popup" class="btn rounded_btn red_btn" data-iziModal-open="#generic_video_popup"><?php echo $button_label; ?></a>
																					
																					<div id="generic_video_popup" class="video_popup_full general_popup_item" data-izimodal-title="" data-izimodal-iframeURL="https://www.youtube.com/embed/<?php echo $button_video; ?>" style="display: none;"></div>

																			 	<?php } ?>

																<?php  			break;
																		 	case 'link':
																		 		$button_link 	= $button['button_link'];
																		 		if(!empty($button_link)){   ?>
																		 			<a href="<?php echo $button_link; ?>" title="" class="btn rounded_btn <?php echo $button_color; ?>_btn"><?php echo $button_label; ?></a>
																		 		<?php } ?>

																<?php  			break;
																		 	default: break;
																		} ?>
																<?php } ?>
															</div>
													<?php } ?>
											<?php } ?>
										</div>
									</div>
								</div>
								<div class="scroll_down_arrow_parent">
									<div class="container">
										<div class="row">
											<div class="col-md-12 text_center">
												<a href="#first-section" class="scroll_down_arrow">
													<img src="<?php echo get_template_directory_uri();?>/images/scroll_down_arrow.png" alt="Scroll Down">
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

<?php	break;	
		default: get_header(); break;
	}	

?>

	<?php 
	if(!empty($red_section_with_form_pardot_form)){ ?>
		<div class="mobile_contact_form demo_request_form landing_page_contact_form">
			<div class="register-form-wrapper mobile-submit-form">
				<div class="register-form-wrapper-sec">
					<?php echo $red_section_with_form_pardot_form; ?>
				</div>
			</div>
		</div>
	<?php } ?>

<?php  // content
$count = 0; 
if( have_rows('flexible_content') ): 
    while ( have_rows('flexible_content') ) : the_row(); $count++;
        if( get_row_layout() == 'simple_text' ){
        	$content_simple_text = get_sub_field('content_simple_text');
        	if(!empty($content_simple_text)){ ?>
	        	<section  <?php if($count==2){ echo 'id="first-section"'; } ?> class="generic-landing-page-wrapper same-margin-section <?php if($type_of_header == "none"){ echo 'no-margin-general'; } ?>" >
					<div class="container"> 
						<div class="row">
							<div class="col-md-12 standard-page">
								<article class="main-article generic-landing-page-txt general_content">
									<?php echo $content_simple_text; ?>
								</article>
							</div>
						</div>
					</div>
				</section>
		<?php }  
		} elseif( get_row_layout() == 'section_final_image_left_text_right' ){ 
				$color_final1	 	= get_sub_field('color_final1');
				$title_final1 		= get_sub_field('title_final1');
				$content_final1 	= get_sub_field('content_final1');
				$image_final1 		= get_sub_field('image_final1');
				$add_button_final1  = get_sub_field('add_button_final1');
				$button_label_final1 = get_sub_field('button_label_final1');
				$button_link_final1 = get_sub_field('button_link_final1');
				$remove_shape_top_final1	= get_sub_field('remove_shape_top_final1');
				$remove_shape_bottom_final1 = get_sub_field('remove_shape_bottom_final1'); ?>

				<section <?php if($count==2){ echo 'id="first-section"'; } ?> class="section-timg-final type_1">
					<?php if(empty($remove_shape_top_final1)){  ?>
						<div class="product_triangle_1_parent_parent <?php echo $color_final1; ?>_color">
							<div class="product_triangle_3_parent">
								<div class="product_triangle_3"></div>
							</div>
						</div>
					<?php } ?>

					<div class="wrapper-container <?php echo $color_final1; ?>_color_bg <?php if(empty($remove_shape_top_final1)){ echo 'no-margin-top'; } if(empty($remove_shape_bottom_final1)){ echo ' no-margin-bottom'; }   ?>">
						<div class="container container-small-3 text_center">
							<div class="row">
								<div class="col-md-6 vcenter">
									<?php if(!empty($title_final1)){ ?>
										<h3 class="style_h2"><?php echo $title_final1; ?></h3>
									<?php } ?>

									<?php if(!empty($content_final1)){ ?>
									<div class="general_content">
										<?php echo $content_final1; ?>
									</div>
									<?php } ?>

									<?php if(!empty($add_button_final1)){ 
										if(!empty($button_label_final1) && !empty($button_link_final1))?>
										<div class="button-timg">
											<a href="<?php echo $button_link_final1; ?>" class="btn rounded_btn green_btn "><?php echo $button_label_final1; ?></a>
										</div>
									<?php } ?>
								</div>
								<?php if(!empty($image_final1)){  ?>
									<div class="col-md-6 vcenter img-small-round text_centred">
										<img src="<?php echo $image_final1['sizes']['image_list']; ?>" alt="<?php echo $image_final1['alt']; ?>">
									</div>
								<?php } ?>
								<div class="clear"></div>
							</div>
						</div>
					</div>

					<?php if(empty($remove_shape_bottom_final1)){  ?>
						<div class="first_black_triangle_parent <?php echo $color_final1; ?>_color">
							<div class="first_black_triangle_bg to_bottom">
								<div class="first_white_triangle_bg"></div>
							</div>	
						</div>
					<?php } ?>
				</section>


<?php 	} elseif( get_row_layout() == 'section_final_image_right_text_left' ){
				$color_final2 		= get_sub_field('color_final2');
				$title_final2 		= get_sub_field('title_final2');
				$content_final2		= get_sub_field('content_final2'); 
				$image_final2		= get_sub_field('image_final2');
				$add_button_final2 	= get_sub_field('add_button_final2');
				$button_label_final2= get_sub_field('button_label_final2');
				$button_link_final2 = get_sub_field('button_link_final2');
				$remove_shape_top_final2	= get_sub_field('remove_shape_top_final2');
				$remove_shape_bottom_final2 = get_sub_field('remove_shape_bottom_final2'); ?>


				<section <?php if($count==2){ echo 'id="first-section"'; } ?> class="section-timg-final type_2">
					
					<?php if(empty($remove_shape_top_final2)){  ?>
						<div class="product_triangle_1_parent_parent <?php echo $color_final2; ?>_color">
							<div class="product_triangle_3_parent">
								<div class="product_triangle_3"></div>
							</div>
						</div>
					<?php } ?>

					<div class="wrapper-container <?php echo $color_final2; ?>_color_bg  <?php if(empty($remove_shape_top_final2)){ echo 'no-margin-top'; } if(empty($remove_shape_bottom_final2)){ echo ' no-margin-bottom'; }   ?>">
						<div class="container container-small-3 text_center">
							<div class="row">

								<div class="col-md-6 col-md-push-6 vcenter">
									<?php if(!empty($title_final2)){ ?>
										<h3 class="style_h2"><?php echo $title_final2; ?></h3>
									<?php } ?>

									<?php if(!empty($content_final2)){ ?>
									<div class="general_content">
										<?php echo $content_final2; ?>
									</div>
									<?php } ?>

									<?php if(!empty($add_button_final2)){ 
										if(!empty($button_label_final2) && !empty($button_link_final2))?>
										<div class="button-timg">
											<a href="<?php echo $button_link_final1; ?>" class="btn rounded_btn green_btn "><?php echo $button_label_final2; ?></a>
										</div>
									<?php } ?>
								</div>

								<?php if(!empty($image_final2)){  ?>
									<div class="col-md-6 col-md-pull-6 vcenter img-small-round text_centred">
										<img src="<?php echo $image_final2['sizes']['image_list']; ?>" alt="<?php echo $image_final2['alt']; ?>">
									</div>
								<?php } ?>

								
								<div class="clear"></div>
							</div>
						</div>
					</div>

					<?php if(empty($remove_shape_bottom_final2)){  ?>
						<div class="first_black_triangle_parent <?php echo $color_final2; ?>_color">
							<div class="first_black_triangle_bg to_bottom">
								<div class="first_white_triangle_bg"></div>
							</div>	
						</div>
					<?php } ?>
				</section>
<?php 	}  elseif( get_row_layout() == 'steps' ){ 	
				$title_steps_2 		= get_sub_field('title_steps_2');
    			$icons_steps_2 		= get_sub_field('icons_steps_2'); ?>

	        	<section  <?php if($count==2){ echo 'id="first-section"'; } ?> class="normal_padding same-margin-section circle_section_general"> <?php // same-margin-section circle_next_list_section ?>
					<div class="container">
						<div class="row">
							<?php if(!empty($title_steps_2)){ ?>
								<div class="col-md-12 text_center">
									<h2><?php echo $title_steps_2; ?></h2>
								</div>
							<?php } ?>
							
							<div class="clear"></div>
							<?php	if ($icons_steps_2){ ?>
								<div class="text_center">
									<ul class="circle_next_list">
									<?php foreach ($icons_steps_2 as $box1) { 
											$step_name = $box1['step_name'];
											$step_icon = $box1['step_icon'];

											if(!empty($step_name)){ ?>					
												<li>
													<span class="icon" style="background-image: url(<?php if(!empty($step_icon)){  echo $step_icon['sizes']['icon_step2']; } ?>);"></span>
													<?php echo $step_name; ?>
												</li>
											<?php } ?>
									<?php } ?>
									</ul>
								</div>
							<?php } ?>
						</div>
					</div>
				</section>
<?php 	} elseif( get_row_layout() == 'team' ){ 
			$title_section_team = get_sub_field('title_section_team');
			$team = get_sub_field('team'); ?>

			<section <?php if($count==2){ echo 'id="first-section"'; } ?> class="hero blog_hero team_hero">
				<div class="container fix-position-plus">
					<div class="row">
						<?php if(!empty($title_section_team)){ ?>
							<div class="col-md-12 title-team">
								<h2 class="style_h2 text_center"><?php echo $title_section_team; ?></h2>
							</div>
						<?php } ?>

						<div class="row-teams">
						<?php if(!empty($team)){
								$count = 0; $count_row = 1; ?>
								<div class="row-team">
								<?php foreach($team as $member){ 
										$name 		= $member['name'];
										$ocupation 	= $member['ocupation'];
										$image  	= $member['image']; 
										if(!empty($name)){ 
											$count++;  ?>
											<div class="single-team <?php /*if($count_row > (intval(sizeof($team)/4)/2) ) { echo 'second-part'; } */ ?>">
												<div class="single-team-row">
													<div class="single-team-img" style="background-image:url(<?php if(!empty($image)){ echo $image['sizes']['team_icon']; } else { echo get_template_directory_uri(). '/images/no-user.png';  } ?>)"></div>
													<div class="single-team-text">
														<div class="vertical-align">
															<p class="team-name"><?php echo $name; ?></p> 
															<p class="team-ocupation"><?php echo $ocupation; ?></p>
														</div>
													</div>
												</div>
											</div>
											<?php //if($count % 4 == 0){ echo '</div><div class="row-team">';  $count_row++; } ?>
										<?php } ?>
								<?php } ?>
								</div>
						<?php } ?>
						</div>
					</div>
				</div>
				<div class="blue-half">
					<div class="position_relative product_triangle_1_parent_parent">
						<div class="product_triangle_1_parent">
							<div class="product_triangle_1"></div>
						</div>	
					</div>
				</div>
			</section>

<?php 	} elseif( get_row_layout() == 'list_of_logos' ){ 

			$title_section_fx2 = get_sub_field('title_section_fx2');
			$list_of_logos_fx2 = get_sub_field('list_of_logos_fx2');  ?>

	      	<section <?php if($count==2){ echo 'id="first-section"'; } ?> class="gray_bg customers_logos customers_logos_inner same-margin-section">
				<div class="container">
					<div class="row">
						<?php if(!empty($title_section_fx2)){ ?>
							<div class="col-md-12 text_center">
								<h2><?php echo $title_section_fx2; ?></h2>
							</div>
						<?php } ?>
						<div class="clear"></div>
						<?php // <!-- use images at 247x131 px --> ?>


						<div class="onload_slider">
							<div class="onload_loading onload_loading_top"> 
								<img src="<?php echo get_template_directory_uri(); ?>/images/ajax-loader.gif" alt="">
							</div>
							<div class="onload_hide">
								<?php	if ($list_of_logos_fx2){ ?>
									<?php foreach ($list_of_logos_fx2 as $box1) {
											$logo = $box1['logo'];
											$link = $box1['link'];
											if(!empty($logo)){  ?>					
												<div class="col-xs-3 col-md-2 single-logo-customers">
													<div class="customer_logo">
														<a href="<?php if(!empty($link)){ echo $link; } else { echo '#'; }?>" <?php if(!empty($link)){ echo 'target="_blank"'; } ?> style="background-image: url(<?php echo $logo['url']; ?>);"></a>
													</div>
												</div>
											<?php } ?>
									<?php } ?>
								<?php } ?>
								
								<div class="clear"></div>
								<?php if(sizeof($list_of_logos_fx2) > 24 ){  ?>
									<div class="col-md-12 text_center see_more_btn_parent">
										<a href="#" title="" id="see-more-logos"><?php _e('See more', THEME_TEXT_DOMAIN); ?></a>
									</div>
								<?php } ?>
								<div class="clear"></div>
							</div>
						</div>

					</div>
				</div>
			</section>

<?php 	} elseif( get_row_layout() == 'testimonials' ){ ?>	
			
			<?php $posts = get_sub_field('testimonials_fx3');
				if( $posts ):
					$count_each = 0; 
					$total_size = sizeof($posts); //var_dump($total_size); ?>
				   
				    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT)  
			         		setup_postdata($post);
			         		$count_each++;
			        		$testimonial_id = $post->ID; //get_the_ID(); // var_dump($post);
			        		$type_of_image = get_field('type_of_image', $testimonial_id);
			        		$type_of_image_crop = get_field('type_of_image', $testimonial_id).'_testimonialsecond';
			        		?>

				        	<section <?php if($count==2){ echo 'id="first-section"'; } ?> <?php 	if($count_each == 1){ echo 'class="background_gray_black testimonial_section" '; } 
				        					else if($count_each == $total_size ){ echo 'class="background_black testimonial_background_gray_black_tablet_mobile testimonial_section" style="padding-bottom: 0;"'; } 
				        					else { echo 'class="background_black testimonial_section"'; }  ?> >
				        		<?php if($count_each == 1){ ?>
									<div class="first_testimonial_triangle_parent" style="display: none;">
										<div class="first_testimonial_triangle" style="display: none;"></div>
									</div>
								<?php } else if($count_each == $total_size ){ ?>
									<div class="hidden-md hidden-lg">
										<div class="last_testimonial_triangle_parent" style="display: none;">
											<div class="last_testimonial_triangle" style="display: none;"></div>
										</div>
									</div>
								<?php } ?>

								<div class="container container-small-2">
									<div class="row">
										<?php  	$img = wp_get_attachment_image_src(get_post_thumbnail_id(), $type_of_image_crop);
												if($img[0]){
													$image_testimonial = $img[0]; 
												} else {  
													$image_testimonial = get_template_directory_uri().'/images/no-testimonial.png';
												} ?>
										<div class="<?php 
												if($type_of_image == 'large'){ echo 'col-md-4'; if($count_each %2 == 0){ echo ' col-md-push-8'; }  } 
												else if($type_of_image == 'medium'){ echo 'col-md-3'; if($count_each %2 == 0){ echo ' col-md-push-9'; }   } 
												else { echo 'col-md-2'; if($count_each %2 == 0){ echo ' col-md-push-10'; }  }  ?> vcenter2 text_center testimonial_img_wrapp ">
											<img src="<?php echo $image_testimonial; ?>">
										</div>
										<div class="<?php 
												if($type_of_image == 'large'){ echo 'col-md-8'; if($count_each %2 == 0){ echo ' col-md-pull-4'; }  } 
												else if($type_of_image == 'medium'){ echo 'col-md-9'; if($count_each %2 == 0){ echo ' col-md-pull-3'; }   } 
												else { echo 'col-md-10'; if($count_each %2 == 0){ echo ' col-md-pull-2'; } }  ?> vcenter2">
											<div class="testimonial <?php if($count_each %2 == 0){ echo 'testimonial_arrow_on_right'; } else { echo 'testimonial_arrow_on_left'; } ?> text_center">
												<?php echo $post->post_content; ?>
												<span class="testimonial_author">
													<?php echo get_the_title($testimonial_id); ?>
												</span>
												<div class="<?php if($count_each %2 == 0){ echo 'speech_right_arrow'; } else { echo 'speech_left_arrow'; } ?> hidden-xs hidden-sm"></div>
												<div class="speech_top_arrow hidden-md hidden-lg"></div>
											</div>
										</div>
									</div>
								</div>
							</section>

				    <?php endforeach; ?>
				    <?php // <!-- this is for last triangle --> ?>
						<section class="background_gray_black testimonial_section last_testimonial_triangle_parent_parent hidden-xs hidden-sm">
							<div class="last_testimonial_triangle_parent" style="display: none;">
								<div class="last_testimonial_triangle" style="display: none;">
								</div>
							</div>
						</section>
					<?php // <!-- end of big sections group --> ?>
				<?php wp_reset_postdata(); ?>
			<?php endif; ?>

<?php 	} elseif( get_row_layout() == 'testimonial' ){

			$image_testimonial 		= get_sub_field('image_testimonial');
			$content_testimonial 	= get_sub_field('content_testimonial');
			$author_testimonial 	= get_sub_field('author_testimonial');
			$logo_testimonial 		= get_sub_field('logo_testimonial');
			$type_of_image2 		= get_sub_field('type_of_image_testimonial').'_testimonial';
			$type_of_image 			= get_sub_field('type_of_image_testimonial');

			if(!empty($content_testimonial)){  ?>	
				<section <?php if($count==2){ echo 'id="first-section"'; } ?> class="testimonial_block testimonial_type_black testm_margin">
					<div class="container container-small-3">
						<div class="row">
							<?php if(!empty($image_testimonial)){ ?>
								<div class="col-md-<?php if($type_of_image == 'large'){ echo '6'; } else if($type_of_image == 'medium'){ echo '4'; } else { echo '3'; }  ?> testimonial-av">
									<div class="testimonial-avatar">
										<img src="<?php echo $image_testimonial['sizes'][$type_of_image2]; ?>" alt="<?php echo $image_testimonial['alt']; ?>">
									</div>
								</div>
							<?php } ?>
							<div class="col-md-<?php if($type_of_image == 'large'){ echo '6'; } else if($type_of_image == 'medium'){ echo '8'; } else { echo '9'; }  ?> testimonial-text">
								<article>
									<?php echo $content_testimonial; ?>
									<?php if(!empty($author_testimonial)){ ?>
										<span><?php echo $author_testimonial; ?></span>
									<?php } ?>

									<?php if(!empty($logo_testimonial)){ ?>
										<div class="company-avatar">
											<img src="<?php echo $logo_testimonial['sizes']['company_single']; ?>" alt="<?php echo $logo_testimonial['alt']; ?>">
										</div>
									<?php } ?>
								</article>
							</div>
						</div>
					</div>
				</section>
			<?php } ?>

<?php 	} elseif( get_row_layout() == 'list_of_resources' ){
			
			$title_list_of_resources 	= get_sub_field('title_list_of_resources');
			$posts 						= get_sub_field('list_resources'); 

			if( $posts ) { ?>	
				<section <?php if($count==2){ echo 'id="first-section"'; } ?> class="generic-blog-wrapper generic-resources-wrapper general-sections-resource same-margin-section">
					<div class="container">
						<div class="row"> 
							<div class="col-md-12 resources-mtitle-general">
								<h2 class="style_h2 centred"><?php echo $title_list_of_resources; ?></h2>	
							</div>
						</div>

						<div class="row">	
							<?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
        						<?php setup_postdata($post);
        							$resource_id = $post->ID; ?>
								<div class="col-md-4 col-sm-12 col-xs-12 single-res">				
									<div class="generic-post-box">
										<div class="generic-featured-post-img" style="background-image: url(<?php 
											$img = wp_get_attachment_image_src(get_post_thumbnail_id($resource_id), 'single-small');
											if($img[0]){
												echo $img[0]; 
											} else { 
												echo get_template_directory_uri().'/images/no-post-small.jpg';
											} ?>);"><a href="<?php echo get_permalink($resource_id); ?>" title="<?php echo get_the_title(); ?>"></a></div>
										<article class="post-short-description">
											<h4><a href="<?php echo get_permalink($resource_id); ?>" title="<?php echo get_the_title($resource_id); ?>"><?php  
													$title = get_the_title($resource_id); 
													if(strlen($title) > 70){ echo substr($title, 0, 70).'...'; } else { echo $title; } ?></a></h4>
											<h4 class="tablet-second-title"><a href="<?php echo get_permalink($resource_id); ?>" title="<?php echo get_the_title($resource_id); ?>"><?php echo get_the_title($resource_id); ?></a></h4>
											<i><?php echo get_the_date('d F Y'); ?></i>
											<div class="excerpt-wrapp">
												<p><?php echo get_the_excerpt($id); ?></p>
											</div>
											<?php 
												$terms = get_the_terms( $resource_id, 'resource-category' );
												$draught_ids = array();
												if ( $terms && ! is_wp_error( $terms ) ) : 
												    
												    foreach ( $terms as $term ) {
												        $draught_ids[] = $term->term_id;
												    }
												?>
											<?php endif;  ?>

											<?php 
											$color 			= get_field('color', 'resource-category_' . $draught_ids[0] );
											$button_label 	= get_field('button_label', $resource_id); 
											if(!empty($button_label)){ ?>		
											<div class="button-wrapp-color">	
												<a href="<?php echo get_permalink($resource_id); ?>" data-color="<?php echo $color; ?>" style="<?php echo 'color:'.$color .';' ; echo 'border: 3px solid '. $color .'; '; ?>" class="resources-width-btn resources-hover" ><?php echo $button_label; ?></a>
											</div>
											<?php } ?>
										</article>
									</div>
								</div>
							<?php endforeach; ?>
						</div>
						<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
					</div>
				</section>

			<?php } ?>

<?php 	} elseif( get_row_layout() == 'list_of_posts' ){ 

			$title_list_of_posts = get_sub_field('title_list_of_posts');
			$posts_list 		 = get_sub_field('list_posts'); 

			if( $posts_list ): ?>
				<section <?php if($count==2){ echo 'id="first-section"'; } ?> class="generic-blog-wrapper generic-resources-wrapper general-sections-resource same-margin-section">
					<div class="container">
						<div class="row"> 
							<div class="col-md-12 resources-mtitle-general">
								<h2 class="style_h2 centred"><?php echo $title_list_of_posts; ?></h2>	
							</div>
						</div>

						<div class="row">	
						    <?php foreach( $posts_list as $post): // variable must be called $post (IMPORTANT) ?>
						        <?php 	setup_postdata($post); 
						        		$resource_id = $post->ID;  ?>
						       
					        	<div class="col-md-4 col-sm-12 col-xs-12 single-res">				
									<div class="generic-post-box">
										<div class="generic-featured-post-img" style="background-image: url(<?php 
											$img = wp_get_attachment_image_src(get_post_thumbnail_id($resource_id), 'single-small');
											if($img[0]){
												echo $img[0]; 
											} else { 
												echo get_template_directory_uri().'/images/no-post-small.jpg';
											} ?>);"><a href="<?php echo get_permalink($resource_id); ?>" title="<?php echo get_the_title(); ?>"></a></div>
										<article class="post-short-description">
											<h4><a href="<?php echo get_permalink($resource_id); ?>" title="<?php echo get_the_title($resource_id); ?>"><?php  
													$title = get_the_title($resource_id); 
													if(strlen($title) > 70){ echo substr($title, 0, 70).'...'; } else { echo $title; } ?></a></h4>
											<h4 class="tablet-second-title"><a href="<?php echo get_permalink($resource_id); ?>" title="<?php echo get_the_title($resource_id); ?>"><?php echo get_the_title($resource_id); ?></a></h4>
											<i><?php echo get_the_date('d F Y'); ?></i>
											<div class="excerpt-wrapp">
												<p><?php echo get_the_excerpt($id); ?></p>
											</div>
											<?php 
												$terms = get_the_terms( $resource_id, 'resource-category' );
												$draught_ids = array();
												if ( $terms && ! is_wp_error( $terms ) ) : 
												    //$draught_ids = array();
												    foreach ( $terms as $term ) {
												        $draught_ids[] = $term->term_id;
												    }
												?>
											<?php endif;  ?>

											<?php 
											if(!empty($draught_ids)){
												$color 			= get_field('color', 'resource-category_' . $draught_ids[0] );
											} else {
												$color 			= '#000';
											}
											
											$button_label 	= get_field('button_label', $resource_id); 
											if(!empty($button_label)){ ?>		
											<div class="button-wrapp-color">	
												<a href="<?php echo get_permalink($resource_id); ?>" data-color="<?php echo $color; ?>" style="<?php echo 'color:'.$color .';' ; echo 'border: 3px solid '. $color .'; '; ?>" class="resources-width-btn resources-hover" ><?php echo $button_label; ?></a>
											</div>
											<?php } ?>
										</article>
									</div>
								</div>	
						    <?php endforeach; ?>
			    		</div>
			    	</div>
			    </section>

			    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
			<?php endif; ?>

<?php 	} elseif( get_row_layout() == 'form' ){  

			$title_form = get_sub_field('title_form');
			$form_code 	= get_sub_field('form_code'); ?>

			<div  <?php if($count==2){ echo 'id="first-section"'; } ?> class="section-left-content-right-image section-form">
				<section class="product_triangle_1_parent_parent ">
					<div class="product_triangle_1_parent" style="display: none;">
						<div class="product_triangle_1 product_triangle_1_gray" style="display: none;"></div>
					</div>
				</section>
				<section class="like_black_form_text black_bg same-margin-section">
					<div class="container">
						<div class="row item_effect">
							<div class="col-md-12 text_center">
								<h2 class="style_h2"><?php echo $title_form; ?></h2>
							</div>

							<div class="col-md-12 item_effect_image">
								<div class="form_wrapp">
									<?php echo $form_code; ?>
								</div>	
							</div>
						</div>
					</div>
				</section>
			</div>

<?php  } elseif( get_row_layout() == 'section_final_image_right_text_left_large' ){ 
				$color_final2 		= get_sub_field('color_final2_large');
				$title_final2 		= get_sub_field('title_final2_large');
				$content_final2		= get_sub_field('content_final2_large'); 
				$image_final2		= get_sub_field('image_final2_large');
				$add_button_final2 	= get_sub_field('add_button_final2_large');
				$type_of_button_final2 = get_sub_field('type_of_button_final2_large');
				$button_label_final2= get_sub_field('button_label_final2_large');
				$button_link_final2 = get_sub_field('button_link_final2_large');
				$button_file_final2 = get_sub_field('button_file_final2_large');
				$remove_shape_top_final2	= get_sub_field('remove_shape_top_final2_large');
				$remove_shape_bottom_final2 = get_sub_field('remove_shape_bottom_final2_large'); ?>


				<section <?php if($count==2){ echo 'id="first-section"'; } ?> class="section-timg-final type_2">
					
					<?php if(empty($remove_shape_top_final2)){  ?>
						<div class="product_triangle_1_parent_parent <?php echo $color_final2; ?>_color">
							<div class="product_triangle_3_parent">
								<div class="product_triangle_3"></div>
							</div>
						</div>
					<?php } ?>

					<div class="wrapper-container <?php echo $color_final2; ?>_color_bg  <?php if(empty($remove_shape_top_final2)){ echo 'no-margin-top'; } if(empty($remove_shape_bottom_final2)){ echo ' no-margin-bottom'; }   ?>">
						<div class="container">
							<div class="row">

								<div class="col-md-5 vcenter item_effect_text scale_text keep_small  text-section-large-general">
									<?php if(!empty($title_final2)){ ?>
										<h3 class="style_h2"><?php echo $title_final2; ?></h3>
									<?php } ?>

									<?php if(!empty($content_final2)){ ?>
									<div class="general_content">
										<?php echo $content_final2; ?>
									</div>
									<?php } ?>

									<?php if(!empty($add_button_final2)){ ?>
										<br>
										<div class="button-timg text_center"><?php	
											switch ($type_of_button_final2) {
												case 'link':
													if(!empty($button_label_final2) && !empty($button_link_final2)){ ?>
														<a href="<?php echo $button_link_final1; ?>" class="btn rounded_btn red_btn"><?php echo $button_label_final2; ?></a>
													<?php } 
													break;
												case 'file':
													if(!empty($button_label_final2) && !empty($button_file_final2)){ ?>
														<a href="<?php echo $button_file_final2['url']; ?>"  target="_blank" class="btn rounded_btn red_btn"><?php echo $button_label_final2; ?></a>
													<?php } 
													break;
												default: break;
											} ?>
										</div>
									<?php } ?>
								</div>

								<?php if(!empty($image_final2)){  ?>
									<div class="col-md-7 vcenter text_right section-img-small item_effect_image scale_image keep_large  image-section-large-general">
										<img src="<?php echo $image_final2['url']; ?>" alt="<?php echo $image_final2['alt']; ?>" class="product_first_image">
									</div>
								<?php } ?>

								
								<div class="clear"></div>
							</div>
						</div>
					</div>

					<?php if(empty($remove_shape_bottom_final2)){  ?>
						<div class="first_black_triangle_parent <?php echo $color_final2; ?>_color">
							<div class="first_black_triangle_bg to_bottom">
								<div class="first_white_triangle_bg"></div>
							</div>	
						</div>
					<?php } ?>
				</section>
				
<?php  } elseif( get_row_layout() == 'section_final_image_left_text_right_large' ){ 	

				$color_final1	 	= get_sub_field('color_final1_large');
				$title_final1 		= get_sub_field('title_final1_large');
				$content_final1 	= get_sub_field('content_final1_large');
				$image_final1 		= get_sub_field('image_final1_large');
				$add_button_final1  = get_sub_field('add_button_final1_large');
				$type_of_button_final1 = get_sub_field('type_of_button_final1_large');
				$button_label_final1 = get_sub_field('button_label_final1_large');
				$button_link_final1 = get_sub_field('button_link_final1_large');
				$button_file_final1 = get_sub_field('button_file_final1_large');
				$remove_shape_top_final1	= get_sub_field('remove_shape_top_final1_large');
				$remove_shape_bottom_final1 = get_sub_field('remove_shape_bottom_final1_large'); ?>

				<section <?php if($count==2){ echo 'id="first-section"'; } ?> class="section-timg-final type_1">
					<?php if(empty($remove_shape_top_final1)){  ?>
						<div class="product_triangle_1_parent_parent <?php echo $color_final1; ?>_color">
							<div class="product_triangle_3_parent">
								<div class="product_triangle_3"></div>
							</div>
						</div>
					<?php } ?>

					<div class="wrapper-container <?php echo $color_final1; ?>_color_bg <?php if(empty($remove_shape_top_final1)){ echo 'no-margin-top'; } if(empty($remove_shape_bottom_final1)){ echo ' no-margin-bottom'; }   ?>">
						<div class="container">
							<div class="row">

								<?php if(!empty($image_final1)){  ?>
									<div class="col-md-7 vcenter text_left section-img-small item_effect_image scale_image keep_large  image-section-large-general hide_on_tablet">
										<img src="<?php echo $image_final1['url']; ?>" alt="<?php echo $image_final1['alt']; ?>">
									</div>
								<?php } ?>
								
								<div class="col-md-5  vcenter item_effect_text scale_text keep_small  text-section-large-general">
									<?php if(!empty($title_final1)){ ?>
										<h3 class="style_h2"><?php echo $title_final1; ?></h3>
									<?php } ?>

									<?php if(!empty($content_final1)){ ?>
									<div class="general_content">
										<?php echo $content_final1; ?>
									</div>
									<?php } ?>

									<?php if(!empty($add_button_final1)){ ?>
										<br>
										<div class="button-timg text_center"><?php 
											switch ($type_of_button_final1) {
											case 'link':
												if(!empty($button_label_final1) && !empty($button_link_final1)){ ?>
													<a href="<?php echo $button_link_final1; ?>" class="btn rounded_btn red_btn"><?php echo $button_label_final1; ?></a>
											
												<?php } 
												break;
											case 'file': 
												if(!empty($button_label_final1) && !empty($button_file_final1)){ ?>
													<a href="<?php echo $button_file_final1['url']; ?>" target="_blank" class="btn rounded_btn red_btn"><?php echo $button_label_final1; ?></a>
												<?php } 
												break;
											default: break;
										} ?>
										</div>
									<?php } ?>
								</div>

								<?php if(!empty($image_final1)){  ?>
									<div class="col-md-7 vcenter text_left section-img-small item_effect_image scale_image keep_large  image-section-large-general show_on_tablet">
										<img src="<?php echo $image_final1['url']; ?>" alt="<?php echo $image_final1['alt']; ?>">
									</div>
								<?php } ?>

								
								
								<div class="clear"></div>
							</div>
						</div>
					</div>

					<?php if(empty($remove_shape_bottom_final1)){  ?>
						<div class="first_black_triangle_parent <?php echo $color_final1; ?>_color">
							<div class="first_black_triangle_bg to_bottom">
								<div class="first_white_triangle_bg"></div>
							</div>	
						</div>
					<?php } ?>
				</section>


<?php 	} ?>

<?php  
    endwhile;
endif; ?>

<?php 	$activate_newsletter_popup = get_field('activate_newsletter_popup');
		if(!empty($activate_newsletter_popup)){ 
			get_template_part( 'parts/general-popup');
		} ?>

<?php get_footer();