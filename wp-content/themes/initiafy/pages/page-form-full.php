<?php /* Template Name: Page Form Full */
get_header('empty'); ?>

<section class="page_form_full">
	<div class="container container-small-3"> 
		<div class="row">
			<div class="col-md-12 standard-page">
				<?php 	$content_page = get_field('content_page');
						if(!empty($content_page)){ ?>
							<div class="register-form-wrapper generator_form no_bg vertical_form">
								<a href='javascript:history.back(1);' class="turn-back"></a>
								<?php echo $content_page; ?>
							</div>
				<?php 	} ?>
			</div>
		</div>
	</div>
</section>

<?php get_footer('empty');
