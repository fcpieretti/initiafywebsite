<?php /* Template Name: Page Form */
//get_header('single');
get_template_part( 'parts/header-type'); ?>

<section class="generic-landing-page-wrapper-normal page_form" >
	<div class="container container-small-3"> 
		<div class="row">
			<div class="col-md-12 standard-page">
				<?php 	$content_page = get_field('content_page');
						if(!empty($content_page)){ ?>
							<div class="register-form-wrapper generator_form">
								<a href='javascript:history.back(1);' class="turn-back"></a>
								<?php echo $content_page; ?>
							</div>
				<?php 	} ?>
			</div>
		</div>
	</div>
</section>

<?php get_footer();
